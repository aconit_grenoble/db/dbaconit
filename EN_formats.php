<?PHP  
// v10.7	130412	PhD	Suppression de toutes les tables menus, formats, seul reste Translations
// v10.8	130805	PhD	Suppression des valeurs inutiles (remplacement par fonction "Tr")
// v14.1	151003	PhD	Reprise des traductions
// v16.1	161130	PhD	Supprimé message inutile
// v18		170804	PhD	Mise à jour
// v26.2	230416	PhD	Reprise traductions erreur importation de fichier



## Translation table for strings inside main code =============================
//											By alphabetic order (more or less)
$Translations = array (
	"Aucun objet n'a été trouvé" => 'No items were found',
	'Aucun objet trouvé' => 'No items were found',
	"Aucune table n'a été trouvée..." => 'No table found',

	"Extension fichier inconnue : traité comme un fichier binaire .IMG" => 'File extension is unknown: processd as a binary .IMG file',
	"Ce numéro n'existe pas..." => 'This number does not exist',

	"Enregistrement effectué" => 'Record saved',
	"Enregistrement %0 créé"=> "%0 index created",
	"Enregistrement %0 supprimé"=> "%0 index deleted",
	"Enregistrement mouvement supprimé %0"=> "Move record deleted %0",
	"Erreur d'écriture" => 'Write error',
	"Erreur de transfert du fichier [# %0]" => 'File import error [# %0]',
	"Erreur lors de l'ouverture - %0 " => 'Opening error - %0',

	"Fiche corrigée" => "Data base updated",
	"Fiche supprimée" => 'Record deleted',
	'Fichier XML non sélectionné' => 'XML file not selected',
	"Fiche machine supprimée"=> "Machine index deleted",
	"Fiche document supprimée"=> "Document index deleted",
	"Fiche logicielle supprimée"=> "Software index deleted",
	"Fiche principale collection supprimée"=> "main collection index deleted",

	"[id] origine n'existe pas..." => "Origin [id] doesn't exist",
	"[id] cible n'existe pas..." => "Target [id] doesn't exist",
	 "Impossible d'enregistrer" => 'Recording impossible',
	"Impossible d'enregistrer le nouveau nom" => 'Impossible to record the new name',
	'Informations manquantes' => 'Missing data',

	"La taille du fichier dépasse la limite autorisée" => "File size exceeds allowed limit",
	"L'enregistrement est déjà utilisé ailleurs, sa suppression nuirait à la cohérence de la table" => 
			'This record is already used sommewhere, its deletion would corrupt the table',
	"La restauration a réussi" => 'Restoration completed successfully',
	"Le champ %0 n'a pas pu être modifié, vérifiez le type que vous avez entré..." =>
				'Field %0 cannot be modified, check datatype you entered...',
	"Le champ date ne peut pas rester nul" => "Date field can't stay null",
	"- Le lien a été supprimé - " => '- The link has been deleted - ',
	"- Le lien a été inversé - " => '- The link has been reversed - ',
	"- Le lien entre le nom %0 et l'élément %1 a été supprimé - " => 
			'The link between name %0 and element %1 has been deleted',
	"- Le lien %0 / %1 a été ajouté - " => 'The link %0 / %1 has been added',
	"- Le lien %0 / %1 existe déjà - " => 'The link %0 / %1 already exists',
	"- Le lien entre le média %0 et l'élément %1 a été supprimé - " => 
				'The link between item %0 and item %1 has been deleted',
	"Le nom « %0 » a été ajouté" => 'The name « %0 » has been added',
	"Le nom « %0 %1 %2 » a été ajouté" => 'The name « %0 %1 %2 » has been added',
	"Le nom %0 est déjà attaché pour la même relation" => 'The name %0 is already attached for the same connection',
	"Le numéro %0 est déjà utilisé" => 'The number %0 is alreday used',
	"%0 liens entre objets supprimés" => '%0 links between items deleted',
	"%0 liens matériaux supprimés" => "%0 materials links deleted",
	"%0 liens médias supprimés" => "%0 medias links deleted",
	"%0 liens mots clés supprimés" => "%0 keywords links deleted",
	"%0 liens mouvements supprimés"=> "*0 mouvements links deleted",
	"%0 liens organismes supprimés" => "%0 organisms links deleted",
	"%0 liens personnes supprimés" => "%0 persons links deleted",
	"Média %0 attaché" => 'Media %0 attached',
	"Modification enregistrée"=> 'Update recorded',
	"Modification refusée..." => 'Update rejected',
	"Modification réussie" => 'Update successful',
	"Mouvement %0 attaché" => 'Mouvement %0 attached',
	
	'Nombre de fiches : %0' => 'Number of records: %0',
	'Nombre de résultats : %0' => 'Number of results: %0',
	"Nouvelle  cote de rangement enregistrée" => "New location recorded",

	"Pas d'inscription dans %0" => 'No writing in %0',
	"Pas d'inscription dans Collections" => 'No writing in Collections',

	"*** Refus ouverture fichier %0 ***" => '*** Impossible to open file %0 ***',
	'Remplacement effectué' => 'Replacing done',
	"Requête '%0' %1 " => "Request '%0' %1",
	"Requête incorrecte : %0" => 'Wrong request: %0',
	"code %0 : %1" => 'code %0: %1',		// 2e ligne

	'Sauvegarde terminée' => 'Save operation completed',
	'Suppression organisme=%0 effectuée' => 'Suppressing organization=%0 is done',
	'Suppression personne=%0 effectuée' => 'Suppressing person=%0 is done',

	"Transfert format %0 inconnu" => 'Unknown transfer format %0',

	"Un des indices [id] doit obligatoirement être celui de l'objet courant" => 
				'One of the indexes must be the own index  of the present object',

	"Vous avez fait une erreur dans votre login/passe." => 'Login failed',
	"Vous avez oublié de sélectionner une table." => 'You forgot to specify a table.',
	"Vous n'avez pas entré de nom recherché" => 'You didn\'t specify a requested name ',
	"Vous n'avez pas entré votre login" => 'No login name specified',
	"Vous ne vous êtes pas identifié..." => 'You didn\'t identify yourself',
	"Vous n'avez pas indiqué de nouveau nom" => 'You didn\'t specify a new name',
	"Vous n'avez pas indiqué la nouvelle relation" => 'You didn\'t specify the new relation',
	"Vous n'avez sélectionné aucun media" => 'You didn\'t select any media',	
	"Vous n'avez pas sélectionné de fichier valide" => 'You didn\'t specify a valid file',
	"Vous n'avez proposé aucun lien" => 'You didn\'t propose any link',
	"Vous n'avez sélectionné aucun lien" => 'You didn\'t select any link',
	"Vous n'avez sélectionné aucun média" => 'You didn\'t select any media',
	"Vous n'avez sélectionné aucun mouvement" => 'You didn\'t select any mouvement',
	"Vous n'avez sélectionné aucun nom" => 'You didn\'t select any name',
	 "Vous n'avez pas spécifié un numéro de mouvement" => 'You didn\'t specify a move number',
	"Vous n'êtes pas autorisé à modifier les fiches d'inventaire de l'organisme %0" => 
				'You are not allowed to modify the %0 inventory files',
);


?>
