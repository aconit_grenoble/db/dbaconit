## DBAconit

## ACONIT : Association pour un Conservatoire de l'Informatique et de la Télématique
L'Association pour un CONservatoire de l'Informatique et de la
Télématique[^1] (ACONIT) est une association loi 1901 qui a été crée le
24 janvier 1985 et dont le but est « de créer les structures permettant
l\'étude et l'illustration de l\'évolution de l'informatique...»[^1].

## DBAconit
DBAconit est un logiciel de gestion de collection spécialement développé par ACONIT depuis 2002 pour gérer le patrimoine informatique. Il a été adapté pour assurer une bonne compatibilité avec la base nationale de la mission de Sauvegarde du patrimoine scientifique et technique contemporain (PATSTEC) [^2].

DBAconit peut gérer plusieurs bases de données sur le même serveur. Il a été généralisé pour permettre des traitements multi-établissements et multi-bâtiments gestionnaires, avec des numérotations indépendantes. Il permet l'export et l'import de fiches en format XML. 
Un package annexe permet la gestion de "galeries virtuelles" [voir DBgalerie](https://gitlab.com/aconit_grenoble/db/dbgalerie).

DBAconit peut être porté sur tout serveur Internet supportant PHP et MySQL. Il est accessible à partir de toute machine ayant accès au réseau, à partir d'un navigateur internet standard, compatible HTML5.

DBAconit est programé en français, mais tous les affichages système sont bilingues français/anglais.
Principale base gérée sous DBAconit : [Base inventaire ACONIT](https://db.aconit.org/dbaconit/).

## Documentation
L'ensemble de la documentation se trouve dans le dossier [Documentation](https://db.aconit.org/documentation/),
en particulier :
1. Notice d'utilisation DBAconit
2. Structure dDBAconit
3. Notice technique DBAconit

## Crédits
Spécification et réalisation par Ph.Denoyelle,
avec le concours de Idriss Farhat pour la maquette, d'Hans Pufal pour l'usage généralisé de XML (transferts et affichages) et de Matthieu Saro pour l'affichage des médias.

## Licence CeCILL
Copyright  Association ACONIT
	contributeur : Philippe Denoyelle, (2002-2018) 
	pdenoyelle@aconit.org - info@aconit.org

Ce logiciel est un programme informatique servant à gérer une base de 
données inventaire du patrimoine, en ligne sous MySQL. 
Il s'applique également au logiciel DBAconit et à son annexe DBgalerie

Ce logiciel est régi par la licence CeCILL soumise au droit français et
respectant les principes de diffusion des logiciels libres. Vous pouvez
utiliser, modifier et/ou redistribuer ce programme sous les conditions
de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
sur le site [http://www.cecill.info](http://www.cecill.info)

En contrepartie de l'accessibilité au code source et des droits de copie,
de modification et de redistribution accordés par cette licence, il n'est
offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
seule une responsabilité restreinte pèse sur l'auteur du programme,  le
titulaire des droits patrimoniaux et les concédants successifs.

A cet égard  l'attention de l'utilisateur est attirée sur les risques
associés au chargement,  à l'utilisation,  à la modification et/ou au
développement et à la reproduction du logiciel par l'utilisateur étant 
donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
manipuler et qui le réserve donc à des développeurs et des professionnels
avertis possédant  des  connaissances  informatiques approfondies.  Les
utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
logiciel à leurs besoins dans des conditions permettant d'assurer la
sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
pris connaissance de la licence CeCILL, et que vous en avez accepté les
termes.

--------------------
[^1] : Extrait des statuts
[^2] : PATSTEC : mission nationale de sauvegarde du patrimoine scientifique et technique contemporain. Créée le 25 février 2004 par le Conservatoire National des Arts et Métiers CNAM [www.pastec.fr](../www.pastec.fr).
