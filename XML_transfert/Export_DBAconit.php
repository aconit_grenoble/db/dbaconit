<?php
# v25				20190712	PhD		Création à partir de modèle de transfert vers PATSTEC
# v25.3			20191091	PhD		Ajout fonctions IdBatiment et XML_TABLE_BATIMENTS
# v25.3.02	20191112	PhD		Traiter le cas Tables des bâtiments vide
# v25.6			20200125	PhD		Ajouté tri par idcollection
# v25.7			20200327	PhD		Ajouté join Tnpositions et Trecols
# v26.6			20240527	PhD		Suprimé l'échapement de l'éperluette dans PostEval (traité automatiquement)
###


############################################################################################ Idbatiment ###           
function IdBatiment()
# Trouve l'index idbatiment à partir des 3 variables 'nbsegment', 'coterange' et 'idetablissement'
# transmises par Xvars
{	global $Xvars;

	$cotebatiment = Cotebatiment ($Xvars['nb_segments'], $Xvars['coterange']);

	$idetablissement =$Xvars['idetablissement'];
	$where = "idetablissement = $idetablissement AND cotebatiment LIKE '$cotebatiment'";

	// Recherche en base
	$resultat = Requete ("SELECT idbatiment FROM Batiments WHERE $where");
												
	if (!mysqli_num_rows($resultat)) return NULL;
	else {
		$ligne = mysqli_fetch_assoc ($resultat);		// S'il y avait plusieurs réponses, c'est la première qui est prise...
		return $ligne['idbatiment'];
	}    

}

############################################################################################### Nonnul ###           
function Nonnul($val) { 
	
	return ($val !=0) ? $val : ""; 
}

################################################################################################### PostEval ###           
function PostEval ($texte)   
# Fonction appelée systématiquement à la sortie du module Xvalue d'évaluation de chaine 
# On ne touche plus les NL, on supprime les LF éventuels, on recode l'apostrophe std
# et on n'échappe plus l'éperluette pour éviter un double codage &amp;amp; !
#------------------------------------------------------------------------------
{ 
	if (is_string ($texte)){
		$texte = str_replace (array("\r", '&#039;'), array ('', '&apos;'), $texte); 
	}  
  return $texte;    
}        

################################################################################################ XML_champs ###  
function XML_champs ($loop, $attr, $Xaction) 
# La fonction relève la valeur de tous les champs de la table 
# dont le nom est passé en attribut 'table' et l'idcollection' par Xvars
# Attention : la fonction n'effectue aucun traitement sur les index id
# Dans le cas d'Export_DBAconit, ne convient pas pour les tables qui appelent des tables d'ordre.
{
	global $Xvars;
  static $SQL_res;

	if ($loop === null) return;		// tag de fin
 
	$table = $attr['table'];
	$idcollection = $Xvars['idcollection'];  
 
	// tag de début, lire la base
	if ($loop === 0) {
        $SQL_res = requete ("SELECT * FROM $table WHERE idcollection='$idcollection'");
  }
              
   // Tant qu'il y a des résultats
    while ($ligne = mysqli_fetch_assoc ($SQL_res)) {
			$Xvars['ligne'] = $ligne;
    	return 'ACT,LOOP';  			// >>>>>>>>>>>>>>>
    }
    
    return 'EXIT';
}

################################################################################################ XML_col ###  
function XML_col ($loop, $attr, $Xaction) 
# La fonction, appelée pour chacun des objets traités, boucle sur les différents item associés 
# (matériaux, mots clés...)
# Elle reçoit 'idcollection' par Xvars, distribué par XML_liste
#!!! Pourait traiter directement la table '$t_idcollection' !!!
# Elle reçoit 2 attributs :
# - table : table Materiaux, Motscles...
# - col_tab : table de liaison entre idcollection et id table
###
{	global $Xvars;
  static $SQL_res;

	if ($loop === null) return;		// tag de fin
 
	$table = $attr['table'];
	$col_tab =$attr['col_tab'];
	$idtab = NomId ($table);	
	
	$idcollection = $Xvars['idcollection'];  
 
	// tag de début, lire la base
	if ($loop === 0) {
        $SQL_res = requete ("SELECT * FROM $table 
        LEFT JOIN $col_tab ON $table.$idtab=$col_tab.$idtab
        WHERE idcollection='$idcollection'");
  }
              
   // Tant qu'il y a des résultats
    while ($ligne = mysqli_fetch_assoc ($SQL_res)) {
			$Xvars['ligne'] = $ligne;
						
    	return 'ACT,LOOP';  			// >>>>>>>>>>>>>>>
    }
    
    return 'EXIT';
}

################################################################################################ XML_liens ###  
function XML_liens ($loop, $attr, $Xaction) 
# Pour tous les objets faisant partie de cet export, la fonction relève 
# les liens père-fils. 
# Les liens "famille" et "même lot" ne sont pas exclus 
# S'il reste des pères multiples... ils sont transmis...
{
	global $Xvars;
  static $SQL_result_liens;  

	if ($loop === null) return;		// tag de fin
 
	$idcollection = $Xvars['idcollection'];  
 
	// tag de début, lire la base
	if ($loop === 0) {
        $SQL_result_liens = requete ("SELECT *
              FROM Liens
              JOIN Familles ON Familles.idfamille = Liens.idfamille
              WHERE idcol1 = $idcollection OR idcol2 = $idcollection");
  }
              
   // Tant qu'il y a des résultats
    while ($ligneliens = mysqli_fetch_assoc ($SQL_result_liens)) {
			$Xvars['idcol1_d'] = $ligneliens['idcol1'];
			$Xvars['idcol2_d'] = $ligneliens['idcol2'];
			$Xvars['commentlien'] = $ligneliens['commentlien'];
			$Xvars['famille'] = $ligneliens['famille'];
    	return 'ACT,LOOP';  			// >>>>>>>>>>>>>>>
    }
    
    return 'EXIT';
}


################################################################################################ XML_liste ###  
function XML_liste ($loop, $attr, $Xaction) 
# Cette fonction explore les index rangés dans la table $t_idcollection
# et retourne à chaque tour un index XML_liens, à travers la variable Xvars['index']
{
  global $t_idcollection;              
	global $Xvars;
	static $index;
 
	if ($loop === null) return;		// tag de fin
 
	// tag de début, lire appeler le premier index
	if ($loop === 0) $index = -1;

	// incrémenter l'index et tester la limite	
	$index += 1;
	if ($index < count ($t_idcollection)) {
		$Xvars['idcollection'] = $t_idcollection[$index];
    return 'ACT,LOOP';  			// >>>>>>>>>>>>>>>
    
  } else return 'EXIT';
}

################################################################################################ XML_medias ###  
function XML_medias ($loop, $attr, $Xaction) 
# La fonction, appelée pour chacun des objets traités, boucle sur les différents médias associés,
# Elle ajoute l'adresse du fichier média ET de la vignette correspondantes dans la table 'fichierListe'
###
{	global $Xvars, $db, $fichierListe;
  static $SQL_res;
	if (!isset ($fichierListe)) $fichierListe = array();       

	if ($loop === null) return;		// tag de fin
 	
	$idcollection = $Xvars['idcollection'];  
 
	// tag de début, lire la base
	if ($loop === 0) {
        $SQL_res = requete ("SELECT * FROM Medias 
        LEFT JOIN Col_Med ON Medias.idmedia=Col_Med.idmedia
        WHERE idcollection='$idcollection'");
  }
              
   // Tant qu'il y a des résultats
    while ($ligne = mysqli_fetch_assoc ($SQL_res)) {
			$Xvars['ligne'] = $ligne;
			$idmedia = $ligne['idmedia'];
			$extmedia = trim ($ligne['formatmedia']);
			
	// Ajouter les médias, s'ils existent bien. (on ne tient pas compte de la publication autorisée...)
	// Les vignettes sont codées avec une lettre 'v'
			if (file_exists (AdMedia($idmedia, $db, $extmedia))) {        
				$fichierListe[$idmedia.'.'.$extmedia] = AdMedia($idmedia, $db, $extmedia);        
				$fichierListe['v'.$idmedia.'.'.$extmedia] = AdMedia($idmedia, $db, $extmedia, 'v');        
			}
    	return 'ACT,LOOP';  			// >>>>>>>>>>>>>>>
    }
    
    return 'EXIT';
}

######################################################################################### XML_organ ###           
function XML_organ ($loop, $attr, $Xaction)
{       

	if ($loop === null) return;		// tag de fin
	
	global $Xvars;
	static $SQLresult_orga;
  global $requete;       

	// tag de début, lire la base
	if ($loop === 0) {
   	$SQLresult_orga = requete (
			"SELECT DISTINCT Collections.idcollection, Organismes.idorganisme, onom, osigle, f_secable, onotes, relation 
			FROM Collections, Organismes, Col_Org, Relations
			WHERE Organismes.idorganisme=Col_Org.idorganisme 
				AND Relations.idrelation=Col_Org.idrelation
				AND Col_Org.idcollection= Collections.idcollection
				AND ".$requete );
	
	   if (0 == (mysqli_num_rows ($SQLresult_orga)))
  	    	return 'EXIT';		// >>>>>>>>>>>>>>>
	}
   // Tant qu'il y a des résultats
	while ($Xvars =  mysqli_fetch_assoc ($SQLresult_orga)) {
		return 'ACT,LOOP';
	}
   
    return 'EXIT';       
} 
       

########################################################################################## XML_person ###           
function XML_person ($loop, $attr, $Xaction) 
{       

	if ($loop === null) return;		// tag de fin
	
	global $Xvars;
	static $SQLresult_perso;
  global $requete;       

	// tag de début, lire la base
	if ($loop === 0) { 	
   	$SQLresult_perso = requete (
   		"SELECT DISTINCT Collections.idcollection, Personnes.idpersonne, pcivil, pnom, pprenom, pnotes, relation 
   		FROM Collections, Personnes, Col_Per, Relations
			WHERE Personnes.idpersonne=Col_Per.idpersonne 
				AND Relations.idrelation=Col_Per.idrelation
				AND Col_Per.idcollection= Collections.idcollection
	    	AND ".$requete );
	
	   if (0 == (mysqli_num_rows ($SQLresult_perso)))
  	    	return 'EXIT';		// >>>>>>>>>>>>>>>
	}
   // Tant qu'il y a des résultats
	while ($Xvars =  mysqli_fetch_assoc ($SQLresult_perso)) {
		return 'ACT,LOOP';
	}
   
    return 'EXIT';       
} 
       
############################################################################### XML_TABLE_COLLECTIONS ###           
function XML_TABLE_COLLECTIONS($loop, $attr, $Xaction)        
{
	if ($loop === null) return 'TAG';		// tag de fin
	
	global $Xvars;
  global $db, $objetListe, $requete, $t_idcollection, $t_idbatiment;              
	static $resultat;

# Tag de début, lire la base
	if ($loop === 0) {
		$t_idbatiment = array ();
		$t_idcollection = array ();
    $objetListe = array ();          

		$resultat = requete(    
		"SELECT * FROM Collections 
			left join Machines on Machines.idmachine=Collections.idmachine    
			left join Documents on Documents.iddocument=Collections.iddocument    
			left join Logiciels on Logiciels.idlogiciel=Collections.idlogiciel  
			
			left join Designations on Designations.iddesignation=Machines.iddesignation    
			left join Services on Services.idservice=Machines.idservice   
			
			left join Langues on Langues.idlangue=Documents.idlangue   
			left join Typedocs on Typedocs.idtypedoc=Documents.idtypedoc 
			left join Typeillustrs on Typeillustrs.idtypeillustr=Documents.idtypeillustr 
			
			left join Codeprogs on Codeprogs.idcodeprog=Logiciels.idcodeprog    
			left join Langprogs on Langprogs.idlangprog=Logiciels.idlangprog    
			left join Supports on Supports.idsupport=Logiciels.idsupport    
			left join Systemes on Systemes.idsysteme=Logiciels.idsysteme    
			
			left join Acquisitions on Acquisitions.idacquisition=Collections.idacquisition    
			left join Domaines on Domaines.iddomaine=Collections.iddomaine    
			left join Etats on Etats.idetat=Collections.idetat
			left join Etablissements on Etablissements.idetablissement=Collections.idetablissement   
			left join Etatfiches on Etatfiches.idetatfiche=Collections.idetatfiche
			left join Periodes on Periodes.idperiode=Collections.idperiode    
			left join Productions on Productions.idproduction=Collections.idproduction    
			LEFT JOIN Tnpositions ON Tnpositions.idtnposition=Collections.idtnposition
			LEFT JOIN Trecols ON Trecols.idtrecol=Collections.idtrecol
			WHERE ".$requete. ' ORDER by idcollection' );       

			if (mysqli_num_rows ($resultat) == 0)  return  'TAG';  	// EXIT sans résultats        
    }            

# À chaque appel :
    $dbdata=mysqli_fetch_assoc($resultat);             
    if ($dbdata == NULL) return; 						  // EXIT    
		// Préparer le tableau des variables
		$Xvars =  $dbdata;
		$Xvars['db'] = $db;
		
		if ($dbdata['idmachine']) $tsec = 'MACHINE';
		if ($dbdata['iddocument']) $tsec =  'DOCUMENT';
		if ($dbdata['idlogiciel']) $tsec = 'LOGICIEL';
		$Xvars['tsec'] = $tsec;
		
		// Dresser un tableau des idcollection, pour créer ensuite l'export des liens
		$t_idcollection[] = $dbdata['idcollection'];

    // Dresser un tableau des idbatiment, pour créer ensuite l'export des batiments
 		$idbatiment = IdBatiment ();											// les 3 variables nécessaires sont dans Xvars
    if ($idbatiment) $t_idbatiment[] = $idbatiment;		// on vérifie que l'index est non null

		// objetListe est utilisé pour créer les documents d'accompagnement de l'envoi.
    $objetListe[count($objetListe)] = $dbdata['idcollection']. ' : ' .          
        ($dbdata['titredoc'] ?       
        'Doc : '.$dbdata['titredoc'] :       
        ($dbdata['titrelog'] ? 'Logiciel : '. $dbdata['titrelog'] :       
        'Machine : ' . $dbdata['nom'])); 
        
    // -----
  	if ($loop === 0)  return 'TAG,ACT,LOOP';			// Le tag de début est émis une seule fois à l'ouverture
		else return 'ACT,LOOP';    
}   

############################################################################################# XML_TABLE_BATIMENTS ###  
function XML_TABLE_BATIMENTS ($loop, $attr, $Xaction) 
# Cette fonction explore les index rangés dans la table $t_idbatiment
# et retourne à chaque tour un index XML_liens, à travers la variable Xvars['index']
{
	global $Xvars, $t_idbatiment;              
 
	if ($loop === null) return 'TAG';		// tag de fin
 
# Tag de début
	if ($loop === 0) {
		// Si la table est vide, sortir uniquement le tag de début et quitter
		if (count ($t_idbatiment) == 0) return 'TAG';			// >>>>>>>>>>>>>>>>>>
		// Sinon, dédoublonner la table des idbatiment
		$t_idbatiment = array_unique ($t_idbatiment, SORT_NUMERIC);
	}
	
#	Appeler le batiment courant
	if (count ($t_idbatiment) > 0) {
		// Après l'opération array_unique, les index ne sont plus continus => préférence à array_shift...
		$idbatiment = array_shift ($t_idbatiment);
	
		$result_bat = requete( "SELECT * FROM Batiments WHERE idbatiment=$idbatiment");          
		$Xvars = mysqli_fetch_assoc($result_bat);  		// Il y a toujours une réponse

	# Sortir 
			if ($loop === 0)  return 'TAG,ACT,LOOP';			// Le tag de début est émis une seule fois à l'ouverture
			else return 'ACT,LOOP';  
			
  } else return 'EXIT';
}
     
############################################################################### XML_TABLE_ETABLISSEMENTS ###           
function XML_TABLE_ETABLISSEMENTS($loop, $attr, $Xaction)        
{
	if ($loop === null) return 'TAG';		// tag de fin
	
	global $Xvars;
  global $requete;              
	static $result_et;

	// tag de début, lire la base
	if ($loop === 0) {

		$result_et = requete( "SELECT DISTINCT Collections.idetablissement, prefinv, etablissement, localisation,
			nb_segments,numeration 
			FROM Collections
			LEFT JOIN Etablissements ON Etablissements.idetablissement=Collections.idetablissement    
			WHERE ".$requete );          
	
	   if (0 == (mysqli_num_rows ($result_et)))
  	    	return 'TAG';													// >>>>>>>>>>>>>>>
	}

 // Tant qu'il y a des résultats
	$dbdata=mysqli_fetch_assoc($result_et);             
	if ($dbdata == NULL) return 'EXIT'; 			  // >>>>>>>>>>>>>>>   
	$Xvars =  $dbdata;
   
	if ($loop === 0)  return 'TAG,ACT,LOOP';			// Le tag de début est émis une seule fois à l'ouverture
	else return 'ACT,LOOP';    
} 
     
?>