<?php
# v26.3			20230323	PhD		Création à partir de Extrait_XML, spécialement pour le projet Museum_IT
# v6.1			20230704	PhD		Complément pour utilisation autonome sur dbserver 
# v7				20230719	PhD		simplifié l'affichage Médias
# v7.1			20230819	PhD		Déplacé Affdate et Snv dans globalfacts
# v26.6			20240527	PhD		Suprimé l'échapement de l'éperluette dans PostEval (traité automatiquement)
###


#================================================================= Description ===           
function Description($td_fr, $tl_fr, $d) { 
	
	if ($tl_fr != "")
		return "Titre traduit : ".$tl_fr."\n".$d ;
	elseif ($td_fr != "") 
		return "Titre traduit : ".$td_fr."\n".$d ;
	else return $d;
}

############################################################# Design_titre ###
function Design_titre_museumit ($designation_en, $nom, $nom_en, $nomsecond, $nomsecond_en) 
# Retourne une désignation complète d'une machine...

{
		// Traiter le cas anglais 
		$np = Snv ($nom, $nom_en);
		$ns = Snv ($nomsecond, $nomsecond_en);
		$titre = $designation_en.' '.$np.(($ns != '')? ' - '.$ns :'');
	return $titre;
}
#================================================================= Fonctionnel ===           
function Fonctionnel($service) { 

	if (($service == 'service partiel') || $service == 'opérationnel') return "1";
	return "0";
}

#======================================================================= Nonnul ===           
function Nonnul($val) { 
	
	return ($val !=0) ? $val : ""; 
}

################################################################################################### PostEval ###           
function PostEval ($texte)   
# Fonction appelée systématiquement à la sortie du module Xvalue d'évaluation de chaine 
# On ne touche plus les NL, on supprime les LF éventuels, on recode l'apostrophe std
# et on n'échappe plus l'éperluette pour éviter un double codage &amp;amp; !
#------------------------------------------------------------------------------
{ 
	if (is_string ($texte)){
		$pattern = array ('#\r#', '#&nbsp;#', '%&#039;%', '#&quot;#', '#&gt;#');
		$replacement = array ('', ' ', "'", '"', '>' );	
		$texte = preg_replace ($pattern, $replacement, $texte);
	}  
  return $texte;    
}        

#================================================================== Souschaine ====
function Souschaine ($s, $n=0) {
// Sépare les différentes parties d'une chaine

	$t = Explode ("|", $s);
	return Trim ($t[$n]);
}

##################################################################### XML_tab_medias ###
function XML_MEDIAS ($loop, $attr, $Xaction) {

	global $Xvars;
	static $SQLresult_photos;
	if ($loop === null) return 'TAG';		// tag final 

	// tag de début, lire la table
	if ($loop === 0) {
		$idcollection = $Xvars['idcollection'];
		$droits = $_SESSION['droits'];
		$reserve = (in_array("aff_req_priv", $droits)) ? '' : " AND pubreserv='non' ";

		$SQLresult_photos = requete (	
			" SELECT Col_Med.idmedia, cotemedia, ordre_media, descrimedia, pubreserv, formatmedia
              FROM Medias, Col_Med 
              WHERE Col_Med.idmedia = Medias.idmedia
                 AND Col_Med.idcollection = $idcollection
                 $reserve
              ORDER BY ordre_media");
		if (0 == mysqli_num_rows ($SQLresult_photos)) return 'TAG';   // >>>>>>>>>>>>>

	}

	while (null != ($l_photo = mysqli_fetch_assoc ($SQLresult_photos))) {
		$Xvars = array_merge ($Xvars, $l_photo);

		if ($loop === 0) return 'TAG,ACR,LOOP'; 	// Le tag de début est émis une seule fois
		else return 'ACT,LOOP';
	}
	
	return 'EXIT' ;
} 
    
     
############################################################################### XML_boucle_objets ###           
function XML_ITEMS_LOOP ($loop, $attr, $Xaction) {       

	if ($loop === null) 'return';		// SORTIE tag de fin, sans affichage du tag
	
	global $Xvars;
  global $objetListe, $requete, $db, $tl_orga, $tl_perso;              
	static $resultat;

	// tag de début, lire la base
	if ($loop === 0) {
    $objetListe = array ();          

		$resultat = requete(    
			'SELECT * FROM (Collections, Acquisitions, Domaines, Etats, Etablissements, Periodes, Productions)    
				left join Machines on Machines.idmachine=Collections.idmachine    
				left join Documents on Documents.iddocument=Collections.iddocument    
				left join Logiciels on Logiciels.idlogiciel=Collections.idlogiciel     
				left join Designations on Designations.iddesignation=Machines.iddesignation    
				left join Etatfiches on Etatfiches.idetatfiche=Collections.idetatfiche          
				left join Services on Services.idservice=Machines.idservice       
				left join Langues on Langues.idlangue=Documents.idlangue   
				left join Typedocs on Typedocs.idtypedoc=Documents.idtypedoc 
				left join Typeillustrs on Typeillustrs.idtypeillustr=Documents.idtypeillustr 
				left join Codeprogs on Codeprogs.idcodeprog=Logiciels.idcodeprog    
				left join Langprogs on Langprogs.idlangprog=Logiciels.idlangprog    
				left join Supports on Supports.idsupport=Logiciels.idsupport    
				left join Systemes on Systemes.idsysteme=Logiciels.idsysteme     
			WHERE Acquisitions.idacquisition=Collections.idacquisition    
				and Domaines.iddomaine=Collections.iddomaine    
				and Etats.idetat=Collections.idetat
				and Etablissements.idetablissement=Collections.idetablissement   
				and Periodes.idperiode=Collections.idperiode    
				and Productions.idproduction=Collections.idproduction    
				and '.$requete.' ORDER BY idcollection');          

			if (mysqli_num_rows ($resultat) == 0)          
				 return;      		// SORTIE sur sélection d'objets vide (TAG, ACT par défaut)
	}            

    $dbdata=mysqli_fetch_assoc($resultat);             
    if ($dbdata == NULL) return 'EXIT';   // EXIT  fin de boucle  
    
		// Préparer les tableaux personnes et organismes
		$tl_orga = Compose_tl_orga ($dbdata['idcollection']);
		$tl_perso = Compose_tl_perso ($dbdata['idcollection']);
		
		// Préparer le tableau des variables
		$Xvars =  $dbdata;
		$Xvars['db'] = $db;
		

		// objetListe est utilisé pour créer les documents d'accompagnement de l'envoi.
    $objetListe[count($objetListe)] = $dbdata['idcollection']. ' : ' .          
        ($dbdata['titredoc'] ?       
        'Doc : '.$dbdata['titredoc'] :       
        ($dbdata['titrelog'] ? 'Logiciel : '. $dbdata['titrelog'] :       
        'Machine : ' . $dbdata['nom']));  
        
  	return 'ACT,LOOP';    // SORTIE avec bouclage
}   
     
?>
