<?php
# v10.7			130308	PhD		Remplacé Design_complete par Design_titre
#				130615	PhD		Transféré ici Tablestruct, Selection, ValParDefaut
# v10.8			130805	PhD		Transféré Tablestruct dans golbalfcts...
# v13.0			150707	PhD 	Introduction Mysqli
# v14.1			151031	PhD		Devient aff_liens indépendant de mod_liens, Reprise des traductions
# v14.2			151207	PhD		Utilisation de Xpose pour afficher aff_leg_liens.xml
# v15			160111	PhD		Ajout param Xvars dans interface fonctions XML_
# v16.5			170201	PhD		Remplacement variable de session par paramètres de profil
# v17.1			170502	PhD		Renvoyé MenuConsulter dans 'consulter'
# v25			190615	PhD		Supprimé inst debug 8
# v25.14		220714	PhD		Corrigé absence while dans XML_liste_familles (PHP 8)
# v26.4			230905	PhD		Traitement séparé liens fonctionnels, même famille, même lot; Ajouté class liens_erreur
# v26.6.02		250108	PhD		"Liens fonctionnels" devient "Arboresence hiérarchique"
###
#	Ce module analyse et affiche le graphique des liens pour un objet.
# L'affichage se fait au fil de l'analyse. 
# (L'automate Xpose ne serait pas adapté à ce traitement. Il n'est utilisé que pour l'affichage du tableau des légendes)
######

#=============================================================== Analyse_liens

function Analyse_liens($idcol, $t) {
/* Fonction principale de l'analyse.
	Elle appelle la recherche des racines, puis la descente de l'arborescence 
	(avec affichage) à partir de la première racine reconnue.
*/
	
	global $deja_vu, $t_select, $tab_racines, $tab_branches, $id_pivot, $Xvars;
	$t_select = $t;
	
	// Désignation de l'élément pivot
	$id_pivot = $idcol;

	$deja_vu = array ();
	$tab_racines = array ();
	
### Chercher les racines possibles de cet élémént
	Racines ($idcol, -1);

	// S'il n'y a qu'unseul élément et que le masquage est autorisé >>> sortie
	$select_famille = $t_select['select'];
	$res_count = Requete ("SELECT idcol1, idcol2 FROM Liens
												JOIN Familles ON Liens.idfamille = Familles.idfamille 
												WHERE Liens.idfamille IN ($select_famille)  
												AND (idcol1=$idcol OR idcol2=$idcol)");		
		
	//debug (255,'COUNT', mysqli_num_rows ($res_count));
	if ((mysqli_num_rows ($res_count) < 1) AND $t_select['fh']) return;		// >>>>>>>>>>>>>>>>>>>>> retour sans affichage


### Ouverture du tableau d'affichage
	$titre =  $t_select['titre'];
	echo "<div id='liens_box'>
		<h3> $titre </h3>
		<div id='liens_arbre'>";
	
	// On traite les racines dans l'ordre de rangement en base
	sort ($tab_racines);
	if ($num_racines = count ($tab_racines) > 1) {
		echo "<p class=liens_note>>>>".Tr ("Il y a plusieurs racines possibles",  'There are several possible roots')." : ";
		foreach ($tab_racines as $id) echo Nrinv ('Collections', $id, TRUE, "&menu=A_liens").", ";
		echo "</p>\n";
	}
	
	
### Déclencher l'arbre depuis chaque racine, afficher au fur et à mesure
	foreach ($tab_racines as $id_racine) {
		echo "<br />";
		$deja_vu = array ();
		$tab_branches = array();
		Arborescence ($id_racine, -1, "", 0);
		if ($num_racines) echo "<br /><hr />";
	}
		
### fin liens_arbre
	echo "</div>";
	
#=======================  Si tableau de composition = Afficher la légende à partir du modèle XML
	if (!$t_select['fh']){
		$liste_xml = Xopen ('./XML_modeles/aff_leg_liens.xml') ;
		Xpose ($liste_xml);
	}
#======================= 

### Fermer les boites
	echo "<hr class='spacer' />";
	echo "</div>";					// fin liens_box

	return;
}


#================================================================  Arborescence

function Arborescence($idcol, $idfamille, $commentlien, $niv) {
/* Cette fonction descend (et affiche) toute l'arborescence à partir d'une racine donnée 
*/

	global $deja_vu, $nb_branches, $t_select, $tab_branches;
	
### Affichage de l'élément courant
	Affichage ($idcol, $idfamille, $commentlien, $niv);

### Se protéger contre les arborescences trop longues, et contre les boucles
	$niv += 1;
	if ($niv >= DB_nbr_niveaux_max) {
		echo "<p class=liens_note>>>> ".Tr ("Nombre max de niveaux affichés atteint", 'Maximum number of displayed levels reached ')." </p>";	
		 return;
	}
	
	$deja_vu[] = $idcol;
	$select_famille = $t_select['select'];
	
### Appel de la table
	$resultat = Requete ("SELECT idcol2, Liens.idfamille, commentlien FROM Liens, Familles WHERE 	
											Liens.idfamille=Familles.idfamille AND Liens.idfamille IN ($select_famille)  AND idcol1=$idcol ");		
		
	// Enregistrer le nombre de branches à ce niveau, si aucun, retour
	$nb_branches = mysqli_num_rows ($resultat);
	
	if ($nb_branches != 0) {
		$tab_branches[$niv] = $nb_branches;
		
		// et relancer le même traitement pour chacun des éléments du niveau inférieur
		while ($ligne = mysqli_fetch_array ($resultat)) {
			if (!in_array ($ligne['idcol2'], $deja_vu)) 
				Arborescence ($ligne['idcol2'], $ligne['idfamille'], $ligne['commentlien'],$niv);
		}
	}

	return;
}

//=====================================================================  Racines

function Racines ($idcol, $nniv) {
/*	Cette fonction remonte aux racines de l'arborescence à partir d'un objet donné.
	Elle construit un tableau $tab_racines des racines reconnues.	
*/

	global $deja_vu, $tab_racines, $t_select, $SQL_famille;
	
### Se protéger contre les arborescences trop longues, et contre les boucles
	$nniv += 1;
	if ($nniv >= DB_nbr_niveaux_max) {
		echo "<p class=liens_note>>>>".Tr ("Limite de montée atteinte", 'Go-up limit reached')."</hp>\n";	// à compléter ???
		$tab_racines[] = $idcol;
	return;
	}
	
	$deja_vu[] = $idcol;

### Appel de la table
	$select_famille = $t_select['select'];
	$resultat = Requete ("SELECT idcol1 FROM Liens, Familles WHERE 
											Liens.idfamille=Familles.idfamille AND Liens.idfamille IN ($select_famille) AND idcol2=$idcol");;

	$nb_resultat = mysqli_num_rows($resultat);
	
	// S'il n'y a pas d'autre réponse, l'élément courant est une racine
	if ($nb_resultat == 0) $tab_racines[] = $idcol;
	
	// sinon, remonter d'un niveau
	else {
		while ($ligne = mysqli_fetch_assoc ($resultat)) {
			if (in_array ($ligne['idcol1'], $deja_vu)) {		// éléments déjà vu (boucle)
				$tab_racines[] = $ligne['idcol1'];
				echo "<p class=liens_erreur>>>> ".Tr ("Boucle sur objet : ",  'Loop detected on this object:')
					.Nrinv ('Collections',$ligne['idcol1'], TRUE, "&menu=A_liens")."\n";
			} else {
				Racines ($ligne['idcol1'], $nniv);
			}
		}
	}
	
	return;
}

//================================================================  Affichage

function Affichage($idcol, $idfamille, $commentlien, $niv) {
/* Affiche la ligne courante de l'arborescence.
*/

	global $id_pivot;

### Appel de la base pour obtenir la désignation de l'élément courant
	$nrinv = Nrinv ('Collections', $idcol, TRUE, "&menu=A_liens");
	$design = Design_titre ($idcol);

### Mise en forme de la ligne imprimée

  //indentation et couleur en fonction du niveau et de la famille
  // sauf pour l'élément racine ($niv=0)
  $present = ($niv) ? Construit_lien ($niv, $idfamille) : "<div>";


  // Mise en évidence de la variable choisi par l'utilisateur au départ  $sp1 =""; $sp2 = "";
 	$sp1 = $sp2 = "";
 	if ($idcol==$id_pivot) {
     	$sp1 = "<span class='en_cours'>";
     	$sp2 = "</span>";
     }

  //Composition de la ligne complète
  	if ($commentlien != "") 
  		$commentlien ="&nbsp;&nbsp;&nbsp; <span style='font-style:italic; font-size:90%; color:black;'> ($commentlien) </span>";
	echo $present."$nrinv $sp1-- $design $commentlien $sp2 </div>\n";

  return;
}

//============================================================= construit_lien
function Construit_lien($niv, $idfamille) {
/* La fonction Construit_lien permet de réaliser l'indentation nécessaire en
	fonction du niveau et de mettre la fléche en couleur en fonction de la famille
	de l'objet
*/

	global $tab_branches;
debug (2, "niv",$niv);	
debug (2, "tab_branches-initial",$tab_branches);
	
	$present="";
	$i=1;
	
// Définition du nombre d'espace que l'on veut pour chaque indentation
	$espace="&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";   //5 espaces
	
// On boucle tant qu'on a pas atteint le niveau actuel
	while ($i<$niv) {
debug (2, "tab_branches",$tab_branches);
		//Si on a encore des branches au niveau i, on met une barre verticale
		if ($tab_branches[$i]>0){
			$present=$present."|".$espace.$espace;
		} else {
			$present=$present.$espace.$espace;
		}
		$i=$i+1;
	}
	
// Lorsqu'on atteint le niveau niv on met une "jolie" flèche
	$present=$present."|----->&nbsp;&nbsp;";
	$tab_branches[$i] -= 1;	// et on décompte le nbre de branches
	
// Enfin on colore la flèche selon la famille
	$present="<div class='color$idfamille'>$present";
	
// Renvoi du résultat de la fonction
	return $present;
}


// =================================================================== XML_liste_familles
function XML_liste_familles ($loop, $attr, $Xaction) {
	global $Xvars;

	if ($loop === null) return;		// tag de fin	

	if ($loop == 0) {							// Premier appel, lire la table
			$Xvars['result_fam'] = requete ("SELECT * FROM Familles");
	}
	
	//  Appel de la ligne courante
	while ($ligne = mysqli_fetch_assoc ($Xvars['result_fam'])) {
		$Xvars['idfamille'] = $ligne['idfamille'];
		$Xvars['color_idfamille'] = 'color'.$ligne['idfamille'];
	
		if ($_SESSION['langue']=='EN' AND $ligne['famille']!='') $Xvars['famille'] = $ligne['famille_en'];
		else 	$Xvars['famille'] = $ligne['famille'];

		return ($ligne) ? 'ACT,LOOP' : 'EXIT' ;
	}
} 

##############################################################
# EXECUTION :  Afficher l'arbre des liens
##############################################################

// Debut () et MenuConsulter () sont déjà traités dans "consulter.php"

$designation = Design_titre ($idcollection);
$Xvars =array();										// Nettoyer le tableau des variables

### Tableau des sélections et des affichages – Composé sans changer la BdD !
		// Base de la table 
		$TT_select = array (
			array ("titre" => Tr("Liens fonctionnels", 'Functional tree'), 'select' => '', 'fh' => false ),
		);
		// Lire la table et la parcourir
		$listid ='';
		$res_fam = Requete ("SELECT * FROM Familles");
		while ($ligne = mysqli_fetch_assoc ($res_fam)) {
			if ($ligne['oriente'] == 'oui') $listid .= $ligne['idfamille'].',';
			else $TT_select[] = array ('titre'=>TR( $ligne['famille'], $ligne['famille_en']), 
			'select'=>$ligne['idfamille'], 'fh'=>true);
		}	
		// Reporter la sélection des liens hiérachiques
		$TT_select[0]['select'] = rtrim ($listid, ',');
		
### Exécuter pour chaque type de lien
foreach ($TT_select as $n=>$v) Analyse_liens($idcollection, $TT_select[$n]);

Fin ();
?>