<?php
# v16.0				161123	PhD		Création : fonctions propres au récolement
# v16.2				161212	PhD		Changement denom consulter_recol_inc -> aff_recol.inc, regroupé les js dans consulter.js
# v20					170930	PhD		Étendu à 10 marques récolement
# v21					171123	PhD		Ajouté tnposition, supprimé nouvrange
# v25.4				191205	PhD		Ajout Tabuler, Valpond, XML_lib_marques et XML_affdix...
# v25.5				191223	PhD		Ré-écriture de Valpond, modification XML_affdix, XML_mod_groupes
# v25.5.01		200114	PhD		Déplacement ici des fonctions Act_id et Crit_id
# v25.6.01		200215	PhD		Ajout NegRedCyan sur tm[9]
# v25.6.02		200219	PhD		Ajout appel miseaJour dans Act_id et Crit_id
###
# Ce module est chargé en include par 'consulter', 'rechercher', 'rappel_rech', 'rappel_rech_recol'
# SI le mode récolement est actif pour assurer l'affichage des champs spécifiques au récolement
#
# Les fonctions sont utilisées dans 'mod_recol.xml', 'aff_recol.xml' et 'aff_recol_desh'
#
################################################################################# Act_id ####
function Act_id ($act_id, $idtrecol, $idtnposition) 
# Affectation du critère dominant et de la nouvelle position si valeur numérique (non NULL)
# sinon maintien de la valeur enregistrée
{
	global $droits;

	$set_trecol = (is_numeric ($idtrecol)) ? "idtrecol=$idtrecol" : '';
	$set_tnposition = (is_numeric ($idtnposition)) ? "idtnposition=$idtnposition" : '';
	$set_virg = (is_numeric ($idtrecol) AND is_numeric ($idtnposition)) ? ',' : '';
	
	if (!empty ($set_trecol) OR !empty ($set_tnposition)) {
		if (in_array ('recol_verrou', $droits))	requete ("UPDATE Collections
			SET $set_trecol $set_virg $set_tnposition WHERE idcollection=$act_id");
		else 	requete ("UPDATE Collections
			SET $set_trecol $set_virg $set_tnposition WHERE idcollection=$act_id AND m10=0");
		miseaJour ($act_id);
	}
}
################################################################################# Crit_id ####
function Crit_id ($crit_id, $tm) 
# Affectation des 10 marques si elles ont une valeur non nulle
# sinon maintien de la valeur enregistrée
{
	global $droits;
	
	$SQLresult = Requete ("SELECT m1, m2, m3, m4, m5, m6, m7, m8, m9, m10 FROM Collections 
											WHERE idcollection=$crit_id");
	$ligne = mysqli_fetch_assoc ($SQLresult);
	
	// Tabuler les critères lus et affecter les critères non nuls
	$xm = Tabuler ($ligne, 'm');
	for ($i = 0; $i <= 9; $i++) {
		$x[$i] = ($tm[$i] != NULL) ? $tm[$i] : $xm[$i];
	}
	
	if (in_array ('recol_verrou', $droits)) requete ("UPDATE Collections 
							SET m1=$x[0], m2=$x[1], m3=$x[2], m4=$x[3], m5=$x[4], m6=$x[5], m7=$x[6], m8=$x[7], m9=$x[8], m10=$x[9]
							WHERE idcollection=$crit_id");
	else requete ("UPDATE Collections 
							SET m1=$x[0], m2=$x[1], m3=$x[2], m4=$x[3], m5=$x[4], m6=$x[5], m7=$x[6], m8=$x[7], m9=$x[8]
							WHERE idcollection=$crit_id AND m10=0");							
	miseaJour ($crit_id);
}

##################################################################### Tabuler ###
function Tabuler ($ligne, $val) 
# Lire les 10 valeurs de marques $ligne[$val.1] et les tabuler dans $tm[0]...
# Attention : les critères dans la base vont de 1 à 10, mais toutes les tables vont de 0 à 9
{	

	$tm = array ();
	for ($i = 0; $i <= 9; $i++) {
		$tm[$i] = $ligne[$val.($i+1)];
	}
	return $tm;
}
##################################################################### ValPond ###
function ValPond () 
# La fonction calcule la somme des 10 marques tm[] pondérées par les coefficients de pondération
# transmis en variable de session
# $_SESSION['km'] <= coefficents définis par le paramètre "poids_tmarques" à l'ouverture de session 
# $_SESSION['kr'] <= coefficents définis par le paramètre "poids_trecols" à l'ouverture de session 
# Retourne une chaine de caractères rouge si négatif
{
	global $Xvars;
	
	$km = $_SESSION['km'];

	$sum = $_SESSION['kr'][$Xvars['idtrecol']-1];
	for ($i = 0; $i <= 9; $i++) {
		$sum += $Xvars['tm'][$i] * $km[$i];
	}
	return NegRed ($sum);
}
###################################################################### XML_affrecol ###
function XML_affrecol ($loop, $attr, $Xaction) 
# Lecture des champs spécifiques au récolement
# – dans l'affichage complet pour séparer de l'affichage des champs standards
# – dans l'écran de modification objet
# – dans la liste résultat localisation, pour la modification en ligne
###
{
	global $Xvars;
	if ($loop === null) return;		// tag final
	
	// Lire les champs variables propre au récolement
	$idcollection = $Xvars['idcollection'];
	$SQLresult_recol = requete (	
		" SELECT m1, m2, m3, m4, m5, m6, m7, m8, m9, m10, 
						Collections.idtrecol, trecol, Collections.idtnposition, tnposition
						FROM Collections, Trecols, Tnpositions
						WHERE Trecols.idtrecol = Collections.idtrecol
							AND Tnpositions.idtnposition = Collections.idtnposition
							AND Collections.idcollection = $idcollection");
  $ligne = mysqli_fetch_assoc($SQLresult_recol); 

	$tm = Tabuler ($ligne, 'm');
  $Xvars['tm'] = $tm; 
  $Xvars['trecol'] = $ligne['trecol'];@ $Xvars['idtrecol'] = $ligne['idtrecol'];
  $Xvars['tnposition'] = $ligne['tnposition']; @ $Xvars['idtnposition'] =$ligne['idtnposition'];
  
  // Mettre à blanc la table des libellés des 10 indicateurs (précaution si table Tmarques plus courte)
  for ($i=1; $i<=10; $i+=1) { $tmarques[$i] =''; }
  // Lire la table des libellés dans la base
	$SQLresult_indic = requete ("SELECT * FROM Tmarques");
	while ($ligne = mysqli_fetch_assoc ($SQLresult_indic)) {
		$tmarques[$ligne['idtmarque']] = $ligne['tmarque'];
	}
  $Xvars['tmarques'] = $tmarques;
    
	return 'ACT' ;
} 
     
###################################################################### XML_affrecol_ind ###
function XML_affrecol_ind ($loop, $attr, $Xaction) 
# Fournit un indice de boucle de 1 à 10 pour l'affichage des libellés des marques 1 à 10
###
{
	global $Xvars;
	
	$Xvars['i'] = $loop+1;
  // Tester la fin de boucle
  if ($loop <9) return 'ACT, LOOP';
  else return 'ACT' ;
} 
  
############################################################ XML_lib_marques ###
function XML_lib_marques ($loop, $attr, $Xaction) 
# Fournit un par un les libellés des tmarques pour affichage rech_resultat_desh.xml
{ 
	global $Xvars;
	static $SQLresult_lib;

	if ($loop === null) return;		// tag de fin
	
	// Si tag de début, appeler le tableau des libellés
	if ($loop === 0) $SQLresult_lib = requete ("SELECT * FROM Tmarques");
			
	//  Appel du libellé courant
	while ($ligne = mysqli_fetch_assoc ($SQLresult_lib)) { 
		$Xvars['tmarque'] = $ligne['tmarque'];

		return ($ligne) ? 'ACT,LOOP' : 'EXIT' ;
	}	
} 

############################################################ XML_affdix ###
function XML_affdix ($loop, $attr, $Xaction) 
# Affichage des 10 marques pour affichage rech_resultat_sel.xml
{ 
	global $Xvars;
	if ($loop === null) return;		// tag de fin
	
	$Xvars['mi'] = $loop;
	return ($loop<9) ? 'ACT, LOOP' : 'ACT' ;
     
} 

###################################################################### XML_aff_desh ###
function XML_aff_desh ($loop, $attr, $Xaction) 
# Lecture des champs spécifiques au récolement
# – dans l'affichage 'rech_resultat_desh' dans la ligne standard
###
{
	global $Xvars;
	if ($loop === null) return;		// tag final
	
	// Lire les champs variables propre au récolement
	$idcollection = $Xvars['idcollection'];
	$SQLresult_recol = requete (	
		" SELECT m1, m2, m3, m4, m5, m6, m7, m8, m9, m10, 
						Collections.idtrecol, trecol, Collections.idtnposition, tnposition
						FROM Collections, Trecols, Tnpositions
						WHERE Trecols.idtrecol = Collections.idtrecol
							AND Tnpositions.idtnposition = Collections.idtnposition
							AND Collections.idcollection = $idcollection");
  $ligne = mysqli_fetch_assoc($SQLresult_recol); 
  
  $Xvars['trecol'] = $ligne['trecol'];
  $Xvars['tnposition'] = $ligne['tnposition'];
  
	$tm = Tabuler ($ligne, 'm');
  $Xvars['tm'] = $tm; 
  
	$Xvars['ligne_criteres'] =	NegRed($tm[0]).' '.NegRed($tm[1]).' '.NegRed($tm[2]).' – '.NegRed($tm[3]).
		' '.NegRed($tm[4]).' '.NegRed($tm[5]).' – '.NegRed($tm[6]).' '.NegRed($tm[7]).' '.NegRed($tm[8]).
		' – – '.NegRedCyan($tm[9]);
    
	return 'ACT' ;
} 
     
########################################################################################

?>
