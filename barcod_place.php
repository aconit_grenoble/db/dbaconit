<?php
# v22.1			180613	PhD		Création
# v22.2			180825	PhD		Retouches
# v23				180830	PhD		Intégration du module "mode de validation", chgt nom barcod_recol=>barcod_place
# v25.8			200411	PhD		Ajout custom_css
# v25.10		210210	PhD		Ajout traitement No inventaire mixte
###

$custom_css = "barcod_place.css";
require_once ('init.inc.php');		// Initialisations, identification..


########################################################### Bar_recol_register ###
function Bar_recol_register ($idcollection, $coterange) {
# Enregistre la nouvelle position de l'objet

  requete ("UPDATE Collections SET coterange='$coterange', daterange=".date ('Ymd')
  				." WHERE idcollection = $idcollection");
	miseaJour ($idcollection);
	Message (Tr ("Nouvelle  cote de rangement enregistrée", "New location recorded"));
  return;
}   

##################################################################################

/* Protection des entrées -------------------------------------------------------
'action'				- POST - uniquement testé switch
'f_auto_valid'	– POST – uniquement testé isset
'sinput'				– POPST – filtré NormIn
------------------------------------------------------------------------------ */

## Traitement des entrées :
###########################

$action = @$_POST['action'];
if (!$action) $action = 'empty';
$sinput = NormIn ('sinput');
$f_auto_valid = isset ($_REQUEST['f_auto_valid']);


#  TRAITEMENT - SAISIE DES POSITIONS ET DES No D'INVENTAIRE  
###########################################################
$Xvars =array();				// Purger le tableau des variables

$design_titre = '';
$nrinv				= '';
$nrinventaire	= '';

$audio 			='';
$id_err			= FALSE;
$man_valid	= 'attente';
	
### Si c'est le premier appel, fixer les valeurs par défaut dans le tableau session
if (!isset ($_SESSION['bar_mode_auto_valid'])) $_SESSION['bar_mode_auto_valid'] = FALSE;
if (!isset ($_SESSION['bar_recol'])) {
	$_SESSION['bar_recol']['coterange']			= '???';
}

switch ($action) {
case 'empty':
#############
	break;
	
case 'entree':
##############
  if ($sinput != '') {
		// Remplacer les codes § par -
		$sinput = str_replace ('§', '-', $sinput);
  	
#### S'il y a une lettre dans les 4 premiers caractères, c'est une cote de rangement
  	if (ctype_alpha (substr ($sinput, 0, 1)) OR ctype_alpha (substr ($sinput, 1, 1)) 
  			OR ctype_alpha (substr ($sinput, 2, 1)) OR ctype_alpha (substr ($sinput, 3, 1))) {
  		$_SESSION['bar_recol']['coterange'] = $coterange = $sinput;
			$audio = 'select';
  		
		} else {
#### Sinon ce doit être un numéro d'inventaire
			// Séparer idetablisement et nrinv
			$n = strpos ($sinput, K_SEP);

			if ($n != 0) {
				$idetablissement = substr ($sinput, 0, $n);
				$nrinv = substr ($sinput, $n+1);
			}	else {
				$idetablissement = $dbase['etablissement_defaut'];
				$nrinv = $sinput;
			}
			
			$result = requete ("SELECT Collections.idcollection, etablissement, prefinv, nrinv
				FROM Collections
				LEFT JOIN Etablissements on Etablissements.idetablissement=Collections.idetablissement
				left join Machines on Machines.idmachine=Collections.idmachine 
				left join Documents on Documents.iddocument=Collections.iddocument     
				left join Logiciels on Logiciels.idlogiciel=Collections.idlogiciel
				WHERE Collections.idetablissement='$idetablissement' AND nrinv='$nrinv'");
	
			if (mysqli_num_rows ($result)) {
				$ligne = mysqli_fetch_assoc ($result);
				$_SESSION['idcollection'] = $idcollection = $ligne['idcollection'];
				$nrinventaire = Nrinventaire (0, $ligne);
				$design_titre = Design_titre ($idcollection);
				$id_err = FALSE;
				// si auto_valid, enregistrer immédiatement la position
					if ($_SESSION['bar_mode_auto_valid']) {
						Bar_recol_register ($idcollection, $_SESSION['bar_recol']['coterange']);
						$audio = 'succes';
					} else {
						$man_valid = 'pret';
						$audio = 'select';
					}
				
			} else {
				$design_titre = '';
				$id_err = TRUE;
				$audio = 'echec';
			}
  	} 	
  }  
  break;
	
case 'valid':
#############
	Bar_recol_register ($_SESSION['idcollection'], $_SESSION['bar_recol']['coterange']);
	$man_valid = 'fait';
	$audio = 'succes';
	break;
	
case 'mode':
############
  if ($_SESSION['bar_mode_auto_valid'] != $f_auto_valid) 
  		Message (Tr ("Changement de mode de validation", "Validation mode changed"));
  $_SESSION['bar_mode_auto_valid'] = $f_auto_valid;
  break;
}    

# AFFICHAGE ÉCRAN PRINCIPAL  
#############################

Debut ();

$Xvars['audio']					= $audio;
$Xvars['design_titre'] 	= $design_titre;
$Xvars['id_err']		 		= $id_err; 
$Xvars['man_valid'] 		= $man_valid;
$Xvars['nrinv'] 				= $nrinv;	
$Xvars['nrinventaire']	= $nrinventaire; 
$Xvars['sinput']				= $sinput;

$Xvars['auto_valid'] 	= $_SESSION['bar_mode_auto_valid'];
$Xvars['coterange'] 	= $_SESSION['bar_recol']['coterange'];

$liste_xml = Xopen ('./XML_modeles/barcod_place.xml') ;
Xpose ($liste_xml);

Fin ();
?>

