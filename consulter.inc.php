<?php
# v20				171001	PhD		Ajouté M9, m10, notes dans 'SauvObjet'
# v21				171123	PhD		Ajouté idtnposition, supprimé nouvrange
# v22.0			180410	PhD		Ajout 'mode_mgsm'
# v22.1			180609	PhD		Déplacé 'Design-titre' vers 'globalfcts'
# v24				190221	PhD		RàZ indic. feuille de don, suppr 'mode_mgsm', ajout Batiments, Localisation=>Etablissement
#														ajout Nrinventaire dans XML_affphotos
# v24.0.01	190226	PhD		Correction test nrinv unique
# v25				190615	PhD		Affdimenions : ajouté comdim et mode abrégé, Table Categories devient Domaines
# v25.3			190914	PhD		Ajouté traitement de 'pubreserv', Fonction Batiment utilise fonction Cotebatiment, 
#														ajout dbase, supprimé appels Trim_nr
# v25.4			191205	PhD		Corrigé 'Sauv_Objet' pour cas récolement : pas recherche doublon, ne pas traiter feuilledon
#													 complété messages 'DIE'
# v25.5			200107	PhD		Simplifié 'Sauv_Objet' pour cas récolement
# v25.5.01	200114	PhD		Repris fortement 'Sauv_Objet' pour les différents cas récolement
# v25.7			200317	PhD		Sorti code Prepar_photo dans globalfcts, bloquer affichage vignette en mode modification
# v25.10.01	210222	PhD		Ajouté global dbase dans MenuConsulter
# v25.14		220716	PhD		Corrigé fonctions Affobjet et Typa pb arguments nul dans strlen et pregreplace (PHP 8.1)
# v25.14.01	220802	PhD		Ajouté 'try' 'catch' dans Verrouiller (PHP 8.1)
# v26.2.01	230427	Phd		Modifié 'XML_affphotos' pour lire 'cotemedia' 
# v26.5			231223	PhD		RàZ indic ITinHeritage
###

#============================================================= AffDimensions ===

function AffDimensions ($abrev=FALSE) {
   // Retourne une liste contenant toutes les données dimension non vides

	global $Xvars;

	if ($abrev){
		if ($_SESSION['langue'] == 'EN') 
			$t = array('L' => 'L', 'l' => 'w', 'H'	=> 'H', 'D' => 'D', 'P' => 'W',	'C' => 'C');
		else $t = array('L' => 'L', 'l' => 'l', 'H'	=> 'H', 'D' => 'D', 'P' => 'P',	'C' => 'C'); 	
	} else {
		if ($_SESSION['langue'] == 'EN') 
			$t = array('L' => 'Length:', 'l' => 'width:', 'H'	=> 'Height:', 'D' => 'Diameter:', 'P' => 'Weight:',	
				'C' => 'Color:');
		else 
			$t = array('L' => 'Longueur', 'l' => 'largeur', 'H'	=> 'Hauteur', 'D' => 'Diamètre', 'P' => 'Poids',	
				'C' => 'Couleur');
	}
	
	$dimlong = $Xvars['dimlong'];
	$dimlarg = $Xvars['dimlarg'];
	$dimhaut = $Xvars['dimhaut'];
	$dimdiam = $Xvars['dimdiam'];
	$dimpoids = $Xvars['dimpoids'];
	$couleur = $Xvars['couleur'];
	$comdim = $Xvars['comdim'];
	
	$st ="";	
	$st .= ($dimlong != 0) ? '<b>'.$t['L'].' </b>'.$dimlong.' cm , ' : '';
	$st .= ($dimlarg != 0) ? '<b>'.$t['l'].' </b>'.$dimlarg.' cm , ' : '';
	$st .= ($dimhaut != 0) ? '<b>'.$t['H'].' </b>'.$dimhaut.' cm , ' : '';
	$st .= ($dimdiam != 0) ? '<b>'.$t['D'].' </b>'.$dimdiam.' cm , ' : '';
	$st .= ($dimpoids != 0) ? '<b>'.$t['P'].' </b>'.$dimpoids.' kg , ' : '';
	$st .= ($couleur != '') ? '<b>'.$t['C'].' </b>'.$couleur.' , ' : '';
	$st .= ($comdim != '') ? '<b>'.Tr (' Note : ', ' Note: ').'</b>'.$comdim : '';
	return $st;	
}   
      
#============================================================== AffMotsCles ===

function AffMotsCles ($idcollection) {
   // Retourne une liste contenant tous les mots clés
      
   $resultmot = requete ("SELECT motcle, motcle_en
      FROM Col_Mot, Motscles
      WHERE Col_Mot.idmotcle = Motscles.idmotcle
        AND Col_Mot.idcollection = $idcollection ");

   if (0 == ($nblignes = mysqli_num_rows ($resultmot)))
      return Tr ("Pas de mots clés pour cet élément...", 'No keywords for this item ...');
   
   // Si il y a des résultats
     $txt ="";
     while ($lignemot = mysqli_fetch_assoc ($resultmot)) {
     	$mot = Snv ( $lignemot['motcle'], $lignemot['motcle_en']);     // affichage en anglais
        $txt .= ltrim ($mot, '-')."&nbsp;&nbsp;-&nbsp;&nbsp;";
    } 
       
     return $txt;
}

          
#============================================================== AffMateriaux ===

function AffMateriaux ($idcollection) {
   // Retourne une liste contenant tous les matériaux
      
   $resultmate = requete ("SELECT materiau, materiau_en
      FROM Col_Mate, Materiaux
      WHERE Col_Mate.idmateriau = Materiaux.idmateriau
        AND Col_Mate.idcollection = $idcollection ");

   if (0 == ($nblignes = mysqli_num_rows ($resultmate)))
      return Tr ("Pas de matériaux spécifiés pour cet élément...", 'No identified materials for this item ...');
   
   // Si il y a des résultats
     $txt ="";
     while ($lignemate = mysqli_fetch_assoc ($resultmate)) {
     	$mat = Snv ( $lignemate['materiau'], $lignemate['materiau_en']);     // affichage en anglais
    	$txt .= ltrim ($mat, '-').";&nbsp;&nbsp;";
    }
       
     return $txt;
}

#================================================================== AffObjet ===

function Affobjet ($nom, $table, $idcollection) {
// $nom		: nom du menu Résumé ou Complet
// $table	: nom de la table Machines, Documents ou Logiciels
// $idcollection	: index de l'objet courant

	global $Xvars;	// Reçoit et transmet l'ensemble des variables utilisées 
	global $db, $dbase, $droits, $dbl, $url_galerie;

	// Lire la base
	$resultat = Reqobjet ($table, $idcollection);
  $Xvars = mysqli_fetch_assoc($resultat);     
	// Remplacer les tables d'ordres xxx par xxx_en si affichage anglais
	if ($_SESSION['langue'] == 'EN') Tr_en ($table);
    
	$Xvars['table'] = $table;	// ajouter l'indication de la table traitée
	$Xvars['droits'] = $droits;
	$Xvars['dbase'] = $dbase;
	$Xvars['dbl'] = $dbl;		// suffixe anglais
	
	// Si l'objet est installé dans une galerie virtuelle, calculer un lien
	if ($Xvars['museevirtuel'] === NULL) $nrsalle_virt =0;
	else $nrsalle_virt =substr ($Xvars['museevirtuel'], 0, 3);
	if ($nrsalle_virt>0 AND $nrsalle_virt<900) 
		$Xvars['urlsalle'] = $url_galerie.$db."&nsal=".$nrsalle_virt;
	else $Xvars['urlsalle'] = '';

#=== Afficher à partir du modèle XML
	switch ($nom) {
		case 'Résumé' :
			$liste_xml = Xopen ('./XML_modeles/aff_resume.xml') ;
			Xpose ($liste_xml);
		break;
	
		case 'Complet' :
			$liste_xml = Xopen ('./XML_modeles/aff_complet.xml') ;
			Xpose ($liste_xml);	
	}
}

#=============================================================== Batiment ===

function Batiment ($coterange) 
# Extrait la ligne de la table Batiments en fonction de l'institution et de la cote de rangement
# Retourne la chaîne de caractères à afficher
{  global $Xvars;
 
# Extraire les éléments du critère de recherche, 
 	$nb_segments = $Xvars['nb_segments'];		// Nombre de segments de la cote de rangement qui désignent le batiment
	$prefixe = Cotebatiment ($Xvars['nb_segments'], $coterange);

	$idetablissement =$Xvars['idetablissement'];
	$where = "idetablissement = $idetablissement AND cotebatiment LIKE '$prefixe'";

	// Recherche en base
	$resultat = Requete ("SELECT batiment FROM Batiments WHERE $where");
												
	if (!mysqli_num_rows($resultat)) return '--';
	else {
		$ligne = mysqli_fetch_assoc ($resultat);		// S'il y avait plusieurs réponses, c'est la première qui est prise...
		return $ligne['batiment'];
	}    
}  
    
#=============================================================== MenuConsulter ===

function MenuConsulter ($designation) {
    
    global $dbase, $Xvars;
    global $droits, $f_modif, $g_menu, $idcol, $idcollection, $menu;
	
	$Xvars['dbase'] = $dbase;		
	$Xvars['droits'] = $droits;
	$Xvars['idcollection'] = $idcollection;
	$Xvars['idcollections'] = @$_SESSION['idcollections'];
	$Xvars['menu'] = $menu;

	// Préparation de l'affichage les éléments Début/Fin/Précédent/Suivant  
   if (isset ($_SESSION['idcollections'])) {
      $Xvars['min'] = $_SESSION['idcollections'][0];
      $Xvars['max'] = $_SESSION['idcollections'][count ($_SESSION['idcollections'])-1];
   } else {
      $Xvars['max'] = maximum ('Collections');
      $Xvars['min'] = minimum ('Collections');
   }

	// Nombre de liens avec d'autres objets (pour message de confirmation d
	$SQLresult_liens = requete (	
		"SELECT idcol1 FROM Liens WHERE ( idcol1 = $idcollection OR idcol2 = $idcollection )" );
	$Xvars['nb_liens'] = mysqli_num_rows ($SQLresult_liens);

	//Éléments pour affichage partie principale
	$Xvars['design_titre'] = $designation;
	$Xvars['msg_abandon'] = Tr('Voulez vous abandonner les modifications ?', 'Abandon changes ?');

	
	$Xvars['Mflags'] = '';
	if ($f_modif) $Xvars['Mflags'] .= '&Modif';
	if ($g_menu) $Xvars['Mflags'] .= '&menu=' . urlencode ($menu);
  
   // Tables des boutons
   // "key" code pour traitement "lab" label du bouton, "droit" droit d'accès nécessaire, "style" couleur du bouton
   // À NOTER QUE : padding réduit non traité dans button Firefox, classes non traitées dans button Chrome...
	$Xvars['boutons'] = array (
		array ("key"=>"A_resume", "lab"=>Tr('Résumé','Summary'), "droit"=>"aff_resume", "style"=>'padding:2px;'),
		array ("key"=>"A_complet", "lab"=>TR('Complet','Complete'), "droit"=>"aff_complet", "style"=>'padding:2px;'),
		array ("key"=>"A_liens", "lab"=>Tr('Liens','Links'), "droit"=>"aff_resume", "style"=>'padding:2px;'),
		array ("key"=>"M_objet", "lab"=>Tr('Modifier','Modify'), "droit"=>"mod_objet", "style"=>'padding:2px; color: red'),
		array ("key"=>"M_orga", "lab"=>Tr('Organisme','organization'), "droit"=>"mod_objet", 
			"style"=>'padding:2px; color: red'),
		array ("key"=>"M_perso", "lab"=>TR('Personne','Person'), "droit"=>"mod_objet", 
			"style"=>'padding:2px; color: red'),
		array ("key"=>"M_medias", "lab"=>"Medias", "droit"=>"mod_objet", "style"=>'padding:2px; color: red'),
		array ("key"=>"M_mots clés", "lab"=>Tr('Mots clés', 'Keywords'), "droit"=>"mod_objet", 
			"style"=>'padding:2px; color: red'),
		array ("key"=>"M_matériaux", "lab"=>Tr('Matériaux','Materials'), "droit"=>"mod_objet", 
			"style"=>'padding:2px; color: red'),
		array ("key"=>"M_mouvements", "lab"=>Tr('Mouvements','Moves'), "droit"=>"mod_objet", 
			"style"=>'padding:2px; color: red'),
		array ("key"=>"M_liens", "lab"=>TR('Liens','Links'), "droit"=>"mod_objet", "style"=>'padding:2px; color: red'),
		array ("key"=>"M_type", "lab"=>"Type", "droit"=>"mod_objet", "style"=>'color: red'),		
		array ("key"=>"SUP_objet", "lab"=>TR('SUP.obj','DEL.obj'), "droit"=>"sup_objet", "style"=>'padding:2px; color: red')		
	);
	
	// Constantes à base de <>
	$Xvars['k'] =array (
		'lt2' => '&nbsp;<b>&lt;&lt;</b>', 'gt2' => '<b>&gt;&gt;</b>&nbsp;', 
		'lt1' => '&nbsp;<b>&lt;</b>', 'gt1' => '<b>&gt;</b>&nbsp;' );

#=== Afficher à partir du modèle XML
	$liste_xml = Xopen ('./XML_modeles/menu_objet.xml') ;
	Xpose ($liste_xml);
	$Xvars = array ();						// Purger la table des variables avant affichage de la fiche
}

#================================================================== ModObjet ===

function Modobjet ($table, $idcollection) {
// $nom		: nom du menu 
// $table	: nom de la table Machines, Documents ou Logiciels
// $idcollection	: index de l'objet courant

	global $droits, $Xvars;	// Reçoit et transmet l'ensemble des variables utilisées 

	// Création d'une table de structure simplifiée
	$tstruct = Tstructure (array ('Collections', $table));
	 
	// Lire la base
	$resultat = Reqobjet ($table, $idcollection);

    $Xvars = mysqli_fetch_assoc($resultat);             
	$Xvars['tstruct'] = $tstruct;	// ajouter la table de structure
	$Xvars['table'] = $table;		// ajouter l'indication de la table traitée
	$Xvars['droits'] = $droits;
	
	//Empècher l'affichage d'une vignette photo dans le bloc mod_recol
	$Xvars['nb_medias'] = 0;
	
#=== Afficher à partir du modèle XML
		$liste_xml = Xopen ('./XML_modeles/mod_objet.xml') ;
		Xpose ($liste_xml);
}

#=============================================================== NormINPUT ===
function NormINPUT () {
# Normalisation NormIN de l'ensemble des résultats d'un formulaire
	global $INPUT;
	foreach ($_POST as $nom => $val) { 
		if (is_array ($val)) {
			foreach ($val as $val_1) {
				if (!is_numeric ($val_1)) DIE ("NormINPUT - Valeur «$val_1» numérique incorrecte")	;     //>>>>>>>>>> DIE
			}
			$INPUT[$nom] = $val;								// cas des tableaux (idcol, tm...)
			
		} else {
			$str = htmlspecialchars (trim ($val), ENT_QUOTES, 'utf-8'); 
			$INPUT[$nom] = $str;								// traitement type NormIN sur les éléments simples								
		}
	} 
	debug (0x40, 'INPUT', $INPUT);
	return;
}	

#================================================================== ReqObjet ===

function Reqobjet ($table, $idcollection) {
# $table	: nom de la table Machines, Documents ou Logiciels
# $idcollection	: infdex de l'objet courant
#
# Lecture des champs de la table Collections et de l'une des tables Machines, Documents ou logiciels
# ainsi que des champs des tables d'ordre
###

	// Partie commune de la requète BdD
	$req_where = 'WHERE Acquisitions.idacquisition=Collections.idacquisition    
			  and Domaines.iddomaine=Collections.iddomaine    
			  and Etats.idetat=Collections.idetat
			  and Etablissements.idetablissement=Collections.idetablissement    
			  and Periodes.idperiode=Collections.idperiode    
			  and Productions.idproduction=Collections.idproduction    
			  and Etatfiches.idetatfiche=Collections.idetatfiche   
			  and Collections.idcollection ='. $idcollection;          

	// Requète selon type de table
	switch ($table) {
		case 'Machines' :
			$req = 'SELECT * FROM (Collections, Acquisitions, Domaines, Etats, Etablissements, 
				Periodes, Productions, Etatfiches)    
			  left join Machines on Machines.idmachine=Collections.idmachine    
			  left join Designations on Designations.iddesignation=Machines.iddesignation    
			  left join Services on Services.idservice=Machines.idservice '
			  .$req_where;       
		break;

		case 'Documents' :
			$req = 'SELECT * FROM (Collections, Acquisitions, Domaines, Etats, Etablissements, 
				Periodes, Productions, Etatfiches)    
			  left join Documents on Documents.iddocument=Collections.iddocument    
			  left join Langues on Langues.idlangue=Documents.idlangue   
			  left join Typedocs on Typedocs.idtypedoc=Documents.idtypedoc 
			  left join Typeillustrs on Typeillustrs.idtypeillustr=Documents.idtypeillustr '
			  .$req_where;
		break;

		case 'Logiciels' :
			$req = 'SELECT * FROM (Collections, Acquisitions, Domaines, Etats, Etablissements, 
				Periodes, Productions, Etatfiches)    
			  left join Logiciels on Logiciels.idlogiciel=Collections.idlogiciel     
			  left join Codeprogs on Codeprogs.idcodeprog=Logiciels.idcodeprog    
			  left join Langues on Langues.idlangue=Logiciels.idlangue   
			  left join Langprogs on Langprogs.idlangprog=Logiciels.idlangprog    
			  left join Supports on Supports.idsupport=Logiciels.idsupport    
			  left join Systemes on Systemes.idsysteme=Logiciels.idsysteme '
			  .$req_where;
	}
	$resultat = requete($req);
	if (mysqli_num_rows ($resultat) == 0) DIE ('ReqObjet - Erreur idcollection ou erreur base');  
	
	return $resultat;
}

#========================================================================= SauvObjet ===

function Sauvobjet ($table, $idcollection) {
# Sauvegarder les champs modifiés
# $table	: indique la table particulière Machines ou Documents ou Logiciels associée à la table Collections
#						si $table == 'Recolement', il s'agit d'un appel par 'rappel_rech_recol' et seul les champs associés à la localisation sont traités
###

   global $debug, $droits, $INPUT;
 
	// Vérification du droit de modification, sauf dans le cas "récolement"
	// Il faut refaire un contrôle ici car le rapporteur pourait avoir modifié la rubrique organisme 
	// et attribué l'objet à un organisme pour lequel ce rapporteur n'est pas habilité.
	if (($table != 'Recolement') && !Autor_modif($idcollection, $INPUT['idetablissement'])) return; // >>>> EXIT
	
	// Préparer une table de correspondance table - id
	$t_nomid = array ('Collections' => 'idcollection', 
				'Machines' => 'idmachine', 'Documents' => 'iddocument', 'Logiciels' => 'idlogiciel');

	// Préparer une table des noms des champs, avec nom de la table
	$tstruct = array ();
	$result = requete (" SHOW COLUMNS FROM Collections ");
	while ($ligne = mysqli_fetch_assoc ($result)) $tstruct[$ligne['Field']] = 'Collections';

	if ($table != 'Recolement') {
		$result = requete (" SHOW COLUMNS FROM $table ");
		while ($ligne = mysqli_fetch_assoc ($result))  $tstruct[$ligne['Field']] = $table;
	}

# Lire l'état actuel de l'objet en base -------------------------------------------
# Requête selon type de table
	switch ($table) {
		case 'Machines' :
			$req = 'SELECT * FROM Collections, Machines 
				WHERE Machines.idmachine=Collections.idmachine    
				and Collections.idcollection ='. $idcollection;          
		break;

		case 'Documents' :
			$req = 'SELECT * FROM Collections, Documents 
				WHERE  Documents.iddocument=Collections.iddocument    
				and Collections.idcollection ='. $idcollection;          
		break;

		case 'Logiciels' :
			$req = 'SELECT * FROM Collections, Logiciels
				WHERE  Logiciels.idlogiciel=Collections.idlogiciel     
				and Collections.idcollection ='. $idcollection;          
		break;

		case 'Recolement' :					// idtrecol, idtnposition et m1 à m10 sont traités à part
			$req = 'SELECT idcollection, lieurange, coterange, daterange, notes
				FROM Collections
				WHERE Collections.idcollection ='. $idcollection; 
	}
	$resultat = requete($req);
	if (mysqli_num_rows ($resultat) == 0) DIE ('SauvObjet - Erreur idcollection ou erreur base');  
	$ligne = mysqli_fetch_assoc ($resultat);
	
### Si mode récolement, traiter à part les champs idtrecol, idtnposition et les marques
	if (in_array ('recol_aff', $droits)) {
		Act_id ($idcollection, $INPUT['idtrecol'], $INPUT['idtnposition']);
		Crit_id ($idcollection, $INPUT['tm']);
		
		// et désactiver ces input
		unset ($INPUT['idtrecol'], $INPUT['idtnposition'], $INPUT['tm']);
	}

### Quelques contrôles en dehors du cas de l'appel par 'rappel_rec_recol', où seuls une partie des champs sont traités) 
	if ($table != 'Recolement'){
		// Pour la checkbox 'feuilledon', il faut créer la valeur 0
		if (!isset ($INPUT['feuilledon'])) $INPUT['feuilledon'] = 0;
		// Pour la checkbox 'ITinHeritage', il faut créer la valeur 0
		if (!isset ($INPUT['res2c'])) $INPUT['res2c'] = 0;
		
		// S'il y a changement d'établissement ou de numéro...
		// vérifier qu'il n'y a pas création de doublon de numéro inventaire...
		if ($INPUT['idetablissement']!=$ligne['idetablissement'] OR $INPUT['nrinv']!=$ligne['nrinv']) {
			if (!Nrunique ($INPUT['idetablissement'], $INPUT['nrinv'])) {
				erreurMsg ("Le numéro d'iventaire %0 est déjà utilisé", $INPUT['nrinv']);
				return;										// EXIT >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
			}
		}
	}

### Comparer champ par champ
	foreach ($tstruct as $champ => $tab) {
		// On ne s'intéresse qu'aux input présents dans la table de structure ET dans la ligne lue

		if (isset ($INPUT[$champ]) AND (isset ($ligne[$champ]) OR $ligne[$champ] === NULL)) {		
			$inp = trim ($INPUT[$champ]);	

			// cas des dates...
			if ('date' == substr ($champ, 0, 4)) {
				if (strlen ($inp) <2 ) $inp ='0000-00-00';	
				if (strlen ($inp) != 10 ) $inp .= (substr ('0000-00-00', (strlen ($inp) - 10)));
			}

			// Si valeurs différentes, enregistrer la nouvelle valeur
			if ($inp != $ligne[$champ]) {
				$ident = $t_nomid[$tab];
				$id    = $ligne[$ident];

				if ($debug & 0x10) echo "<br/>@@@ Champ &lt;".$champ."&gt; : valeur assignée '"
				.$inp."'  --- valeur actuelle '".$ligne[$champ]."' dans la table &lt;".$tab."&gt;<br/>"; 
					
				// cas des dimensions et des prix, éviter d'afficher 0...
				if (('dim' == substr ($champ, 0, 3) OR $champ == 'valeurest' OR $champ == 'valeurorg') AND $inp<=0) 
					$result_sav = requete ("UPDATE $tab SET $champ = NULL WHERE $ident = $id");
				else
					$result_sav = requete ("UPDATE $tab SET $champ = '".NormSQL($inp)."' WHERE $ident = $id");
				
				if (!$result_sav)
				erreurMsg ("Le champ %0 n'a pas pu être modifié, vérifiez le type que vous avez entré...", $champ); 
			} 
		}
	}

	global $erreur; 
	if (!$erreur) {
		miseaJour ($idcollection);
		Message ("Enregistrement effectué");
	}
}

############################################################################# Tr_en ###
function Tr_en ($table) {

	global $Xvars;
	
	// Tout objet
	if ($Xvars['acquisition_en'] != '') $Xvars['acquisition'] = $Xvars['acquisition_en'];
	if ($Xvars['domaine_en'] != '') $Xvars['domaine'] = $Xvars['domaine_en'];
	if ($Xvars['etat_en'] != '') $Xvars['etat'] = $Xvars['etat_en'];
//	if ($Xvars['etatfiche_en'] != '') $Xvars['etatfiche'] = $Xvars['etatfiche_en'];
	if ($Xvars['production_en'] != '') $Xvars['production'] = $Xvars['production_en'];
	
	switch ($table) {
		case 'Machines' :
	if ($Xvars['designation_en'] != '') $Xvars['designation'] = $Xvars['designation_en'];
	if ($Xvars['production_en'] != '') $Xvars['production'] = $Xvars['production_en'];
	if ($Xvars['service_en'] != '') $Xvars['service'] = $Xvars['service_en'];
		break;

		case 'Documents' :
	if ($Xvars['langue_en'] != '') $Xvars['langue'] = $Xvars['langue_en'];
	if ($Xvars['typedoc_en'] != '') $Xvars['typedoc'] = $Xvars['typedoc_en'];
	if ($Xvars['typeillustr_en'] != '') $Xvars['typeillustr'] = $Xvars['typeillustr_en'];
		break;

		case 'Logiciels' :
	if ($Xvars['langue_en'] != '') $Xvars['langue'] = $Xvars['langue_en'];
	if ($Xvars['langprog_en'] != '') $Xvars['langprog'] = $Xvars['langprog_en'];
	if ($Xvars['codeprog_en'] != '') $Xvars['codeprog'] = $Xvars['codeprog_en'];
	if ($Xvars['support_en'] != '') $Xvars['support'] = $Xvars['support_en'];
	}	
		
	return;
}	

######################################################################### Typa ###
function Typa ($str) {
# Traitement typographie et liens HTML
#------------------------------------------------------------------------------
	if ($str === NULL) return '';
	
	$pattern = array ('#\n#', '#\{\{#', '#\{#', '#\}\}#', '#\}#',
										 '#\[\[#', '#\=\=#', '#\]\]#'	);
	$replacement = array ('<br/>', '<b>', '<i>', '</b>', '</i>',
												'<a href="', '" target="_blank">', '</a>' );	
	return preg_replace ($pattern, $replacement, $str);
}	

################################################################### Verrouiller ###

function Verrouiller ($idcollection) {
# Placer un verrou de modification pour le rapporteur en cours
#	Si le verrou est créé, résultat TRUE
# Si le verrou existe déjà, pour ce rapporteur, résultat TRUE
# Si le verrou existe déjà, pour un autre rapporteur, résultat FALSE
#			(si ce verrou date de plus d'une heure, il est effacé => nouvelle écriture)
###

	$idrapporteur = $_SESSION['idrapporteur'];

### Tenter d'écrire le verrou , sans test préalable pour éviter une écriture concurrente
	try {
		$result = requete ("INSERT INTO Verrous SET idrapporteur=$idrapporteur, idcollection=$idcollection", FALSE);
	}
	catch (mysqli_sql_exception $e){}		// L'exception n'est pas traitée, le reste du code est inchangé...
	if ($result == 1) return TRUE;			// >>>>>> EXIT
	
	else {

### Analyser le rejet
		$result = requete ("SELECT Verrous.idrapporteur, idcollection, UNIX_TIMESTAMP(dateverrou), rprenom, rnom
												FROM Verrous 
												LEFT JOIN Rapporteurs on Verrous.idrapporteur = Rapporteurs.idrapporteur
												WHERE idcollection=$idcollection");
		$ligne = mysqli_fetch_assoc ($result);
		// Si c'est "mon" verrou, pas de problème - CAS INEXISTANT : Insert agit dans ce cas comme un Update !?
		if ($ligne['idrapporteur'] == $idrapporteur) return TRUE;			// >>>>>> EXIT
		
		else {	
			// le verrou est il encore valide ? Sortir le timestamp 
			$dateverrou = $ligne['UNIX_TIMESTAMP(dateverrou)'];

			if ($dateverrou+3600 < time() ) {
				// si verrou périmé, le supprimer et replacer un verrou...
				requete ("DELETE FROM Verrous WHERE idcollection=$idcollection");
				return Verrouiller ($idcollection);												// >>>>>> EXIT... par un appel en ré-entrance...
				
			}	else {
				// Compter la collision et sortir faux
					Compter ('nbr_conflits');
					$nom = $ligne['rprenom'].' '. $ligne['rnom'];
					$text = Tr ("Cet objet est en cours de modification par $nom", " Update in progress by $nom");
					echo"<script language='javascript'>";
					echo"alert('$text' + ' !')";
					echo"</script>";

				return FALSE;
			}
		}
	}
}


###################################################################### XML_affliens0 ###
function XML_affliens0 ($loop, $attr, $Xaction) {

	global $Xvars, $tl_orga, $tl_perso, $save_tl_orga, $save_tl_perso;
	
	// Si tag final </affliens0>, restituer les tables de noms
	if ($loop === null) {
		$tl_orga = $save_tl_orga;
		$tl_perso = $save_tl_perso;
		return;
	}


	// Si tag de début
	if ($loop === 0) {				// un seul appel du tag de début
	
		// Sauver les tables de noms de l'objet principal
		$save_tl_orga = $tl_orga;
		$save_tl_perso = $tl_perso;

		// Chercher l'ensemble des liens		
		$idcollection = $Xvars['idcollection'];
		$SQLresult_liens = requete (	
			"SELECT idcol1, idcol2, Liens.idfamille, famille, commentlien
			FROM Liens, Familles
			WHERE  Familles.idfamille = Liens.idfamille 
				AND ( idcol1 = $idcollection OR idcol2 = $idcollection )" );
	
	// Noter le nombre de résultats
		$Xvars['nb_liens'] = mysqli_num_rows ($SQLresult_liens);
		$Xvars['SQLresult_liens'] = $SQLresult_liens;
		return 'ACT' ;
	}
} 
     

####################################################################### XML_affliens ###
function XML_affliens ($loop, $attr, $Xaction) {

	global $Xvars;
	if ($loop === null) return;		// tag final : </affliens>
	
	global $nrinv_objet;
	
	//  Appel du lien courant
	$l_lien = mysqli_fetch_assoc ($Xvars['SQLresult_liens']);	
	if ($l_lien) {	
		// charger la tables des variables
		$Xvars = array_merge ($Xvars, $l_lien);
	
		// Préparer les champs enrichis,
		// Flèche père ou fils
		if ($l_lien['idcol1'] == $Xvars['idcollection'])  {
			$idcol_lien = $l_lien['idcol2'];
			$prefix = '==&gt; ';
			$postfix = '';
		 } else {
		 	$idcol_lien = $l_lien['idcol1'];
		 	$prefix = '';
		 	$postfix = ' ==>';
		 }

		// la désignation complète;
		$Xvars['design_complete'] = Design_titre ($idcol_lien);
		// et le numéro d'inventaire transmis par global depuis Design_titre
		$Xvars['nrinv_lien'] = $prefix.$nrinv_objet.$postfix; 
		// sans oublier l'id du lien
		$Xvars ['idcol_lien'] = $idcol_lien;

		// Alternance des couleurs de ligne
		$Xvars['class'] =  ($loop % 2) ? 'colligb' : 'collig2';

		return 'ACT,LOOP';

	// Sinon sortie			
	} else return  'EXIT' ;
} 
     
###################################################################### XML_affmouv0 ###
function XML_affmouv0 ($loop, $attr, $Xaction) {

	global $Xvars;
	
	// Si tag de début
	if ($loop === 0) {				// un seul appel du tag de début
	
		// Chercher l'ensemble des liens		
		$idcollection = $Xvars['idcollection'];
		$SQLresult_mouv = requete (	
	 		"SELECT * FROM Col_Mouv 
	 		LEFT JOIN Mouvements ON Mouvements.idmouvement=Col_Mouv.idmouvement
	 		LEFT JOIN Typemouvts On Typemouvts.idtypemouvt=Mouvements.idtypemouvt
			WHERE idcollection=$idcollection
	 		ORDER BY datemouv");
	
	// Noter le nombre de résultats
		$Xvars['nb_mouv'] = mysqli_num_rows ($SQLresult_mouv);
		$Xvars['SQLresult_mouv'] = $SQLresult_mouv;
		return 'ACT' ;
	}
} 
     

####################################################################### XML_affmouv ###
function XML_affmouv ($loop, $attr, $Xaction) {

	global $Xvars;
	if ($loop === null) return;		// tag final : </affmouv>

	//  Appel du mouvement courant
	while ($ligne = mysqli_fetch_assoc ($Xvars['SQLresult_mouv'])) {
		// charger la tables des variables
		$Xvars['idmouvement'] = $ligne['idmouvement'];
		$Xvars['datemouv'] = $ligne['datemouv'];
		$Xvars['typemouvt'] = $ligne['typemouvt'];
		$Xvars['orgmouv'] = $ligne['orgmouv'];
		$Xvars['destmouv'] = $ligne['destmouv'];
		$Xvars['comgenmouv'] = $ligne['comgenmouv'];
		$Xvars['commouv'] = $ligne['commouv'];
		
		// Alternance des couleurs de ligne
		$Xvars['class'] =  ($loop % 2) ? 'colligb' : 'collig2';

		return 'ACT,LOOP';

	// Sinon sortie		
	}  return  'EXIT' ;
} 
     
##################################################################### XML_affphotos0 ###
function XML_affphotos0 ($loop, $attr, $Xaction) {

	global $droits, $Xvars;
	if ($loop === null) return;		// tag final : </item>
	
	// tag de début, lire la table
	if ($loop === 0) {
		$idcollection = $Xvars['idcollection'];
		$reserve = (in_array("aff_req_priv", $droits)) ? '' : " AND pubreserv='non' ";

		$SQLresult_photos = requete (	
			" SELECT Col_Med.idmedia, cotemedia, ordre_media, descrimedia, pubreserv, formatmedia
              FROM Medias, Col_Med 
              WHERE Col_Med.idmedia = Medias.idmedia
                 AND Col_Med.idcollection = $idcollection
                 $reserve
              ORDER BY ordre_media");
	}

	// Noter le nombre de résultats
	$Xvars['nb_photos'] = mysqli_num_rows ($SQLresult_photos);
	$Xvars['SQLresult_photos'] = $SQLresult_photos;

	return 'ACT' ;
} 
     
###################################################################### XML_affphotos ###
function XML_affphotos ($loop, $attr, $Xaction) {

	global $db, $Xvars;
	if ($loop === null) return;		// tag final
	
	//  Appel de la photo courante
	$l_photo = mysqli_fetch_assoc ($Xvars['SQLresult_photos']);
	
	if ($l_photo) {
		$Xvars = array_merge ($Xvars, $l_photo);
		
		Prepar_photo ($l_photo);		// Préparer l'instruction d'appel javascript des photos élargies

		// Alternance des couleurs de ligne
		$Xvars['class'] =  ($loop % 2) ? 'colligb' : 'collig2';
		return 'ACT,LOOP';
	} else return  'EXIT' ;
} 
     
######################################################################## XML_boutons ###
function XML_boutons ($loop, $attr, $Xaction) {

	if ($loop === null) return;		// tag final
	global $Xvars;

	if ($loop < count ($Xvars['boutons'])) {
		$Xvars['lb']=$Xvars['boutons'][$loop];					
		return  'ACT,LOOP';
	} else return  'EXIT' ;
} 

################################################################### XML_idcollection ###
function XML_idcollections ($loop, $attr, $Xaction) {

	if ($loop === null) return;		// tag final
	global $Xvars;

	if ($loop < count ($_SESSION['idcollections'])) {
		$Xvars['idcol'] = $_SESSION['idcollections'][$loop];		
		return  'ACT,LOOP';
	} else return  'EXIT' ;
} 

###################################################################### XML_item_enum ###
function XML_item_enum ($loop, $attr, $Xaction) {
	if ($loop === null) return;		// tag final : </item>
	
	global $Xvars;
	static $t_enum;
	$item = $attr['item'];

	// tag de début, mettre en tableau la liste des enum
	if ($loop === 0) 
		$t_enum = explode (',', $Xvars['tstruct'][$item]);

	//  Appel de la valeur courante
	if ($loop < count ($t_enum)) {
		$Xvars['item_val'] = $t_enum[$loop];
	
		// Préparer les variables
		$Xvars['item_select'] = $Xvars['item_val'] == $Xvars[$item];
		
		return 'ACT,LOOP';
	} else return 'EXIT' ;
} 

#################################################################### XML_item_select ###
function XML_item_select ($loop, $attr, $Xaction) {
# Les notations n.... et v... aident à distinguer le Nom des champs et la Valeur des champs
	if ($loop === null) return;		// tag final : </item>

	global $Xvars;
	static $SQLresult_liste;

	$nitem = $attr['item'];
	$niditem = 'id'.$nitem;
	$tabl = idtable ($niditem);
	
	// tag de début, 
	if ($loop === 0) {
		// Enregistrer la valeur existante, ou éventuellement la valeur par défaut
		if ($attr['defaut']>0) $Xvars['iditem_sel'] = $attr['defaut']; 
		else $Xvars['iditem_sel'] = $Xvars[$niditem];

		// Commencer la lecture de la table complète
		$SQLresult_liste = requete ("SELECT * FROM $tabl ORDER BY $nitem");		
	}
	
	//  Appel des valeurs courantes
	$ligne = mysqli_fetch_assoc ($SQLresult_liste);
	
	// Préparer les variables
	$Xvars['iditem'] = $ligne[$niditem];
	$Xvars['item_select'] = $ligne[$niditem] == $Xvars['iditem_sel'];
	// Traiter le cas anglais si la table le permet (colonne '_en' existe)
	if (isset ($ligne[$nitem.'_en'])) $Xvars['item'] = Snv ($ligne[$nitem], $ligne[$nitem.'_en']);
	else $Xvars['item'] = $ligne[$nitem];

	return ($ligne) ? 'ACT,LOOP' : 'EXIT' ;
} 

########################################################################################

?>
