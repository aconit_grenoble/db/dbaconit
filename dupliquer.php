<?php 
# v25				190502	PhD		Création
# v25.5.01	200113	PhD		Ajour NormSQL (normalement inutile)
# v25.8			200411	PhD		Ajout custom_css
# v25.9.02	201007	PhD		Correction type iconographie
###

		

/* Protection des entrées -------------------------------------------------------
'action'				- POST	- uniquement testé switch
'idcollection'	-	REQ 	- testé numérique (GET au 1er appel, PUT au retour formulaire)
'cols[]'				- POST 	- comparé par valeur 
'coxs[]'				- POST 	- comparé par valeur 
------------------------------------------------------------------------------ */

$custom_css = "dupliquer.css";
require_once ('init.inc.php');

## Traitement des entrées 
#########################

if (is_numeric(@$_REQUEST['idcollection'])) $idcollection = $_REQUEST['idcollection'];
$cols = @$_POST['cols'];
$coxs	= @$_POST['coxs'];

// Retour du formulaire
$action = @$_POST['action'];
if (!$action) $action = 'empty';

   
##########################################################

Debut ();		// Affichage menu principal

switch ($action) {
	### Premier passage dans Créer ##########################################################################
	case 'empty' : 		
		if ($idcollection == "") {
			// Normalement ce cas ne doit pas se produire, le lien suppose que idcollection est défini...
			DIE ( Tr ("Pas de fiche sélectionnée !", 'No record selected!'));
		}
		
		// Lire le type de fiche pour pouvoir afficher le tableau de sélection des champs
		$result = requete (
							"SELECT type, prefinv, nrinv FROM Collections 
							LEFT JOIN Etablissements ON Collections.idetablissement=Etablissements.idetablissement
							WHERE idcollection=$idcollection ");
		$l1 = mysqli_fetch_assoc ($result);	
		$type = $l1['type'];
	  $nrinvc = $l1['prefinv']." ".$l1['nrinv'];
		break;
	
	### Retour Annuler ######################################################################################
	case 'annuler' : 
		break;
		
	### Retour Dupliquer ####################################################################################
	#########################################################################################################
	case 'dupliquer' :

# Si le numéro est complet : Insérer dans la base #########################################################
###########################################################################################################

### Lire la fiche d'origine, partie commune
		$result = requete ("SELECT * FROM Collections WHERE idcollection=$idcollection ");
		$ligne = mysqli_fetch_assoc ($result);
		$type = $ligne['type'];
		$idetablissement = $ligne["idetablissement"];
	
### Trouver le prochain No d'inventaire
		// Chercher le sigle et le type de numération
		$requete = requete 
							("SELECT prefinv, numeration FROM Etablissements WHERE idetablissement=$idetablissement");
		$l2 = mysqli_fetch_assoc ($requete);
		mysqli_free_result ($result);

		if ($l2['numeration'] != 'MdF') {			// ------- Numération simple -----------------
			// Recherche du prochain numéro
			// L'opération "nrinv+1" oblige MySQL à prendre la partie numérique 
			// des chaines de caractère nrinv... Ensuite l'opérateur MAX fonctionne correctement.
			// Sans cette astuce, MAX (nrinv) sort le dernier en tri alphanumérique, 
	
			// Distinguer le cas standard du cas spécial avec numérotation commune à tous les établissements
			$where = isset ($dbase['num_commune']) ? "" : "WHERE idetablissement=$idetablissement";
			$result = requete ("SELECT MAX(nrinv+1) AS nr FROM Collections ".$where);
			$l3 = mysqli_fetch_assoc ($result);
			mysqli_free_result ($result);
			if (!($nrinv = $l3['nr'])) $nrinv = 1;		
		
		} else {																	// ------- Numération MdF -----------------
			// déterminer le code série courant
			$tn = explode ("-", $nrinv);

			// Sélectionner le dernier numéro de la série
			$result = requete (
				"SELECT nrinv FROM Collections
					WHERE nrinv LIKE '".$tn[0].'-'.$tn[1]."%'
					ORDER BY nrinv DESC LIMIT 1");
			$l4 = mysqli_fetch_assoc ($result);
			$nrinv = $l4['nrinv'];
			mysqli_free_result ($result);
		
			$tn = explode ("-", $l4['nrinv']);

			// ne garder que les chiffres en tête du dernier numéro (hors indices lettres)
			$n = preg_split("#\D#", $tn[2], -1);
			// Incrémenter et formater sur 3 caractères, composer le nouveau numéro
			$n = (string)$n[0]+1;
			$n = substr ('00'.$n, -3, 3);
			$nrinv = $tn[0].'-'.$tn[1].'-'.$n;
		}

	### Préparer la liste des champs à recopier
		$set = "";		

		//  Vérifier qu'il y a des champs sélectionnés !
		if (count ($cols)>0){ 
			// Détailler la liste des dimensions : seul dimlong est transmis, il faut ajouter les autres
			if (in_array ('dimlong', $cols)) 
				array_push ($cols, 'dimlarg', 'dimhaut', 'dimdiam', 'dimpoids', 'couleur', 'comdim');
		
			foreach ($cols as $col) if ($ligne[$col] != '') $set .= ", $col='".NormSQL($ligne[$col])."'";
			// NorMSQL est ajouté ici pour récupérer des apostrophes égarées, cas occasionnel...
		}
		
		// Variables session
		$nomrapcreat = $_SESSION['rapporteur'];
		$datecreat = date ('Ymd'); 

	### Écriture en table Collections
		$result = requete ("INSERT INTO Collections SET type='$type', nrinv='$nrinv', idetablissement=$idetablissement, 
													idetatfiche=1, nomrapcreat='$nomrapcreat', datecreat=$datecreat".$set);

		if (!$result) erreurMsg ("Pas d'inscription dans Collections"); 		
		else {
			// Récupérer l'index
			$idcollection = mysqli_insert_id ($dblink);				
			
	### Maintenant qu'on a vérifié qu'on peut créer la fiche 
			// on choisit la table Machines, Documents, Logiciels, pour enregistrer la partie spécifique
			if ($type == 'machine' OR $type == 'objet') {$table = 'Machines'; $id_table = 'idmachine';}
			elseif ($type == 'document' OR $type == 'iconographie') {$table = 'Documents'; $id_table = 'iddocument';}
			if ($type == 'logiciel') {$table = 'Logiciels'; $id_table = 'idlogiciel';}

	### Préparer la liste des champs à recopier
			// Lire la table d'origine
			$result = requete ("SELECT * FROM $table WHERE ".NomID ($table)."=".$ligne[NomID ($table)]);
			$lignex = mysqli_fetch_assoc ($result);
			
			$set = "";	
			// Vérifier qu'il y a des champs sélectionnés
			if (!empty($coxs)){
				foreach ($coxs as $cox) if ($lignex[$cox] != '') $set .= ", $cox='".NormSQL($lignex[$cox])."'";
				$set = ltrim ($set, ',');
			}			
			
			if (strlen ($set) == 0) 					// Il faut bien écrire quelque chose !
				$set = ($table == 'Machines') ? "nom=''" : (($table == 'Documents') ? "titredoc=''" : "titrelog=''");
		
	### Écriture en table Machines, Documents ou Logiciels
			if (!($result = requete ("INSERT INTO $table SET $set"))) 
				erreurMsg ("Pas d'inscription dans %0", $table); 	
			else {	
				// Récupérer l'index de cette table
				$idtable = mysqli_insert_id ($dblink);

				// Et on reporte dans la fiche Collections 
				$result = requete ("UPDATE Collections SET $id_table = $idtable WHERE idcollection = $idcollection");

	### ON TERMINE EN APPELANT L'ÉCRAN MODIFICATIONS :
				$_POST['cols'] = '';					// effacer cols et coxs
				$_POST['coxs'] = '';	
				
				$menu = $_REQUEST['menu'] = 'M_objet';
				$_REQUEST['idcollection'] = $idcollection;
				$_REQUEST['Modif'] = 1;
				include ('consulter.php'); // >>>>>>>>>>>>>>>>>>>>>> Passer à l'écran MODIFIER objet
				debug (255, 'RETOUR >>>>>>'); // Pour piéger un retour intempestif
				exit ();											// ... par sécurité
			}
		
	}
}

###  AFFICHAGES ############################################################################################
############################################################################################################

### Charger le tableau de contexte
	$Xvars['idcollection'] = $idcollection;
	$Xvars['nrinvc'] = $nrinvc;
	$Xvars['type'] = $type;
	
### Afficher
$liste_xml = Xopen ('./XML_modeles/dupliquer.xml') ;
Xpose ($liste_xml);

#################################### Fin de traitement
Fin ();
?>