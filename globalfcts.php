<?php
# v25				190712	PhD		Adapté fonction Compter à nouvelle table Compteurs, refonte 'profil', modifié 'Design_titre'
#														correction apostrophes dans Nrinv_to_idcol, ErreurMSG remplacé ++
# v25.0.1		190715	PhD		Corrigé traitement titre_avec_designation
# v25.1			190717	PhD		Corrigé MYSQLI_BOTH
#	v25.2			190905	PhD		Ajouté Phd_encode et Phd_decode
# v25.3			191030	PhD		Corrigé valeur par défaut fonction Profil, Ajouté Cotebatiment, supprimé Trim_nr
# v25.3.01	191107	PhD		Ajout urlencode/decode dans les fonctions Phd_encode/decode
# v25.3.02	191113	PhD		Correction fonction Profil : un profilh correct fait 12 caractères
# v25.4			191122	PhD		Complété message 'DIE' dans Design_titre
# v25.5			191223	PhD		Ajouté'NegRed'
# v25.6.01	200215	PhD		Recodé Phd_encode et Phd_decode (passage en hexa), ajout NegRedCyan
# v25.7			200318	PhD		Ajouté Prepare_photo, standardisé présentation des fonctions
# v25.8			200411	PhD		Préparé appel plugin_menu
# v25.9			200720	Phd		Complété AdMedia pour traitement des extensions img
# v25.10		210130	PhD		Ajout password_sha1
# v25.14		220716	PhD		Corrigé functions NormIN, cotebatiment pour arg nuls; remplacé strftime par date (PHP 8.1)
# v25.14.01	220803	PhD		Corrigé fonction Autor_modif : cas ou droit local est null, corr Tablestruct (PHP 8.1)
# v26.2.01	230427	PhD		Modifié 'Prepar_photo' pour transmettre la 'cotemedia'
# v26.5			231223	PhD		Message erreur ni machine/ni doc/ni log => continuer => permet effacement fiche
###

###### Les fonctions sont triés : d'abord Debut et Fin, puis tri alphabétique ##################

###################################################################### Debut ###
function Debut () 
# Affichage du bandeau d'en tête et du menu principal
# C'est normalement le premier affichage après l'ouverture de la page HTML
{
	global $Xvars;										// variables pour l'affichage du menu en-tête
  global $dba_retour, $dbase, $dir_export, $dir_plugin, $menu_tete_plus, $droits;		// paramètres 
	global $langues, $DebutFait;
	
	$Xvars['dba_retour'] = $dba_retour;
	$Xvars['dbase'] = $dbase;
	$Xvars['dir_export'] = $dir_export;
	$Xvars['droits'] = $droits;
	$Xvars['plugin_menu_xml'] = $dir_plugin.$menu_tete_plus;		
	$Xvars['plugin_menu'] = substr ($dir_plugin.$menu_tete_plus, 0, -4);		// idem sans suffixe xml (automatique dans le call)

   if ($DebutFait) return;
   $DebutFait = 1;
  
### Bandeau et menu principal---------------------------------------------------	   
	// Noter le No de l'objet en cours - s'il existe
	if (isset ($_SESSION['idcollection']))  
	  	$Xvars['idcollection'] = $_SESSION['idcollection'];
	else unset ($Xvars['idcollection']);			// inutile...
	
	// Sélection du basculement de langues, solution pour 2 langues
	$n = array_search($_SESSION['langue'], $langues);
	$Xvars['newlang'] = $langues[!$n];
		
#### Afficher partir du modèle XML
	$liste_xml = Xopen ('./XML_modeles/menu_tete.xml') ;
	Xpose ($liste_xml);
	$Xvars = array ();						// Purger la table des variables

	// et affichages des messages résultats de l'opération précédente   
	AfficheMessages ();
}

####################################################################### Fin ###
function Fin () 
# Effectuer les opérations de fin d'exécution d'une transaction : profile, messages, fermeture page html
{
	Profil ('W');												// Enregistrer les changements de profil 
	
	$mem = memory_get_usage ();				// Déterminer la taille mémoire utilisée
	debugMsg ('Mémoire utilisée en fin de traitement : '.$mem);	

	AfficheMessages ();
	echo "\n</body></html>\n";
	exit; 
}

################################################################################################################
################################################################################################################

################################################################### AffDate ###
function AffDate ($v) 
# Met en forme une date pour affichage
{
	global $date_fmt;
	
	if (!$v || ($v == "0000-00-00")) return '';
	if (3 == sscanf ($v, "%04d-%02d-%02d", $y,$m,$d)) {
		if ($m == 0) return $y;
		if ($d == 0) return date ($date_fmt[1], mktime (0,0,0,$m,1,$y));
		return date ($date_fmt[0], mktime (0,0,0,$m,$d,$y));
	}
	return $v;
}

################################################################### AdMedia ###
function AdMedia ($nrmedia, $db, $typ, $t='', $mod='rel', $w=FALSE) 
# Calcul du No de dossier et composition de l'adresse (relative ou absolue) du fichier media
# $nrmedia : numéro = index idmedia
# $db =	No de base de données
# $typ : type de média
# $t : 	spécifie vignette 'v' ou rien
# $mod : 'rel' (ou 'abs') pour adresse relative ou absolue 
# $w : 	cas écriture : si le dossier n'existe pas il est créé
# return : $adfile
#
{
	global $dir_media, $url_media;	// Bases du nom des dossiers images
	$path = ($mod == 'rel') ? $dir_media : $url_media;

	$n = (int)($nrmedia / 1000);

	$t = ($t == 'v') ? 'v' : '';
	$adfile = $path.'_'.$db.'/'.$typ.'_'.$t.$n.'/'.$nrmedia.'.'.$typ;
	
	//Corespondance entre type de fichier et type de vignette
	if(($typ=='pdf' || $typ=='mp3' || $typ=='mp4' || $typ=='mov' || $typ=='img') && ($t=='v'))
		$adfile = $path.'_'.$db.'/'.$typ.'_'.$t.$n.'/'.$nrmedia.'.'.'png';
	
	// créer dossier photo et vignette si inexistant
	if ($w) {
		if (!is_dir($dir_media.'_'.$db.'/'.$typ.'_'.$n)) {	
			mkdir ($dir_media.'_'.$db.'/'.$typ.'_'.$n);
			mkdir ($dir_media.'_'.$db.'/'.$typ.'_v'.$n);
		}	 
	}
	return $adfile;
}

########################################################### AfficheMessages ###
function AfficheMessages () 
# Déclenche l'affichage des messages préparés pendant le traitement courant
# les messages de bon fonctionnement en vert, les messages d'erreur en rouge
# Efface les chaines messages et erreurs.
{
   global $erreurs,$messages;
   
   foreach ($erreurs as $value)
      if ($value) 
         echo "<p class=erreur>$value</p>\n";
   $erreurs = array ();   

   foreach ($messages as $value)
      if ($value) 
         echo "<p class=message>$value</p>\n";
   $messages = array ();   
}

#################################################################### AffNoms ###
function AffNoms ($op, $relation) 
# Sélectionne une liste de noms pour affichage, en fonction de la relation choisie.
# $op : précise si on sélectione 'O'=organisms, 'P'=Personnes ou l'ensemble
{	
	global $tl_orga, $tl_perso;

	if (strstr ($op, 'o') && isset ($tl_orga[$relation])) 
		$liste = $tl_orga[$relation];
	
	if (strstr ($op, 'p') && isset ($tl_perso[$relation])) {
		if (!isset($liste)) $liste = $tl_perso[$relation];
		else $liste .= ', '.$tl_perso[$relation];
	}
	return (isset ($liste)) ? $liste : ''; 
}

################################################################ Autor_modif ###
function Autor_modif ($idcollection, $îdetablissement ='') 
# Fonction vérifie que l'opérateur est autorisé à créer & modifier un objet 
{
	global $droits;

	// Si l'opérateur n'a pas droit de modification... rejet simple
	if (!in_array ("mod_objet", $droits)) return FALSE;	//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

	// S'il n'y a pas de restriction d'etablissement (test d'abord droitlocal NULL, puis vide)
	if (empty ($_SESSION['droitlocal'])) return TRUE;	//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
	if ($_SESSION['droitlocal'][0] == '' ) return TRUE;	//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
	
	// Trouver l'etablissement
	if ($îdetablissement =='') {
		$result = requete ("SELECT idetablissement FROM Collections WHERE idcollection = $idcollection");
  		$ligne = mysqli_fetch_assoc ($result);
    	$îdetablissement = $ligne['idetablissement'];
    }

    // Vérifier le droit d'accès
    if (in_array($îdetablissement, $_SESSION['droitlocal'] )) return TRUE;	//>>>>>>>>>>>>>>>>>>>>>>>>>>
    
    // et si l'utilisateur n'a pas droit à cet etablissement, l'avertir...
	$result = requete ("SELECT prefinv FROM Etablissements WHERE idetablissement = $îdetablissement");
  		$ligne = mysqli_fetch_assoc ($result);
    	$siglelocal = $ligne['prefinv'];
    
	erreurMsg ("Vous n'êtes pas autorisé à modifier les fiches d'inventaire de l'organisme %0", $prefinv);
	AfficheMessages ();
    return FALSE;
}

################################################################# Caller ###
function Caller ($err=FALSE) 
# La fonction retourne le nom et la ligne d'une fonction appelante
# Par défaut, c'est la première fonction apellée à l'intérieur du module principal
# Dans la cas d'une erreur, c'est le point d'appel de la fonction ErreurMsg
{
    $caller = debug_backtrace (); 
	  $t = array_keys ($caller);		// fonction reset() et end() attendent une variable
	  
	  if ($err) $level = reset ($t) +1;
	  else $level = end ($t) -1;
	  
      if ((!($f = strrchr ($caller[$level]['file'], '/'))) && 
          (!($f = strrchr ($caller[$level]['file'], '\\'))))
          $f = ' '.$caller[$level]['file'];
      $caller = '[' . substr ($f,1) . ':' . $caller[$level]['line'] . ']  ';

   return $caller;
}

############################################################ Compose_tl_orga ###
function Compose_tl_orga ($idcollection, $nomcomplet=FALSE) 
# Compose un tableau des listes de noms d'organismes, classées par relation
{
	$tl_orga = array ();
	
	$SQLresult_orga = requete ("SELECT onom, f_secable, osigle, relation FROM Organismes, Col_Org, Relations
		WHERE Organismes.idorganisme=Col_Org.idorganisme AND Relations.idrelation=Col_Org.idrelation
		AND Col_Org.idcollection= $idcollection");
	
   if (0 == (mysqli_num_rows ($SQLresult_orga)))
      return $tl_orga;		// >>>>>>>>>>>>>>>
   
   // Si il y a des résultats
	while ($ligne =  mysqli_fetch_assoc ($SQLresult_orga)) {
		if ($ligne['onom'] == '(inconnu)') $ligne['onom'] = Tr ('(inconnu)', '(unknown)');
		
		if ($nomcomplet OR !$ligne['f_secable']) $nom = $ligne['osigle'].' '.$ligne['onom'];
		else $nom = $ligne['osigle'];
		
		if (!isset ($tl_orga[$ligne['relation']])) 
			$tl_orga[$ligne['relation']] = $nom;
		else $tl_orga[$ligne['relation']] .= ", ".$nom;
	}
    return $tl_orga;
}
       
################################################################## Compose_tl_perso ###
function Compose_tl_perso ($idcollection) 
# Compose un tableau des listes de noms de personnes, classées par relation
{
	global $tl_perso;
	$tl_perso = array ();
	
	$SQLresult_perso = requete ("SELECT pnom, pprenom, relation FROM Personnes, Col_Per, Relations
		WHERE Personnes.idpersonne=Col_Per.idpersonne AND Relations.idrelation=Col_Per.idrelation
		AND Col_Per.idcollection= $idcollection");
	
   if (0 == (mysqli_num_rows ($SQLresult_perso)))
      return $tl_perso;		// >>>>>>>>>>>>>>>
   
   // Si il y a des résultats
	while ($ligne =  mysqli_fetch_assoc ($SQLresult_perso)) {
		if ($ligne['pnom'] == '(inconnu)') $ligne['pnom'] = Tr ('(inconnu)', '(unknown)');
		if (!isset ($tl_perso[$ligne['relation']])) 
			$tl_perso[$ligne['relation']] = $ligne['pprenom'].' '.$ligne['pnom'];
		else $tl_perso[$ligne['relation']] .= ", ".$ligne['pprenom'].' '.$ligne['pnom'];
	}
	return $tl_perso;
}

################################################################### Compter ###
function Compter ($compteur) 
# Incrémente un des compteurs du tableau Paramtres
{
	global $dba_comptage;
	if ($dba_comptage) requete ("UPDATE `Compteurs` SET compte=compte+1 WHERE compteur='$compteur'");
}

################################################################## colonnes ###
function colonnes ($table)  
# renvoie les noms des colonnes de la table
{
   $colonnes = array ();
   $result = requete ('SHOW COLUMNS FROM ' . $table);
   for ($i = 0; $ligne = mysqli_fetch_assoc ($result); $i++)
        $colonnes[$i] = $ligne['Field'];
   mysqli_free_result ($result);
   return $colonnes;
}

##################################################################### Cotebatiment ###
function Cotebatiment ($nb_segments, $coterange) 
# Détermine la partie d'une cote de rangement qui désigne un bâtiment. 
# Ce préfixe dépend du nombre de segments fixés pour l'établissement concerné,
#  et du nombre de cractères (lettres ou chiffres dans chaque segment)
{
	if ($coterange === NULL) $coterange = '';
	
	// Préciser la longueur du préfixe (dépend du nbre de segments traités)
	$seg = 1;											// compteur de segments
	$n = 0;												// nombre de caractères du préfixe
	
	for ($i = 0; $seg <= $nb_segments;){
		if ($i+1 > strlen ($coterange)) break;						// protection contre cote trop courte...

		if ($seg % 2) $k = ctype_digit($coterange[$i]);   // impair : chiffre attendu
		else $k = ctype_alpha($coterange[$i]);						// pair : lettre attendue
	
		if ($k) { $n++; $i++;	}			// si lettre ou chiffre attendu, OK	on compte et on avance			
		else $seg++;								// sinon, on change de segment, sans avancer
	}

	if ($n == 0) return '';							// >>>>>>>>>>>>>>>>>>>>>>> SORTIE, format coterange incorrect

	$cotebatiment = substr ($coterange, 0, $n);
	return $cotebatiment;
}

################################################################### debug ###
function debug ($code, $msg, $var="", $mix="") 
# Affiche un message et (éventuellement) le contenu d'une variable 
# si un bit positionné de la  variable $debug correspond à un bit de $code.
# Traite les variables simples et les tableaux.
{
	global $debug;
	if ($debug & $code) {
		echo "<pre>[[[".$msg ;
		if ($var) {
			echo " : ";
			if (is_array ($var) || is_numeric ($var) || empty ($mix) ) {			
				print_r ($var); 
			} else  {
				for ($i=0; $i<strlen($var); $i+=1) {
					echo ".".substr ($var, $i, 1);
				}
			}			
		} 
		echo "]]]</pre>";
	}
	return;
}
         
################################################################# debugMsg ###
function debugMsg ($msg, $force=false) 
# $msg		: Message en clair
# $level	: Précise le niveau d'appel à afficher (0 pour le corps des modules, 1 pour les premières n, etc.
# $force	: Force le traitement , indépendamment de la variable globale $debug
{
	global $messages, $debug;
   if (!($debug & 1) && !$force) return;
   array_push ($messages,  Caller ().Translate ($msg));
}

############################################################# Design_titre ###
function Design_titre ($idcollection, $typ=TRUE) 
# Retourne la désignation complète d'un objet : nom, compléments, fabricant, auteur, éditeur...
# $typ : si vrai, autorise l'ajout du type en tête de ligne (voir paramêtre)
# le numéro d'inventaire nrinv est retourné également en global
{
	global $dbase, $nrinv_objet, $tl_orga, $tl_perso;
  
  // Le drapeau 'op' permettait de contrôler l'ajout des organismes et personnes.
  // il est maintenu en prévision de modifs futures...
  $op=TRUE;
	// Composer les listes de personnes et d'organismes
	if ($op) {
		$tl_orga = Compose_tl_orga ($idcollection);
		$tl_perso = Compose_tl_perso ($idcollection);
	}

	// Appel de la base, sans savoir si machine ou document ou logiciel
	$resultat = requete (
	"SELECT DISTINCT Collections.idcollection, nrinv, type,
	Collections.idmachine, Collections.iddocument, Collections.idlogiciel, 
	nom, nom_en, nomsecond, nomsecond_en, designation, designation_en, titredoc, typedoc, typedoc_en, titrelog, version
	FROM Collections

	LEFT JOIN Machines on Machines.idmachine=Collections.idmachine 
	LEFT JOIN Documents on Documents.iddocument=Collections.iddocument     
	LEFT JOIN Logiciels on Logiciels.idlogiciel=Collections.idlogiciel    

	LEFT JOIN Designations on Designations.iddesignation=Machines.iddesignation  
	LEFT JOIN Typedocs on Typedocs.idtypedoc=Documents.idtypedoc
	WHERE  Collections.idcollection = $idcollection");
	
	if (mysqli_num_rows ($resultat) == 0) DIE ('Design_titre - Erreur idcollection ou erreur base'); 
	$ligne = mysqli_fetch_assoc ($resultat);
	extract ($ligne);

	// Paramètre spécifique  : controle l'affichage de la désignation dans le titre
	if (isset ($dbase['titre_avec_designation']) AND !$dbase['titre_avec_designation']) $designation = '';
	
	// Mettre de côté le No d'inventaire !
	$nrinv_objet = $nrinv;
	
	// Traiter les 3 cas 
	if ($idmachine) {
		// Traiter le cas anglais 
		$np = Snv ($nom, $nom_en);
		$ns = Snv ($nomsecond, $nomsecond_en);
		$titre = Tr ($designation, $designation_en).' '.$np.(($ns != '')? ' - '.$ns :'');
		if ($op) $titre .= ' ('.AffNoms('o','Fabricant')
										.(AffNoms('o','Marque') != '' ? ' - '.AffNoms('o','Marque') : '').')';
	
	} else if ($iddocument) {
		$tdoc = Tr ($typedoc, $typedoc_en);
		$titre = ($tdoc != '' ? $tdoc.' : ' : '').$titredoc;
		if ($op) $titre .= ' ('.(AffNoms('op','Auteur') != '' ? AffNoms('op','Auteur').' - ' : '')
										.AffNoms('o','Editeur').')';
	
	} else if ($idlogiciel) {
		$titre = $ligne['titrelog'].' '.$ligne['version'];
		if ($op) $titre .= ' ('.(AffNoms('op','Auteur') != '' ? AffNoms('op','Auteur').' - ' : '')
										.AffNoms('o','Editeur').')';
	} else echo "*** Fiche ni machine ni document ni logiciel [id] $idcollection ***";

	
	// Ajout éventuel du type d'objet en tête (Paramètre)
	if ($typ AND $op AND isset ($dbase['titre_avec_type']) AND $dbase['titre_avec_type']) $titre = "<i>$type</i> : ".$titre;
	return $titre;
}

############################################################# Deverrouiller ###
function Deverrouiller () 
# Supprime tout verrou de modification existant pour le rapporteur en cours
{
	if (isset ($_SESSION['idrapporteur'])) {
		$idrapporteur = $_SESSION['idrapporteur'];
		requete ("DELETE FROM Verrous WHERE idrapporteur='$idrapporteur'");
	}	
	return;
 
}


################################################################= erreurMsg ###
function erreurMsg ($msg) 
# Appelle la fonction Translate pour traduction éventuelle
# Met le message (d'erreur) en file d'attente dans la chaîne globale $erreurs
# La variable globale $erreur est incrémentée
# Si le droit "backtrace" est présent, déclenche un debug avec backtrace.
{
	global $droits, $erreurs, $erreur;

	if (in_array ("backtrace", $droits))
		debug (0x80, $msg, debug_backtrace ());	// Back-trace approfondi sur demande

	$args = func_get_args ();
	array_shift ($args);
	array_push ($erreurs, Caller (TRUE).Translate ($msg, $args));
	$erreur += 1;

	return;
}


################################################################### Id_next ###
function Id_next ($idcollection, $pas, $min, $max) 
# Fournit l'idcollection précédent ou suivant
# dans la liste $_SESSION['idcollections'] si elle existe
# sinon dans l'ensemble des éléments de la table Collections
#
# min et max concernent les limites de la table Collections
{

### Si il y a une liste d'éléments sélectionnés
	if (isset ($_SESSION['idcollections'])) {
		$cid = count ($_SESSION['idcollections']);
		// trouver l'indice de l'élément courant dans la liste
		for ($i = 0; $i < $cid ; $i++) {
			if ($_SESSION['idcollections'][$i] == $idcollection) {			
				// Incrémenter l'indice, le test aux limites permet d'éviter une erreur d'index
				// mais fonctionnellement on ne peut cliquer au delà des limites.
				$nextid = $i + $pas;
				if ($nextid < 0) $nextid += $cid;
				if ($nextid >= $cid) $nextid -= $cid;
				$idcollection =  $_SESSION['idcollections'][$nextid];
			break;
			}
		}

### S'il n'y a pas de liste d'éléments sélectionnés, 
	} else {
		// Chercher le premier objet existant
		// remarque : il n'y a pas d'appel depuis le menu objet si on est déjà en butée
		$idcollection += $pas;

		// Vérifier que le nouvel idcollection correspond bien à un enregistrement
		while ($idcollection >= $min && $idcollection <= $max) {
			$result = requete ("SELECT nrinv FROM Collections WHERE idcollection=$idcollection");
			if (mysqli_num_rows ($result)) break;
			$idcollection += $pas;
		}
	}

	return $idcollection;
}


################################################################### idtable ###
function idtable ($nomid) 
# renvoie la table de l'identifiant
{
  switch ($nomid) {
  	case 'motcle' : return 'Motscles';
  	case 'materiau' : return 'Materiaux';
  	default :  return  ucfirst (substr ($nomid, 2, strlen ($nomid)))."s";
  }
}


############################################################################## Ligne ###
function Ligne ($table, $id) 
# renvoi un tableau indicé de la ligne correspondante à l'identifiant
{
   $result = Requete ("SELECT * FROM $table WHERE " . nomid ($table) . " = '$id' ");
   return mysqli_fetch_array ($result, MYSQLI_BOTH);
}

#############################################################== Localisation ###
function Localisation ($s, $f) 
# Fournit les segments utiles de la chaine localisation : "sigle | organisme | labo"
{
	$t = explode ("|", $s);
	switch ($f) {
		case 'sig':
			return trim ($t[0]);
		case 'org':
			return trim ($t[1]);
		case 'lab':
			return trim ($t[2]);
		case 'org_lab':
			return trim ($t[1])." | ".trim ($t[2]);
	}
}


################################################################### maximum ###
function maximum ($table) 
# Renvoi le dernier identifiant d'une table
{
   $result = requete ("SELECT MAX(".$table.".".nomid ($table).") as max FROM $table");
   $ligne = mysqli_fetch_assoc ($result);
   return $ligne['max'];
}   


################################################################### Message ###
function Message ($msg) 
# Appelle la fonction Translate pour traduction éventuelle
# Met le message (de bon fonctionnement) en file d'attente dans la chaîne $messages
{
   global $messages;
   
   $args = func_get_args ();
   array_shift ($args);
   array_push ($messages, Translate ($msg, $args));
}

################################################################### minimum ###
function minimum ($table) 
# Renvoi le premier identifiant d'une table
{
   $result = requete ("SELECT MIN(".$table.".".nomid ($table).") as min FROM $table");
   $ligne = mysqli_fetch_assoc ($result);
   return $ligne['min'];
}   


################################################################# miseaJour ###
function miseaJour ($id) 
# Inscrit la date du jour et le nom du rapporteur dans la fiche qui vient d'être modifiée
{
  requete ("UPDATE Collections SET nomrap='" . $_SESSION['rapporteur'] . 
      "', datemodrap = ".date ('Ymd')." WHERE idcollection = $id");
  Compter ('nbr_modifs');			

}


################################################################# Nbrlignes ###
function Nbrlignes ($table) 
# Retourne le nombre de lignes de la table concernée
{
   $resultat = requete ("SELECT * FROM $table");
   return mysqli_num_rows ($resultat);
}

##################################################################### NegRed ###
function NegRed ($sum) 
# Si la variable $sum est négative elle est éditée en rouge
{
	if ($sum>=0) return $sum;
	else return "<span style='color:red'> $sum </span>";
}

################################################################# NegRedCyan ###
function NegRedCyan ($sum) 
# Si la variable $sum est négative elle est éditée en rouge
# si elle est nulle, elle est éditée couleur cyan
{
	if ($sum>0) return $sum;
	elseif ($sum==0) return "<span style='color:cyan'> $sum </span>";
	else return "<span style='color:red'> $sum </span>";
}

##################################################################### Nomid ###
function Nomid ($table) 
# renvoie l'index de la table
{
  switch ($table) {
  	case 'Col_Mate'		: return 'idcollection';
  	case 'Col_Med'		: return 'idcollection';
  	case 'Col_Mot'		: return 'idcollection';
  	case 'Col_Mouv'		: return 'idcollection';
  	case 'Col_Org'		: return 'idcollection';
  	case 'Col_Per'		: return 'idcollection';
  	case 'Motscles'		: return 'idmotcle';
  	case 'Materiaux'	: return 'idmateriau';
  	case 'Verrous'		: return 'idrapporteur';
  	default :  return 'id'.strtolower (substr ($table, 0, strlen ($table) - 1));
  }
}


################################################################## NormIN ###
function NormIN ($nom, $mode="P") 
# Normalisation des textes à l'entrée : protection contre les scripts
# et suppression des espaces tête et queue
{
	switch ($mode) {
		case "P" : $in = @$_POST[$nom]; break;
		case "G" : $in = @$_GET[$nom]; break;
		case "R" : $in = @$_REQUEST[$nom]; 
	}
	
	// (l'encodage utf-8 - permet de ne pas avoir un retour à blanc avec les versions récentes de php)
	if ($in === NULL) return '';
	else return htmlspecialchars (trim ($in), ENT_QUOTES, 'utf-8');
}

################################################################## NormSQL ###
function NormSQL ($str) 
# Normalisation des textes avant entrée dans MySQL : 
#   codage apostrophes et fin de ligne
# (les apostrophes sont déjà traitées en entrée par NormIN, mais ça ne fait pas de mal...
{
global $dblink;

//	Échappement standard pour MySQL   
	$str = mysqli_real_escape_string ($dblink, $str);
	return $str;
}

################################################################## nrinv ###
function Nrinv ($table, $id,$lien=TRUE, $param="")
# A partir d'une table et d'un identifiant, on recherche le numéro inventaire
# Retourne : si $lien est vrai : un lien vers la fiche inventaire, 
# 	complété éventuellement par un paramètre URL
# sinon le numéro inventaire lui-même
{
  if (!$id) return '';
  
  $ligne = mysqli_fetch_assoc (requete ("SELECT nrinv , prefinv
  		FROM Collections
			LEFT JOIN Etablissements ON Collections.idetablissement=Etablissements.idetablissement
			WHERE  Collections.".nomid ($table)." = $id"));
  $nrinv = $ligne['prefinv']." ".$ligne['nrinv'];
  return $lien ? "<a href=\"consulter.php?idcollection=$id$param\">$nrinv</a>" : $nrinv;
}

########################################################### nrinventaire ###
function Nrinventaire ($id,$ligne=FALSE) 
# Fournit le numéro d'inventaire complet
# Chaque fois que possible on évite un nouvel appel base en transmettant $ligne
{
	if (!$ligne) {
		$ligne = mysqli_fetch_assoc (requete ("SELECT nrinv, prefinv 
			FROM Collections
			LEFT JOIN Etablissements ON Collections.idetablissement=Etablissements.idetablissement
			WHERE  idcollection = $id "));
	}
	
	return $ligne['prefinv']." ".$ligne['nrinv'];
}

############################################################ Nrinv_to_idcol ###
function Nrinv_to_idcol ($nrinv, $idetablissement='') 
# A partir d'un numero inventaire et du numéro d'établissement, recherche le idcollection
# en cas spécial de numérotation commune, le numéro d'établissement n'est pas traité
{
	global $dbase;
	if (isset ($dbase['num_commune'])) $where = "nrinv='$nrinv'";
	else $where = "Collections.idetablissement='$idetablissement' AND nrinv='$nrinv'";
   
	$result = requete ("SELECT idcollection FROM Collections WHERE $where");
   $ligne = mysqli_fetch_assoc ($result);
   return $ligne['idcollection'];
}

########################################################### Nrunique ###
function Nrunique ($idetablissement, $nrinv) 
# Vérifie que ce numéro d'inventairen'est pas en doublon
# Il faut tenir compte des numérations séparées par établissement,
# sauf dans le cas particulier de numération commune
{
	global $dbase;
	if (isset($dbase['num_commune'])) 
		$result = requete ("SELECT idcollection FROM Collections WHERE  nrinv = '$nrinv' ");
	else 
		$result = requete ("SELECT idcollection FROM Collections 
												WHERE  idetablissement= $idetablissement AND nrinv = '$nrinv' ");
	
	$nb = mysqli_num_rows ($result);
	return $nb == 0;
}

######################################################################### Password_sha1 ###
function Password_sha1 ($passe) 
# Cette fonction remplace l'ancienne fonction PASSWORD de MySQL disparue lors du passage à MySQL8
# PASSWORD('PWD') === CONCAT('*', UPPER(SHA1(UNHEX(SHA1('PWD')))))
# Elle conserve le codage SHA1, moins performant que SHA2 maintenant recommandé
{
	return '*'.strtoupper (sha1 (hex2bin (sha1 ($passe))));
}

######################################################################### Pb ###
function Pb ($index) 
# La variable session 'b_profil' est une chaine de caractères 0 et 1
# La fonction Pb lit le caractère pointé par l'index : valeur 0 ou 1
{
	return substr ($_SESSION['b_profil'], $index, 1);
}

##################################################################### Pb_set ###
function Pb_set ($index, $val) 
# La fonction Pb_set positionne à 1 ou 0 le caractère de la variable session 'b_profil' pointé par 'index'
{
	$char = $val ? '1' : '0';
	$_SESSION['b_profil'] = substr_replace ($_SESSION['b_profil'], $char, $index, 1);
	return;
}

############################################################### Phd_decode ###
function Phd_decode ($dathex, $key)
# Fonction de décodage des chaines Phd_encode
{	
	$data = '';
	$length = strlen($dathex) /2;
	$key=str_pad ($key, $length, $key);		// ajuster la longueur de la clé
	
  for($i = 0; $i < $length; $i++){
  	$k = substr($dathex, $i*2, 2);
  	$data .= chr (base_convert ($k, 16,10));		// décodage hexa
    $data[$i] = $key[$i] ^ $data[$i];						// décryptage
  }
  	
  return $data;
}

############################################################### Phd_encode ###
function Phd_encode($data,$key)
# Cette fonction permet de coder des chaines de caractères transmises par URL 
#  en se protégeant contre les attaques par injection
# Nécessaire pour le traitement des listes : chaines SQL WHERE 
# Le codage par urlencode ne convient pas car non réciproque pour + et espace
{
	$key=str_pad ($key, strlen($data), $key);		// ajuter la longueur de la clé
	$dathex = '';
  for($i = 0; $i < strlen($data); $i++){			// cryptage
		$data[$i] = $data[$i] ^ $key[$i] ;
		$k  =ord ($data[$i]);
		$k16 = str_pad (base_convert ($k, 10, 16), 2, '0', STR_PAD_LEFT);
		$dathex .= $k16;													// codage hexa
  }
  return $dathex;
}

##################################################################### Prepar_photo ###
function Prepar_photo ($l_photo)
# Préparer l'instruction d'appel javascript des photos élargies
# $l_photo : ligne de la table Medias pour le média courant
{
	global $db, $Xvars;
	
	$fid = AdMedia ($l_photo['idmedia'], $db, $l_photo['formatmedia']);					// Adresse du fichier
	
	$Xvars['extension_photo'] = $l_photo['formatmedia'];
	$Xvars['nom_media_file'] = $fid;
	
	$nrinventaire = Nrinventaire ($Xvars['idcollection']);

	// Encoder la légende pour transmission par URL...
	$legende = $nrinventaire.' : '.$l_photo['descrimedia']	;
	$enc_legende = base64_encode ($legende);
	
	$cotemedia = $l_photo['cotemedia'];
	$type = $l_photo['formatmedia'];
	
	if ( $type == "mov") $type = "quicktime";
	// si $type == "mp4" ou $type == "mp3" ou $type == "pdf" : pas de changement...
	
	$page = 'medias.view.php?media='.$fid.'&cotemedia='.$cotemedia.'&legende='.$enc_legende.'&format='.$type;
	$Xvars['mpview'] = "javascript:window.open('".$page."', 'Hostel', 'toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=no, resizable=no, width=10%,height=10%');imagewindow.focus ();";
	return;
}


##################################################################### Profil ###
function Profil ($rw) 
# $rw == 'R' : Lit le profil de l'utilisateur dans la table Rédacteurs et met à jour la variable au format string
# $rw == 'W' : met à jour le profil utilisateur à partir du tableau booléen
#
# Dans le cas d'un visiteur (non inscrit) on utilise la valeur par défaut PROFIL_DEF en lecture. 
# Pas d'action en écriture.
#
# Le rangement du profil dans un "bigint" de 64 bits a été abandonné : non compatibiltés du bigint (?) et des 
# conversions d'une plate-forme à l'autre => traitement caractère par caractère
{
	if ($_SESSION['statut'] == 'visiteur') {
		if ($rw == 'R') {
			$_SESSION['b_profil'] = PROFIL_DEF_B;	  // valeur par défaut
		}			// Dans le cas visiteur && W : aucune action
	
	} else {
		$idrapporteur = $_SESSION['idrapporteur'];
		
		switch ($rw) {
			case 'R' :
				$ligne = mysqli_fetch_assoc (requete ("SELECT profilh FROM Rapporteurs WHERE idrapporteur=$idrapporteur"));
				$profilh = $ligne['profilh'];													// valeur du profil dans la base				
				if (strlen ($profilh)!=12) $profilh = PROFIL_DEF_H;		// si longueur incorrecte on garde la valeur par défaut
				
				// Codage de 12 caractères hexadécimaux en chaine de 48 caractères binaires
				$b_profil = "";
				$thex = array ('0000', '0001', '0010', '0011', '0100', '0101', '0110', '0111',
											 '1000', '1001', '1010', '1011', '1100', '1101', '1110', '1111');
				for ($i=0; $i<12; $i++) {
					$k = hexdec (substr ($profilh, $i, 1));
					$b_profil .= $thex[$k];
				}
				$_SESSION['b_profil'] = $b_profil;
				break;
 		
			case 'W' :
				// Codage chaine de 48 caractères binaires en 12 caractères hexadécimaux
				$profilh =""; 
				$b_profil = $_SESSION['b_profil'];
				for ($i=0; $i<48; $i=$i+4)	{ 
					$q = substr ($b_profil, $i, 4);
					$profilh .= dechex(bindec($q));
				}						
				requete ("UPDATE Rapporteurs SET profilh='$profilh' WHERE idrapporteur=$idrapporteur");			
				break;
		}
	}
	return;
}

################################################################### requete ###
function requete ($requete, $f_msg=TRUE) 
# Effectue une requête MySQLi
# Le cas échéant, affiche un message d'erreur, sauf si le drapeau f_msg est faux
#
# Si $f_msg est vrai : retourne le résultat brut de la requête
# Si $f_msg est faux, retourne le code d'erreur
{
   global $dblink, $result;

   debugMsg ("Requête :$requete.");

   if (false === ($result = mysqli_query ($dblink, $requete))) {
   	  $err = mysqli_errno ($dblink);
   	  if ($f_msg) {
				erreurMsg ("Requête incorrecte : %0", $requete);	
				erreurMsg	("code %0 : %1", $err, mysqli_error ($dblink));		// affichage sur 2 lignes
 	   	} else $result = $err;
   }
   return $result;	
}

################################################################### Section ###
function Section ($idcollection) 
# En fonction d'un idcollection, la fonction renvoie une section : 
# Machines, Documents, ou Logiciels
{   
   $result = requete ("SELECT idmachine, iddocument, idlogiciel FROM Collections WHERE Collections.idcollection = $idcollection");
   $ligne = mysqli_fetch_assoc ($result);
   if ($ligne['idmachine']) return 'Machines';
   if ($ligne['iddocument']) return 'Documents';
   if ($ligne['idlogiciel']) return 'Logiciels';
}

####################################################################### Snv ###
function Snv ($fr, $en) 
# Sélection du champ anglais si affichage en anglais et si celui-ci est non vide
{
	   if ($_SESSION['langue'] == 'FR') return $fr;
	   else if ($en != '') return $en; 
	   else return $fr;
}

################################################################### Tablestruct ###
function Tablestruct ($table) 
# Création d'une table de structure améliorée 
# A partir d'une table, cette fonction renvoie un tableau associatif
# contenant toutes les lignes de résultats avec séparation des types, longueurs et énums
# Index : 	'Field' - nom du champ
# Éléments :	Type, Null, Key, Default, Extra, Length
{

	$tstruct = array ();

	$result = requete ("SHOW COLUMNS FROM $table");
	while ($ligne = mysqli_fetch_assoc ($result)) {

		$tstruct[$ligne['Field']] = $ligne;				// recopie de tous les éléments sauf 'Field' utilisé en index
		$t = explode ('(', $ligne['Type']);				// élaguer la longueur dans le type
		$tstruct[$ligne['Field']]['Type'] = $t[0];
		
		switch (substr($ligne['Type'],0,4)) {
			case 'varc' :
			case 'char' :
			case 'medi' :					// enregistrer la taille des champs caractères et mediumint 
				$tstruct[$ligne['Field']]['Length'] = preg_replace ('/[a-z()]/','', $ligne['Type']);
				break;

			case 'floa' :
				$tstruct[$ligne['Field']]['Length'] = 20;		// longueur imposée...
				break;

			case 'enum' :
				$nn = strlen ($ligne['Type']);		
				$st = substr ($ligne['Type'], 5, $nn-6);
				// enregistrer la liste d'énumération dans le champ 'Length'....
				$tstruct[$ligne['Field']]['Length'] = preg_replace ("/'/",'', $st);  
				break;
	
		default :														// cas tinyint, date, text
				$tstruct[$ligne['Field']]['Length'] = '';
		}
		
		// Cas particuliers : 
		// mot de passe
		if ($ligne['Field'] == 'passe') $tstruct[$ligne['Field']]['Type'] = 'passe';
		// chaines de caractères anormalement longues (ne fonctionne plus en PHP 8 à cause liste enum)
		// if ($tstruct[$ligne['Field']]['Length'] >100) $tstruct[$ligne['Field']]['Type'] = 'text';
	}	
	return $tstruct;
}

######################################################################## Tr ###
function Tr ($fr, $en) 
# Choix entre 2 chaînes de caractères (ou 2 variables) suivant la langue français ou anglais
# Si la chaîne figure dans la table des alias, retourner l'alias.
{ 
	global $dbase, $t_alias;
		
	$str = ($_SESSION['langue'] == 'FR') ? $fr : $en;
	
  if ($str_alias = @$t_alias[$str]) return $str_alias;
  else return $str;
         $s = $ts;
}

################################################################= Translate ###
function Translate ($s) 
# Traduction de la chaine "s" d'après la table "Translations"
# avec traitement des arguments %1, %2... 
# Si la traduction n'est pas trouvée, la chaine est retournée inchangée
{
   if ($_SESSION['langue'] != 'FR') {
      global $Translations;	   
      if ($ts = @$Translations[$s]) 
         $s = $ts;
   }

   if (($na = func_num_args ()) <= 1)
      return $s;
   
   $args = func_get_args ();
   if (($na == 2) && (is_array ($args[1])))
      $args = $args[1];
   else
      array_shift ($args);
			return preg_replace_callback ("/\%([0-9])/", 
   					function ($matches) use ($args) {static $n=0; $arg=$args[$n]; $n++; return $arg;},
   					$s);

}

#================================================================== TStructure ===
function TStructure ($tables) 
# Création d'une table de structure simplifiée pour les tables SQL en parmètre
# elle ne comporte qu'un index 'nom du champ' 
# et une seule valeur : longueur ou liste d'enum 
#
# Un second index 'nomduchamp_d' reçoit la valeur par défaut des champs 'id...'
{

	if (!is_array ($tables)) $tables = array ($tables);
	$tstruct = array ();
	$vdefault = array ();
	
	foreach ($tables as $tab) {
		$result = requete ("SHOW COLUMNS FROM $tab ");
		while ($ligne = mysqli_fetch_assoc ($result)) {
			switch (substr($ligne['Type'],0,4)) {
			case 'varc' :
			case 'char' :
			case 'mediumint' :					// enregistrer la taille des champs caractères et mediumint 
				$tstruct[$ligne['Field']] = preg_replace ('/[a-z()]/','', $ligne['Type']);
				break;
			case 'float' :
				$tstruct[$ligne['Field']] = 20;		// longueur imposée...
				break;
			case 'enum' :
				$nn = strlen ($ligne['Type']);		// enregistrer la liste d'énumération
				$st = substr ($ligne['Type'], 5, $nn-6);
				$tstruct[$ligne['Field']] = preg_replace ("/'/",'', $st);
				break;
			default :														// cas tinyint, date, text
				$tstruct[$ligne['Field']] = '';
				if (substr($ligne['Field'], 0 ,2)=='id') $tstruct[$ligne['Field'].'_d'] = $ligne['Default'];
			}
		}
	}
	return $tstruct;
}

?>