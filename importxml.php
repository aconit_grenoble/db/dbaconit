<?php
# v25				190712	PhD		Création
# v25.3			191101	PhD		Correction dossier ZIP, ajout traitement batiments
# v25.4			191208	PhD		Complété affichage liens
# v25.6			200121	PhD		Testé 'personne' sur civil, prenom et nom, retouché affichage liens incomplet
# v25.7			200327	PhD		Ajouté indications id dans messages anomalies, corrigé écritures Col_Org Col_Per MEDIA
#														Effacement du dossier temporaire en fin d'opération
# v25.8			200411	PhD		Ajout custom_css
# v25.9.02	201008	PhD		Complété les messages d'erreurs
# v25.11.01	210501	PhD		Modification MySQL 8 : pas de valeur '' dans champ numérique
# v25.12		211026	PhD		traitement comparaison cotemedia, ajouté cas forçage d'écriture, ajouté effacement des liens
# v25.14.02	220919	PhD		Corrigé dernier paragraphe table Col_Med et non COL_MED
# v26.1			230223	PhD		Déplacé ligne '$idaux =$ligne[$nom_id];' de 237 en 242 (cas MàJ uniquement)
###

$custom_css = "importxml.css";
require_once ('init.inc.php');
require_once ('importxml.inc.php');

/* Protection des entrées -------------------------------------------------------
'action'				- POST non accessible - uniquement testé isset
'auts'					- POST (checkbox) - uniquement testé sur valeurs connues
'origin'				- POST (radio) - test des valeurs connues
'server_file'		- POST  - Filtré NormIN
'zip_file'			- TRAITÉ PAR $_FILES
------------------------------------------------------------------------------ */
global $erreur;

## Traitement des entrées :
###########################

$f_action = isset ($_POST['action']);
$t_auts = (array)@$_POST['auts'];
$server_file = NormIN ('server_file');
$origin = @$_POST['origin'];

## Initialisations :
####################
// // Tables de correspondance 
$t_etabl	= array();		// idetabl_d -> idetabl_l
$t_col		= array();		// idcollection_d -> idcollection_l
$t_mate		= array();		// idmateriau_d -> idmateriau_l
$t_mot		= array();		// idmot_d -> idmot_l
$t_org		= array();		// idorganisme_d -> idorganisme_l
$t_per		= array();		// idpersonne_d -> idpersonne_l
$t_med		= array();		// idmedia_d -> idmedia_l
	

########################### Traitement principal ####################################

if ($f_action) {
### Lire et mettre en forme le fichier XML
#-----------------------------------------------------------------
# NOTE : tous les objets SimpleXML sont notés avec une majuscule |
#-----------------------------------------------------------------

	if ($origin == 'local') {
		// Téléchargement du fichier ZIP
		$zip_tmp = $_FILES['zip_file']['tmp_name'];

		if (!$zip_tmp) ErreurMsg (Tr ('Fichier ZIP non sélectionné', 'ZIP file not selected'));
		else {
			// On récupère le nom de base et le nom complet dans le dossier tempo
			$fbase = basename ($_FILES['zip_file']['name'], '.zip');	// nom de base (sans extension)
			$zip_file = $dir_tempo.$_FILES['zip_file']['name'];

			if (!move_uploaded_file($zip_tmp, $zip_file)) 
				ErreurMsg (TR ('Erreur de transfert fichier ZIP', 'Wrong ZIP file move'));
	
			Fil ('h2', Tr ("IMPORT DU FICHIER LOCAL : ", "Import local file: ").$zip_file);
		}
		
	}
	// Si le fichier est déjà dans le dossier tempo, l'identifier
	else {$zip_file = $dir_tempo.$server_file;

		// On reconstruit le nom du fichier (ainsi, il peut être reçu avec ou sans extension)
		$fbase = basename ($zip_file, '.zip');	// nom de base (sans extension)
		$zip_file = $dir_tempo.$fbase.'.zip';
	
		Fil ('h2', Tr ("IMPORT DU FICHIER SERVEUR: ", "Import server file: ").$zip_file);
	}
	
	
	
### Ouverture du fichier ZIP , puis du fichier XML #############################################

	// Vérifier que le fichier ZIP est sélectionné et qu'il existe.
  if (file_exists($zip_file)) {
  
  	// Créer un répertoire pour recevoir xml et médias
  	$dir_dezip = $dir_tempo.$fbase.'/';
  	mkdir ($dir_dezip);
  
		$zip = new ZipArchive();
		// Extractions multiples dans le dossier courant.
		$zip->open($zip_file);	
		if (!$zip->extractTo($dir_dezip)) ErreurMsg (Tr ('Extraction ZIP impossible', 'Wrong ZIP extract')); 	
		$zip->close();
	
	} else ErreurMsg (Tr ('Fichier ZIP introuvable', 'ZIP file not found'));


	// SORTIE IMMÉDIATE si le traitement des fichiers a causé des erreurs
	if ($erreur) Fin (); // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
  

  
#-- Ouvrir le fichier XML	------------------------------------------------------------
	$Import = Xopen ($dir_dezip.$fbase.'.xml');
	debug (2, 'IMPORT', $Import);
	$Import_item = $Import->attributes();
	$date = $Import_item->date;
	Fil ('h2', "Date : ". $date);
	
	
### Traitement de la table des établissements #########################################
	$TABLE_ETABLISSEMENTS = $Import->TABLE_ETABLISSEMENTS;

	// Pour chaque établissement importé
	foreach ($TABLE_ETABLISSEMENTS->ETABLISSEMENT as $Etablissement) {
		$nometabl = (string)$Etablissement->etablissement;
	
		// Tester l'existence en base locale, créer si autorisé
		$idetablissement_l = TableCheck ('Etablissements', 'etablissement', $nometabl, in_array ("etabl", $t_auts), $Etablissement);

		// Enregistrer dans la table de correspondance
		if ($idetablissement_l) {
			$idetablissement_d =(string) $Etablissement->idetablissement_d;
			$t_etabl[$idetablissement_d] = $idetablissement_l;
		}
	}

	if (count ($t_etabl) == 0) {
		Fil ('r', Tr ("Aucun établissement reconnu - ARRÊT DU TRAITEMENT", "No recognized organization - PROCESS ABORTED"));
		Fin ();
		Exit;			// EXIT >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
		}
				
### Traitement de la table principale Collections #########################################
	$TABLE_COLLECTIONS = $Import->TABLE_COLLECTIONS;

	// Pour chaque établissement importé
	foreach ($TABLE_COLLECTIONS->COLLECTION as $Collection) {
		$idetablissement_d = (string)$Collection->idetablissement_d;
		$idetablissement_l = $t_etabl[$idetablissement_d];			// Prendre le No d'établissement correspondant en local
		$nrinv = (string)$Collection->nrinv;										// Le No d'inventaire est constant
		$type_d = (string)$Collection->type;
	
#-- Tester l'existence en base locale, créer si autorisé	------------------------------------------------------------
		$res_col = requete (
			"SELECT idcollection, type, idmachine, iddocument, idlogiciel 
				FROM Collections WHERE idetablissement='$idetablissement_l' AND nrinv='$nrinv'");
		if (0 == (mysqli_num_rows ($res_col))) {

		// L'objet n'existe pas localement, créer une fiche quasi vide 
			$mode_maj = FALSE;
			$res_col = requete ("INSERT INTO Collections SET idetablissement='$idetablissement_l', nrinv='$nrinv'");			
			$idcollection_l = mysqli_insert_id ($dblink);		// Récupérer l'index		
			
		} else {
			$mode_maj = TRUE;
			$ligne = mysqli_fetch_assoc($res_col); 

			// si pas autorisation de mise à jour, abandon pour cette fiche
			if (!in_array ("rempl", $t_auts)) {
				Fil ('r', Tr ('Remplacement de la fiche non autorisé : ', 
					'Update this record not allowed: ').$idetablissement_l.'-'.$nrinv);
				break;		// on sort de la boucle courante foreach
			}

			// Test de cohérence : les 2 fiches sont elles de même type ?
			if ($ligne['type'] != $type_d) {
				Fil ('r', Tr ("Type de la fiche importée non compatible avec fiche locale ", 
					"Type of imported record not compatible with local record ").$idetablissement_l.'-'.$nrinv);			
####				break;		// on sort de la boucle courante foreach COLLECTION
			}
			
			$idcollection_l = $ligne['idcollection'];
		}	

		// Enregistrer dans la table de correspondance
		if ($idcollection_l) {
			$idcollection_d =(string) $Collection->idcollection_d;
			$t_col[$idcollection_d] = $idcollection_l;
		}

		
#-- Composer le SET pour la fiche locale	-----------------------------------------------------------------
		$set = "";
		foreach ($Collection as $nchamp=>$champ) {	
			// Sortie si appel de table auxiliaire Machines, Documents ou Logiciels
			if ($nchamp == 'table_auxiliaire') {
				$table_auxiliaire = $champ;
				break;		// on sort de la boucle courante foreach COLLECTION	
			}
			if ($nchamp!='idetablissement_d' AND $nchamp!='idcollection_d')	{		// Ne pas retraiter dans le SET
			
				if (substr ($nchamp, 0, 2)!= 'id') {				// Champ simple
					if ($champ == '') $set .= "$nchamp=NULL, ";			// (MySQL 8 n'accepte pas val vide pour champ numérique)
					else $set .= "$nchamp='".NormSQL ($champ)."', ";
				} else {																		// Champ index
					$tn = Id2nom ($nchamp);
					$idchamp_l = TableCheck ($tn[0], $tn[1], $champ, in_array ("tordre", $t_auts));
					if ($idchamp_l) $set .= "$nchamp='$idchamp_l', ";				
				}
			}
		}											// il reste à ajouter idmachine... on ne 'rtrim' pas le set
debug (2, 'SET partiel', $set);

#-- Préparation de la table auxiliaire Machines, Documents ou Logiciels ----------------------------------------------
		$Auxiliaire = $Collection->$table_auxiliaire;

		// Composer le SET pour la table auxiliaire
		$setaux = "";
		foreach ($Auxiliaire as $aaa=>$bbb) {	
			foreach ($bbb as $nchamp=>$champ) {				// C'est absurde mais comme ça, ça marche...
				if ($nchamp!='idmachine_d' AND $nchamp!='iddocument_d' AND $nchamp!='idlogiciel_d')	{	
				
					if (substr ($nchamp, 0, 2)!= 'id') {			// Champ simple
						if ($champ == '') $setaux .= "$nchamp=NULL, ";			// (MySQL 8 n'accepte pas val vide pour champ numérique)
						else $setaux .= "$nchamp='".NormSQL ($champ)."', ";
					} else {																	// Champ index
						$tn = Id2nom ($nchamp);
						$idchamp_l = TableCheck ($tn[0], $tn[1], $champ, in_array ("tordre", $t_auts));
						if ($idchamp_l) $setaux .= "$nchamp='$idchamp_l', ";				
					}	
				}
			}
		}
		$setaux = rtrim ($setaux, " ,");
debug (2, 'SETAUX', $setaux);

#-- Écriture de la table auxiliaire Machines, Documents ou Logiciels	------------------------------------------------
		$nom_table = $type2aux[$type_d][0];
		$nom_id = $type2aux[$type_d][1];

		// Mode mise à jour
		if ($mode_maj){
			// vérifier la cohérence !
			if ($nom_table != $type2aux[strtolower ($table_auxiliaire)][0]) DIE ('Incohérence des tables auxiliaires');
			$idaux =$ligne[$nom_id];
			$result_aux = requete ("UPDATE $nom_table SET $setaux WHERE $nom_id='$idaux'");
			
		} else {
		// Mode création
			$result_aux = requete ("INSERT INTO $nom_table SET $setaux");
		 	$idaux = mysqli_insert_id ($dblink);				
		}
		
		// Contrôle écriture
		if (!$result_aux){
			Fil ('r', Tr ("Erreur écriture table auxiliaire", 'Write error auxiliary table').$nom_table);
		
		} else {
			Fil ('v', Tr ("Écriture table auxiliaire ", 'Write into table ').$nom_table.' ['.$idaux.']'); 		
			// Compléter le set pour la table Collections
			$set .= "$nom_id= '$idaux'"; 
debug (2, 'SET', $set);
		
#-- Et enfin écriture de la table Collections	-----------------------------------------------------------------

			$result_col = requete ("UPDATE Collections SET $set WHERE idcollection='$idcollection_l'");
			Fil ('v', Tr ("Écriture table Collections : ", 'Write into table Collections: ')
				.$idetablissement_l.'-'.$nrinv.' ['.$idcollection_l.']'); 
		}
debug (2,'T_COL', $t_col);
	}

### CAS MISE À JOUR : effacement des liens #######################################
# Si on est en mode mise à jour, il est nécessaire d'effacer tous les liens (Col_xxx) correspondant aux fiches traitées. # Les liens actifs vont ensuite être re-créés
###
	if (in_array ("rempl", $t_auts)) {
		foreach ($t_col as $idcollection_d => $idcollection_l) {
			Requete ("DELETE FROM Col_Mate WHERE idcollection = $idcollection_l");
			Requete ("DELETE FROM Col_Med WHERE idcollection = $idcollection_l");
			Requete ("DELETE FROM Col_Mot WHERE idcollection = $idcollection_l");
			Requete ("DELETE FROM Col_Mouv WHERE idcollection = $idcollection_l");
			Requete ("DELETE FROM Col_Org WHERE idcollection = $idcollection_l");
			Requete ("DELETE FROM Col_Per WHERE idcollection = $idcollection_l");
			Requete ("DELETE FROM Liens WHERE idcol1=$idcollection_l OR idcol2=$idcollection_l");
		}
	}

### Traitement de la table des batiments #########################################
	$TABLE_BATIMENTS = $Import->TABLE_BATIMENTS;

	// Pour chaque batiment importé
	foreach ($TABLE_BATIMENTS->BATIMENT as $Batiment) {
		$batiment = (string)$Batiment->batiment;		
		$cotebatiment = (string)$Batiment->cotebatiment;
		
		// Tester l'existence en base locale,
		// TableCheck n'est pas utilisable à cause du traitement de l'index idbatiment	
		$resbat = requete ("SELECT idbatiment FROM Batiments 
												WHERE batiment=\"$batiment\" AND cotebatiment LIKE '$cotebatiment'");
		if (0 == (mysqli_num_rows ($resbat))) {

			// N'existe pas localement : créer l'élément en table locale
			// Ajout toujours autorisé...
			$idetablissement_d = (string)$Batiment->idetablissement_d;
			$idetablissement_l = $t_etabl[$idetablissement_d];
			
			// Créer l'élèment en table locale				
			$result = requete ("INSERT INTO Batiments 
				SET idetablissement=$idetablissement_l, cotebatiment='$cotebatiment', batiment=\"$batiment\" ");
			if (!$result) erreurMsg (Tr ("Erreur d'écriture dans Batiments", "Writing error in Batiments")); 		
			else {
				Fil ('v', Tr ("Élément «".$batiment."» ajouté en table : Batiments", "Item «".$batiment."» added in table:  Batiments"));
			}
		}	
	}  

	
### Traitement de la table des liens #########################################
	$TABLE_LIENS = $Import->TABLE_LIENS;

	// Pour chaque lien importé
	foreach ($TABLE_LIENS->LIEN as $Lien) {
		$idcol1_d = (string)$Lien->idcol1_d;
		$idcol2_d = (string)$Lien->idcol2_d;
		
		if (array_key_exists ($idcol1_d, $t_col) AND array_key_exists ($idcol2_d, $t_col)) {
			$idcol1_l = $t_col[$idcol1_d];
			$idcol2_l = $t_col[$idcol2_d];
			$res = requete ("SELECT idcol1 FROM Liens WHERE idcol1='$idcol1_l' AND idcol2='$idcol2_l'");

			if (0 == (mysqli_num_rows ($res))){										// Lien à créer
				$commentlien = (string)$Lien->commentlien; 
					$commentlien = NormSQL ($commentlien);
				$famille_d = (string)$Lien->idfamille; 
				// Rechercher la famille 
				$idfamille_l = Tablecheck ('Familles', 'famille', $famille_d,  in_array ("tordre", $t_auts));
				if ($idfamille_l) {
					$res = requete ("INSERT INTO Liens 
						SET idcol1='$idcol1_l', idcol2='$idcol2_l', commentlien='$commentlien', idfamille='$idfamille_l'");
					Fil ('v', Tr ("Lien inter-objets enegistré : $idcol1_l($idcol1_d) - $idcol2_l($idcol2_d)", 
												'Link between items recorded: $idcol1_l($idcol1_d) - $idcol2_l($idcol2_d)'));
				}	else Fil ('r', 
									Tr ("Lien inter-objets non enregistré : ($idcol1_d) - ($idcol2_d) - Relation inconnue : $famille_d", 
											'Link between items not recorded: ($idcol1_d) - ($idcol2_d) - Unknown link kind: $famille_d'));
						
			} else Fil ('v', Tr ("Lien inter-objets pré-existant", 'Link between items already existing'));
		} else Fil ('r', Tr ("Lien incomplet non créé : ($idcol1_d) - ($idcol2_d)", 
													'Incomplete link not recorded: ($idcol1_d) - ($idcol2_d)'));	
	}

### Traitement de la table des matériaux #########################################
	$TABLE_MATERIAUX = $Import->TABLE_MATERIAUX;

	// Pour chaque matière importée
	foreach ($TABLE_MATERIAUX->MATERIAU as $Materiau) {
		$nommate = (string)$Materiau->materiau;
	
		// Tester l'existence en base locale, créer si autorisé
		$idmateriau_l = TableCheck ('Materiaux', 'materiau', $nommate, in_array ("tordre", $t_auts), $Materiau);

		// Enregistrer dans la table de correspondance
		if ($idmateriau_l) {
			$idmateriau_d =(string) $Materiau->idmateriau_d;
			$t_mate[$idmateriau_d] = $idmateriau_l;
		}
	}
debug (2, 'T_MATE', $t_mate);

### Traitement de la table de liaison Col_Mate #########################################
	$TABLE_COL_MATE = $Import->TABLE_COL_MATE;

	// Pour chaque lien importé
	foreach ($TABLE_COL_MATE->COL_MATE as $Col_mate) {
		
		$idmateriau_d = (string)$Col_mate->idmateriau_d;
		if (!array_key_exists ($idmateriau_d, $t_mate)) {
			Fil ('r', Tr ("Index matériau inconnu : $idmateriau_d", 'Material index unknown: $idmateriau_d'));
		} else {		
			$idcollection_d = (string)$Col_mate->idcollection_d;
			$idcollection_l = $t_col[$idcollection_d];
			$idmateriau_l = $t_mate[$idmateriau_d];

			$res = requete 
				("SELECT idcollection FROM Col_Mate WHERE idcollection=$idcollection_l AND idmateriau=$idmateriau_l");
			if (!mysqli_num_rows ($res)){					// Si le lien n'existe pas déjà (il existe en cas de remplacement)
				$res = requete 
					("INSERT INTO Col_Mate SET idcollection=$idcollection_l, idmateriau=$idmateriau_l");
				Fil ('v', Tr ("Ajouté lien matériau", 'Material link added'));
			} else Fil ('v', Tr ("Lien matériau pré-existant", 'Material link already existing'));
		}
	}
	
### Traitement de la table des mots clés #########################################
	$TABLE_MOTSCLES = $Import->TABLE_MOTSCLES;

	// Pour chaque mot clé importé
	foreach ($TABLE_MOTSCLES->MOTCLE as $Motcle) {
		$nommot = (string)$Motcle->motcle;
	
		// Tester l'existence en base locale, créer si autorisé
		$idmotcle_l = TableCheck ('Motscles', 'motcle', $nommot, in_array ("tordre", $t_auts), $Motcle);
		// Enregistrer dans la table de correspondance
		if ($idmotcle_l) {
			$idmotcle_d =(string) $Motcle->idmotcle_d;
			$t_mot[$idmotcle_d] = $idmotcle_l;
		}
	}
debug (2, 'T_MOT', $t_mot);

### Traitement de la table de liaison Col_Mot #########################################
	$TABLE_COL_MOT = $Import->TABLE_COL_MOT;

	// Pour chaque lien importé
	foreach ($TABLE_COL_MOT->COL_MOT as $Col_mot) {
		
		$idmotcle_d = (string)$Col_mot->idmotcle_d;
		if (!array_key_exists ($idmotcle_d, $t_mot)) {
			Fil ('r', Tr ("Index motclé inconnu : $idmotcle_d", 'Keyword index unknown: $idmotcle_d'));
		} else {		
			$idcollection_d = (string)$Col_mot->idcollection_d;
			$idcollection_l = $t_col[$idcollection_d];
			$idmotcle_l = $t_mot[$idmotcle_d];
			
			$res = requete 
				("SELECT idcollection FROM Col_Mot WHERE idcollection=$idcollection_l AND idmotcle=$idmotcle_l");
			if (!mysqli_num_rows ($res)){					// Si le lien n'existe pas déjà (cas des remplacements)
				$res = requete 
					("INSERT INTO Col_Mot SET idcollection=$idcollection_l, idmotcle=$idmotcle_l");
				Fil ('v', Tr ("Ajouté lien motclé", 'Keyword link added'));
			} else Fil ('v', Tr ("Lien motclé pré-existant", 'Keyword link already existing'));
		}
	}
	

### Traitement de la table des organismes #########################################
	$TABLE_ORGANISMES = $Import->TABLE_ORGANISMES;

	// Pour chaque personne importée
	foreach ($TABLE_ORGANISMES->ORGANISME as $Organisme) {
		$nomorg = (string)$Organisme->onom;

		// Tester l'existence en base locale, créer si autorisé
		$idorganisme_l = TableCheck ('Organismes', 'onom', $nomorg, in_array ("orga", $t_auts), $Organisme);

		// Enregistrer dans la table de correspondance
		if ($idorganisme_l) {
			$idorganisme_d =(string) $Organisme->idorganisme_d;
			$t_org[$idorganisme_d] = $idorganisme_l;
		}
	}
debug (2, 'T_ORG', $t_org);

### Traitement de la table de liaison Col_Org #########################################
	$TABLE_COL_ORG = $Import->TABLE_COL_ORG;

	// Pour chaque lien importé
	foreach ($TABLE_COL_ORG->COL_ORG as $Col_org) {
		$relation_d = (string)$Col_org->idrelation;
		$idrelation_l = Tablecheck ('Relations', 'relation', $relation_d,  in_array ("tordre", $t_auts));
		$idcollection_d = (string)$Col_org->idcollection_d;

		if (!$idrelation_l OR !array_key_exists ($idcollection_d, $t_col)) {
			Fil ('r', Tr ("Lien organisme non créé - idcollection_d=$idcollection_d, relation='$relation_d'", 
											"Organization link not recorded - idcollection_d=$idcollection_d, relation='$relation_d"));	
		} else {
			$idcollection_l = $t_col[$idcollection_d];
			$idorganisme_d = (string)$Col_org->idorganisme_d;
			$idorganisme_l = $t_org[$idorganisme_d];
			
			$res = requete 
				("SELECT idcollection FROM Col_Org 
					WHERE idcollection=$idcollection_l AND idorganisme=$idorganisme_l AND idrelation=$idrelation_l");
			if (!mysqli_num_rows ($res)){						// Si le lien n'existe pas déjà (cas des remplacements)				
				$res = requete 
				("INSERT INTO Col_Org SET idcollection=$idcollection_l, idorganisme=$idorganisme_l, idrelation=$idrelation_l");
				Fil ('v', Tr ("Ajouté lien organisme", 'Organization link added'));
			} else Fil ('v', Tr ("Lien organisme pré-existant", 'Organim link already existing')
									."[idcollection_d ]" .$idcollection_d. "/ [idorganisme_d] ".$idorganisme_d);
		}	
	}
	
### Traitement de la table des personnes #########################################
	$TABLE_PERSONNES = $Import->TABLE_PERSONNES;

	// Pour chaque personne importé
	foreach ($TABLE_PERSONNES->PERSONNE as $Personne) {
		$nompcivil = (string)$Personne->pcivil;
		$nomprenom = (string)$Personne->pprenom;
		$nompnom = (string)$Personne->pnom;
		$where = "pnom=\"$nompnom\" AND pprenom=\"$nomprenom\" AND pcivil=\"$nompcivil\"";
		
		// Tester l'existence en base locale, créer si autorisé
		$idpersonne_l = TableCheck ('Personnes', '', $where, in_array ("perso", $t_auts), $Personne);

		// Enregistrer dans la table de correspondance
		if ($idpersonne_l) {
			$idpersonne_d =(string) $Personne->idpersonne_d;
			$t_per[$idpersonne_d] = $idpersonne_l;
		}
	}
debug (2, 'T_PER', $t_per);

### Traitement de la table de liaison Col_Per #########################################
	$TABLE_COL_PER = $Import->TABLE_COL_PER;

	// Pour chaque lien importé
	foreach ($TABLE_COL_PER->COL_PER as $Col_per) {
		$relation_d = (string)$Col_per->idrelation;
		$idrelation_l = Tablecheck ('Relations', 'relation', $relation_d, in_array ("tordre", $t_auts));
		$idcollection_d = (string)$Col_per->idcollection_d;
		
		if (!$idrelation_l OR !array_key_exists ($idcollection_d, $t_col)) {
			Fil ('r', Tr ("Lien personne non créé - idcollection_d=$idcollection_d, relation='$relation_d'", 
										"Person link not recorded - idcollection_d=$idcollection_d, relation='$relation_d'"));
		} else {		
			$idcollection_l = $t_col[$idcollection_d];
			$idpersonne_d = (string)$Col_per->idpersonne_d;
			$idpersonne_l = $t_per[$idpersonne_d];
			
			$res = requete 
				("SELECT idcollection FROM Col_Per 
					WHERE idcollection=$idcollection_l AND idpersonne=$idpersonne_l AND idrelation=$idrelation_l");
			if (!mysqli_num_rows ($res)){					// Si le lien n'existe pas déjà (cas des remplacements)
				$res = requete 
					("INSERT INTO Col_Per SET idcollection=$idcollection_l, idpersonne=$idpersonne_l, idrelation=$idrelation_l");
				Fil ('v', Tr ("Ajouté lien personne", 'Person link added'));
			} else Fil ('v', Tr ("Lien personne pré-existant", 'Person link already existing')
												."[idcollection_d ]" .$idcollection_d. "/ [idpersonne_d] ".$idpersonne_d);
		}
	}
	

### Traitement de la table des médias #########################################
	$TABLE_MEDIAS = $Import->TABLE_MEDIAS;

	// Pour chaque média importé
	foreach ($TABLE_MEDIAS->MEDIA as $Media) {
		$cotemedia = (string)$Media->cotemedia;
		$formatmedia = (string)$Media->formatmedia;



	
		// Tester l'existence en base locale, créer si autorisé
		// La comparaison porte sur le nom 'cotemedia' du media : 
		// – il faut impérativement qu'il soit unique !
				// Si la cae "forcer" du formulaire est coché, il faut utiliser le cas d'exception de la fonction
				$force = (in_array ("force", $t_auts)) ? 'FORCE' : TRUE;
		$idmedia_l = TableCheck ('Medias', 'cotemedia', $cotemedia, $force, $Media);

		// Enregistrer dans la table de correspondance
		if ($idmedia_l) {
			$idmedia_d =(string) $Media->idmedia_d;
			$t_med[$idmedia_d] = $idmedia_l;
		
# Copie du media
			// Le fichier existe-til déjà ,
			$admedia = file_exists (AdMedia($idmedia_l, $db, $formatmedia, '', 'rel', 'w'));
			// On copie de toutes façons
			$rcopy = copy ($dir_dezip.$idmedia_d.'.'.$formatmedia, AdMedia($idmedia_l, $db, $formatmedia, '', 'rel', 'w'));
			// et la vignette
			copy ($dir_dezip.'v'.$idmedia_d.'.'.$formatmedia, AdMedia($idmedia_l, $db, $formatmedia, 'v', 'rel', 'w'));

			if (!$admedia)	
				Fil ('v', Tr ("Ajouté média : $cotemedia", "Add media: $cotemedia")." ($idmedia_d -> $idmedia_l)");
			else 
				Fil ('v', Tr ("Mise à jour du media : $cotemedia", 'Update media: $cotemedia')." ($idmedia_d -> $idmedia_l)");
		}
	}	
debug (2, 'T_MED', $t_med);
	

### Traitement de la table de liaison Col_Med #########################################
	$TABLE_COL_MED = $Import->TABLE_COL_MED;

	// Pour chaque média importé
	foreach ($TABLE_COL_MED->COL_MED as $Col_med) {
		$ordre_media = (string)$Col_med->ordre_media;
		$mediacle = (string)$Col_med->mediacle;
		
		$idcollection_d = (string)$Col_med->idcollection_d;
		if (!array_key_exists ($idcollection_d, $t_col)) break;		// si pas d'autorisation de MàJ d'une fiche
		$idcollection_l = $t_col[$idcollection_d];
		
		$idmedia_d = (string)$Col_med->idmedia_d;
		$idmedia_l = $t_med[$idmedia_d];
		
		$res = requete 
			("SELECT idcollection FROM Col_Med
				WHERE idcollection='$idcollection_l' AND idmedia='$idmedia_l'");
		if (!mysqli_num_rows ($res)){					// Si le lien n'existe pas déjà (cas des remplacements)
			$res = requete ("INSERT INTO Col_Med SET idcollection='$idcollection_l', idmedia='$idmedia_l', 
											ordre_media='$ordre_media', mediacle='$mediacle'");
			if ($res)	Fil ('v', Tr ("Ajouté lien média $idmedia_l", "Add media link $idmedia_l"));
			else Fil ('r', Tr ("Écriture lien média  $idmedia_l refusée", 'Wrong media link $idmedia_l'));	
			
		} else Fil ('v', Tr ("Lien media pré-existant", 'Media link already existing')
												."[idcollection_d ]" .$idcollection_d. "/ [idmedia_d] ".$idmedia_d);
	}

####### Effacement du dossier temporaire
UnlinkRecursive($dir_dezip, TRUE);
}

###############################################################
# Affichage du formulaire de commande
###############################################################
Debut ();

$Xvars['dir_tempo'] = $dir_tempo;

$liste_xml = Xopen ('./XML_modeles/importxml.xml') ;
Xpose ($liste_xml);

?>
