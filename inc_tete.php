<!DOCTYPE html >
<html lang="fr">
<?php
# v14.2		PhD	151214	Création extrait de fonction Debut
# v15			PhD	160106	Basculement HTML5
# v15.5		PhD	161101	Suppression variable mmsg
# v21			PhD	171122	Ajout retour_DBAconit
# v25.1		PhD	190806	PHP7 : Corrigé appel ?php 
# v25.4		PhD	191208	Simplifié retour_DBAconit pour accepter https
# v25.7		PhD	200322	Ajout appel fichier de style dbaconit_plus
#					EML	200328	Ajout pour custom style UGA
# v25.8		PhD	200411	Reprise mécanisme plus_css et custom_css
# v25.13	PhD	220409	Modif appel feuille de style pour appel à partir des plugin
?>

	<head>
		<title><?php echo "$DBAconit : $local_titre"; ?></title>
		<meta charset="utf-8" />
    <link rel="stylesheet" href="../dbaconit/dbaconit.css" type="text/css" />
    
<?php 
    // Appel d'un fichier de style supplémentaire s'il existe
		if(file_exists ($dir_plugin.$dbaconit_plus)) 
    	echo "<link rel='stylesheet' href='$dir_plugin$dbaconit_plus' type='text/css' />";	
    
    // Appel d'un fichier de style custom si désigné dans le fichier de config.php
		if(!empty($custom_css) and file_exists ($dir_plugin.$custom_css)){
    	echo "<link rel='stylesheet' href='$dir_plugin$custom_css' type='text/css' />";	}
?>
			
		<link rel="stylesheet" href="dbaconit_imp.css" type="text/css" media="print" />
		<link rel="icon" type="image/png" href="commun/favicon.png" />
        
    <script type="text/javascript">
        
			var modifier = false;
	
      function toutselect (nomf){
	   		var ptrf = document.forms[nomf];
        for (var i=0;i<ptrf.length;i++) {
        	var e = ptrf[i];
         	if ((e.name != 'allbox') && (e.type=='checkbox')) e.checked = ptrf['allbox'].checked;
        }
    	}
		
			function ValiDate (link) {
	   		var s=link.value.split ('-');
	   		while (s.length < 3) s[s.length] = 0;
					link.value = ("0000"+s[0]).substr (-4)+'-'+('00'+s[1]).substr (-2)+'-'+('00'+s[2]).substr (-2);
			}

			<!-- Branchement en retour vers accueil DBAconit -->
			function retour_DBAconit() {
				<!-- Recupere URL du programme : Remonter d un niveau-->
				newURL = '';
				decoupURL = location.href.split('/'); 
				for (i=0, n = decoupURL.length-1; i<n; i++) { newURL += decoupURL[i]+'/'; }
				<!-- Branchement 	-->		
				window.location.href=newURL;
			}

			<!-- Fonctions Javascript propres au traitement en cours -->
		 	<?php if (isset ($jscript)) echo "$jscript" ?> 

		</script>
	</head>  

	<body onload="focus ();">
<!-- fin inc_tete -->