<?PHP
# v25				190529	PhD		Lecture des paramètres jusqu'à élément vide, ajout dans table alias,
#														controle de No version Base
# v25.1			190727	PhD		Fixer le codage caractère de MySQL
# v25.4			191212	PhD		Ajouté traitement paramètre 'ponderations'
# v25.5			191230	PhD		'ponderations' devient 'poids_tmarques'
# v25.6.03	200310	PhD		Spécifié codage base en UTF-8
# v25.9.03	201106	PhD		Ajouté forçage du profil par défaut en cas d'entrée directe
# v25.10		210213	PhD		MySQL8 : autoriser le mode permissif pour les DATE ; fixer l'idetablissment par défaut
# v25.13		220401	PhD		Commentaires : particularités de numération nrinv fixées par $dbase
# v25.14		220717	PhD		Controlé is_array avant in_array (PHP 8.1) 
# v25.14.01	220803	PhD		Corrigé test droits (conflits) (PHP 8.1)
# v26.1.01	230325	FWe		Ajout possibilité de spécifier le port MySQL
#
#################################################################################
# Note : 'init.inc' est appelé de 3 façons :
# - en retour de 'index.xml' :
# 				Il faut contrôler les changements d'utilisateur et de base.
# - par un autre module, dans le cadre d'une session standard, déjà activée
# 				dans ce cas toutes les variables de session sont en place.
# - par un autre module, dans le cadre d'un appel isolé, non passé par l'identification 
# 				dans ce cas, l'appel doit nécessairement comporter le paramère 'db'
# 				et éventuellement 'langue'
# 
################################### Initialisations constantes et fonctions #####

require_once ('globalvars.php');	// Charger les variables globales
require_once ('globalfcts.php');	// Charger les sous-programmes communs
require_once ('xml_inc.php');					// Moteur XML, utilisé partout
require_once ('xml_inc_html5.php');		// extension pour code HTML5 correct (non XHTML)
global 	$dblink,									// Lien base de données ouverte
				$Xvars;										// Déclaré global pour tous les traitements XML

  $erreur = 0;
  $erreurs = array ();
  $messages = array ();
   
// Ajuster l'espace mémoire
  ini_set ('memory_limit','512M');
   
   
/*################################################  Protection des entrées #####
"db"								* REQUEST - Filtré numérique Comparé à valeurs connues
"langue"							GET -	Comparé à valeurs connues
														Attention le champ 'langue' est transmis en POST par modifier_table
----------------------------------------------------------------------------- */
	
  if (isset ($_REQUEST['db']))	{
		if (is_numeric($_REQUEST['db']) && ($_REQUEST['db'] < count ($Bases))) 
			$R_db = $_REQUEST['db'];
	else DIE ("*** Paramètre 'db' faux ! ***");  
	}

  if (@$_GET['langue']) {
		if (in_array($_GET['langue'], $langues)) {		// langues : variable globale
			$R_langue = $_GET['langue'];
		} else DIE ("*** Paramètre 'langue' faux ! ***");
  } else $R_langue ='';
	
########################## Ouverture de session et  OUVERTURE DE LA PAGE HTML ###

  session_start ();

  $local_titre = isset ($_SESSION['db']) ? $Bases[$_SESSION['db']]['nompublic'] : '';		// Pour titre html									
  if (!isset ($jscript)) $jscript = '';				// pour chargement fonctions javascript définies dans un module
  
	// Dans le cas de l'affichage d'un transfert XML, il faut retarder l'ouverture de la page HTML
	// et supprimer cette ouverture en cas d'opération ajax
	if (!isset ($f_non_head_html)) require_once ('inc_tete.php');

######################################################### Variables de session ###	
	// Controle du mode session/cookies
	if ( $_SESSION == NULL && !isset ($R_db)) {
		// pas de session mémorisée , pas de base spécifiée dans l'appel
		echo <<<EOD
<script language='javascript'>
	alert('Votre session a expiré - Vous devez vous reconnecter');		
	retour_DBAconit();
</script>
EOD;
			exit;	// (non exécuté) >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
	} 
   
	debug (0x10, 'SESSION', $_SESSION);
	debug (0x20, '$_REQUEST', $_REQUEST);
 
#====================== Peut-on ouvrir une nouvelle session sans conflit ?
	// Ne concerne pas les utilisateurs, qui n'interviennent pas en modification
	if ((isset ($_SESSION['droits']) AND in_array ('mod_objet', $_SESSION['droits']))) {
	
		if (isset ($_SESSION['db']) AND isset ($R_db) AND $_SESSION['db'] != $R_db) {
			echo <<<EOD
<script language='javascript'>
	alert('Vous ne pouvez pas ouvrir simultanément 2 sessions sur 2 bases différentes. Fermez la session précédente');
	retour_DBAconit();
</script>
EOD;
			exit;	// (non exécuté) >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
		}
	}

#====================== Chargement langue, droits...
	// Si une langue est demandée en paramètre, l'enregistrer
	if ($R_langue) $_SESSION['langue'] = $R_langue;
	// Fixer la langue
  if (!@$_SESSION['langue'])  $_SESSION['langue'] = 'FR';	// FR langue par défaut
  // Si nécessaire charger la table des messages anglais
   if ($_SESSION['langue'] == 'EN') require_once ('EN_formats.php');

     
// Créer le tableau  droits d'accès élémentaires à partir de la session existante
// sinon, il est fixé par défaut aux droits visiteur (cas de l'entrée directe sur "consulter"),
// le tableau correct sera créé lors de l'identification
	if (isset ($_SESSION['droits']))	$droits = $_SESSION['droits'];
	else {
		$_SESSION['statut'] ='visiteur';			
		Profil ('R');		// Fixer les paramètres d'affichage à partir du profil par défaut
		$droits = $tab_droits ["visiteur"];
	}
	
################## Choix de la base et connexion #####################################

#====================== S'il n'y a qu'une base, elle est automatiquement sélectionnée
	if (1 == count ($Bases)) $_SESSION['db'] = 0;

#==================== Si une base est demandée en paramètre, l'enregistrer
	if (isset ($R_db)) $_SESSION['db'] = $R_db;

#==================== Si une base est sélectionnée, connecter MySQL et ouvrir la base
	if (isset ($_SESSION['db'])) {			// Inscrit en session, ouvrir

		$db = $_SESSION['db'];
		$dbase = $Bases[$db]; 
		$t_alias = array ();

		if ($dblink = mysqli_connect ($dbase['serveur'], $dbase['user'], $dbase['passe'], $dbase['nom'], key_exists('port', $dbase) ? $dbase['port'] : 3306)) {
			debugMsg ("mySQL session ouverte - Base de données " . $dbase['nom'] . " ouverte");
			// Spécifier le codage de la base !		
			mysqli_set_charset($dblink, "utf8");
			
			// si version MySQL 8 : suprimer du mode SQL les directives : NO_ZERO_IN_DATE,NO_ZERO_DATE
			if (mysqli_get_server_version($dblink) >= 80000) requete ("SET SESSION 
				sql_mode = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';");
			
		} else {
			erreurMsg ("Erreur lors de l'ouverture - %0 ",  mysqli_connect_error ());
		}

### Si OK
		if (!$erreur) {
		
			// Particularités de numération, fixées dans la déclaration de la base dans config.php
					//-- La numérotation commune à tous les établissements peut être activée 
					//		par une ligne < 'num_commune' => TRUE > 			
					//-- Nombre de caractères du numéro d'inventaire : fixé par < $nbre_cars_nrinv = ? >
											
			// Lecture de la table des paramètres
			if ($result = requete ('SELECT * FROM Parametres')) {
				while ($ligne = mysqli_fetch_assoc ($result)) {
					//------  alias : remplir la table
					if (strtolower($ligne['parametre']) == 'alias') {
						list ($st_fr, $st_en) = explode ('#', $ligne['valeur']);
						$t_alias[$st_fr] = $st_en;
					
					//------  pondérations : mettre en tableau et en session
					} elseif (strtolower($ligne['parametre']) == 'poids_tmarques') {
						$_SESSION['km'] = explode (',', $ligne['valeur']);
					} elseif (strtolower($ligne['parametre']) == 'poids_trecols') {
						$_SESSION['kr'] = explode (',', $ligne['valeur']);
						
 					//------ et transférer dans $dbase les autres éléments non vides
					}else
					if ($ligne['parametre'] != '') $dbase[$ligne['parametre']] = $ligne['valeur'];
				}
				$erreur = FALSE;	    
			}

		// Contrôles sur les valeurs chargées...
			// taille de bloc par défaut
			if (!isset ($dbase['taille_bloc'])) $dbase['taille_bloc'] = 100;
			// établissement par défaut
			if (!isset ($dbase['etablissement_defaut'])) $dbase['etablissement_defaut'] = 1;
			// Si l'établissement courant n'est pas défini prendre l'établissement par défaut
			if (!isset ($_SESSION['idetab_c']))	$_SESSION['idetab_c'] = $dbase['etablissement_defaut'];

		
			// Si mode récolement, les pondérations ne peuvent pas rester non-définies
			if (@$dbase['mode_recolement']) {
				if (!isset ($_SESSION['km'])) $_SESSION['km'] = array (0,0,0,0,0,0,0,0,0,0);									// 10 nbre connu
				if (!isset ($_SESSION['kr'])) $_SESSION['kr'] = array (0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0); // 20... estimé
			}
			
			//  et vérifier la compatibilité logiciel et BdD
			$Base = array_key_exists ('Base', $dbase) ? $dbase['Base'] : '???';
			$n = preg_match("#^[^0-9]*([0-9]*)#", $DBAconit, $vlog);
			$n = preg_match("#^[^0-9]*([0-9]*)#", $Base, $vbase);
			if ($vlog[1] != $vbase[1]) {
				$text = Tr ("Le logiciel $DBAconit n'est pas compatible avec la Base $Base", 
								'$DBAconit software is not compatible with Base $Base');
				echo <<<EOD
<script language='javascript'>
	alert("$text");
	retour_DBAconit();
	</script>
EOD;
				exit;	//  (non exécuté) >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
			}

### Sortie si erreur  
		} else {
			Debut ();
			Fin ();
			exit;
		}
#=======================	S'il n'y a pas de base sélectionnée... forcer l'appel d'index.php ?
	} else {
		debug (0x80, 'INIT_INC anormal');
		$_SERVER['PHP_SELF'] = 'index.php';
		include ($_SERVER['PHP_SELF']);
	}

#==========> Retour au module appelant ======>
?>
