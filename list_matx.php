<?php
# v24					190210	PhD		Création à partir de list_etats
# v25					190615	PhD		Supprimé inst debug 8
# v25.2				190906	PhD		Le terme SQL de recherche est maintenant codé
# v25.8				200411	PhD		Ajout custom_css
###

/* Protection des entrées -------------------------------------------------------
'action'				- POST - uniquement testé switch
------------------------------------------------------------------------------ */
############################################################ XML_list_mat ###
function XML_list_mat ($loop, $attr, $Xaction) {

	if ($loop === null) return;		// tag de fin
	global $Xvars;
	static $SQLresult;

	// Si tag de début, appeler la liste des matériaux
	if ($loop === 0) {
		
		$SQLresult = requete ("SELECT *	FROM Materiaux ORDER BY materiau");
	}
			
	//  Appel de l'état courant
	while ($ligne = mysqli_fetch_assoc ($SQLresult)) { 
		$Xvars['ligne'] = $ligne;

		// Chercher le nombre de fiches concernées	
		$idmateriau = $ligne['idmateriau'];

		$SQLresult2 = requete ("SELECT idcollection FROM Col_Mate
      WHERE Col_Mate.idmateriau = $idmateriau");

		$Xvars['nbr_fiches'] = mysqli_num_rows($SQLresult2);
	
		// Préparer les paramètres pour l'URL de recherche
		$Xvars['quest'] = Phd_encode("Col_Mate.idmateriau = $idmateriau", session_id ());
	
		// Alternance des couleurs de ligne
		$Xvars['class'] =  ($loop % 2) ? 'collig1' : 'collig2';		
	
		return ($ligne) ? 'ACT,LOOP' : 'EXIT' ;
	}
} 

########################################################################################################################
########################################################################################################################

$custom_css = "list_matx.css";
require_once ('init.inc.php');

## Traitement des entrées :
###########################
	
# Initialisations ##############################

Debut ();

# AFFICHAGE de l'écran principal 
###############################################

// Passage des paramètres principaux
global $Xvars;

#======================= Afficher partir du modèle XML

	$liste_xml = Xopen ('./XML_modeles/list_matx.xml') ;
	Xpose ($liste_xml);

#################################### Fin de traitement
Fin(); 
?>