<?php
# v18				170805	PhD		Création
# v19.1			170924	PhD		Changé 'date1' et 2 à cause des limitations sur OVH
# v25				190615	PhD		Supprimé inst debug 8
# v25.2			190906	PhD		Le terme SQL de recherche est maintenant codé
# v25.8			200411	PhD		Ajout custom_css
###

/* Protection des entrées -------------------------------------------------------
'action'				- POST - uniquement testé switch
'date1'					- POST - filtré NormIN
'date2'					- POST - filtré NormIN
------------------------------------------------------------------------------ */
############################################################ XML_list_mouv ###
function XML_list_mouv ($loop, $attr, $Xaction) {

	if ($loop === null) return;		// tag de fin
	global $Xvars;
	static $SQLresult_attach;

	// Si tag de début, appeler la liste des mouvements
	if ($loop === 0) {
		$date1 = $Xvars['date1'];
		$date2 = $Xvars['date2'];
		
		$SQLresult_attach = requete (
	 		"SELECT DISTINCT Mouvements.idmouvement, datemouv, typemouvt, orgmouv, destmouv, comgenmouv  
	 		FROM Mouvements 
	 		LEFT JOIN Col_Mouv ON Mouvements.idmouvement=Col_Mouv.idmouvement
	 		LEFT JOIN Typemouvts On Typemouvts.idtypemouvt=Mouvements.idtypemouvt
	 		WHERE ((datemouv >= '$date1') AND (datemouv <= '$date2')) OR datemouv='00-00-00'
	 		ORDER BY datemouv");
			// Noter l'ajout permettant d'afficher les dates 00 (incorrectes !)
 	}
			
	//  Appel du mouvement courant
	while ($ligne = mysqli_fetch_assoc ($SQLresult_attach)) { 
		$Xvars['ligne'] = $ligne;
	
		// Chercher le nombre de fiches concernées	
		$idmouvement = $ligne['idmouvement'];
		$SQLresult2 = requete ("SELECT idcollection FROM Col_Mouv WHERE idmouvement = $idmouvement");
		$Xvars['nbr_fiches'] = mysqli_num_rows($SQLresult2);
	
		// Préparer les paramètres pour l'URL de recherche
		$Xvars['quest'] = Phd_encode("Col_Mouv.idmouvement=$idmouvement", session_id ());
	
		// Alternance des couleurs de ligne
		$Xvars['class'] =  ($loop % 2) ? 'collig1' : 'collig2';		
	
		return ($ligne) ? 'ACT,LOOP' : 'EXIT' ;
	}
} 

########################################################################################################################
########################################################################################################################

$custom_css = "list_mouvts.css";
require_once ('init.inc.php');

## Traitement des entrées :
###########################
	if (isset($_POST['action'])) {
		$date1 = NormIn ('date1');
		$date2 = NormIn ('date2');
	} else {
		$date1 = '1970-01-01';
		$date2 = '2037-12-31';		// sur OVH, 'strftime' refude de traiter les dates au delà...
	}
			
	
# Initialisations ##############################

Debut ();

# AFFICHAGE de l'écran principal 
###############################################

// Passage des paramètres principaux
global $Xvars;
$Xvars['date1'] =$date1;
$Xvars['date2'] =$date2;

#======================= Afficher partir du modèle XML

	$liste_xml = Xopen ('./XML_modeles/list_mouvts.xml') ;
	Xpose ($liste_xml);

#################################### Fin de traitement
Fin(); 
?>