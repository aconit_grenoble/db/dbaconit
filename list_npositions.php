<?php
# v21.2				180212	PhD		Création
# v25					190615	PhD		Supprimé inst debug 8
# v25.2				190906	PhD		Le terme SQL de recherche est maintenant codé
# v25.4				191203	PhD		Affichage de la première ligne (position indéfinie)
# v25.5				200107	PhD		Ajout totaux par colonne et lien sur chaque nombre
# v25.8				200411	PhD		Ajout custom_css
###


############################################################ XML_list_npositions ###
function XML_list_npositions ($loop, $attr, $Xaction) {

	if ($loop === null) return;		// tag de fin
	global $Xvars;
	static $SQLresult_npositions;

	// Si tag de début, appeler la liste des positions
	if ($loop === 0) {		
		$SQLresult_npositions = requete (
	 		"SELECT * FROM Tnpositions ORDER BY tnposition");
	 		
		// Préparer les totalisateurs
		$Xvars['tot_fiches'] = $Xvars['tot_machines'] = $Xvars['tot_documents'] = $Xvars['tot_logiciels'] = 0;	
		$Xvars['tot_nonver'] = $Xvars['tot_ver'] = 0;
 	}
			
	//  Appel de chaque position (valide) courant
	while ($ligne = mysqli_fetch_assoc ($SQLresult_npositions)) { 
		$Xvars['ligne'] = $ligne;
		$idtnposition = $ligne['idtnposition'];
		$Xvars['idtnposition'] = $idtnposition; // Conserver l'index position pour le comptage par critère dominant
		
		// Chercher le nombre global de fiches concernées	
		$SQLresult2 = requete ("SELECT idcollection FROM Collections WHERE idtnposition = $idtnposition");
		$Xvars['nbr_fiches'] = mysqli_num_rows($SQLresult2);
		if ($idtnposition != 1) $Xvars['tot_fiches'] += $Xvars['nbr_fiches'];

		// Chercher le nombre de machines	
		$SQLresult2 = requete ("SELECT idcollection FROM Collections WHERE idtnposition = $idtnposition
									and (Collections.idmachine!=0)");
		$Xvars['nbr_machines'] = mysqli_num_rows($SQLresult2);
		if ($idtnposition != 1) $Xvars['tot_machines'] += $Xvars['nbr_machines'];
				// URL de recherche
				$Xvars['quest_m'] = 
					Phd_encode("Collections.idtnposition=$idtnposition AND (Collections.idmachine!=0)", session_id ());


		// Chercher le nombre de documents
		$SQLresult2 = requete ("SELECT idcollection FROM Collections WHERE idtnposition = $idtnposition
									and (Collections.iddocument!=0)");
		$Xvars['nbr_documents'] = mysqli_num_rows($SQLresult2);
		if ($idtnposition != 1) $Xvars['tot_documents'] += $Xvars['nbr_documents'];
				// URL de recherche
				$Xvars['quest_d'] = 
					Phd_encode("Collections.idtnposition=$idtnposition AND (Collections.iddocument!=0)", session_id ());

		// Chercher le nombre de logiciels
		$SQLresult2 = requete ("SELECT idcollection FROM Collections WHERE idtnposition = $idtnposition
									and (Collections.idlogiciel!=0)");
		$Xvars['nbr_logiciels'] = mysqli_num_rows($SQLresult2);
		if ($idtnposition != 1) $Xvars['tot_logiciels'] += $Xvars['nbr_logiciels'];
				// URL de recherche
				$Xvars['quest_l'] = 
					Phd_encode("Collections.idtnposition=$idtnposition AND (Collections.idlogiciel!=0)", session_id ());

		// Chercher le nombre de fiches non verrouillées
		$SQLresult2 = requete ("SELECT idcollection FROM Collections WHERE idtnposition = $idtnposition AND m10=0");
		$Xvars['nbr_nonver'] = mysqli_num_rows($SQLresult2);
		if ($idtnposition != 1) $Xvars['tot_nonver'] += $Xvars['nbr_nonver'];
				// URL de recherche
				$Xvars['quest_nv'] = Phd_encode("Collections.idtnposition = $idtnposition AND m10=0", session_id ());

		// Chercher le nombre de fiches verrouillées
		$SQLresult2 = requete ("SELECT idcollection FROM Collections WHERE idtnposition = $idtnposition AND m10!=0");
		$Xvars['nbr_ver'] = mysqli_num_rows($SQLresult2);
		if ($idtnposition != 1) $Xvars['tot_ver'] += $Xvars['nbr_ver'];
				// URL de recherche
				$Xvars['quest_v'] = Phd_encode("Collections.idtnposition = $idtnposition AND m10!=0", session_id ());

		// Préparer les paramètres pour l'URL de recherche
		$Xvars['quest'] = Phd_encode("Collections.idtnposition=$idtnposition", session_id ());

		if ($idtnposition != 1) $Xvars['class'] =  ($loop % 2) ? 'collig1' : 'collig2';		
		else $Xvars['class'] = 'colfond';

		return ($ligne) ? 'ACT,LOOP' : 'EXIT' ;
	}
} 

############################################################ XML_tdnbr ###
function XML_tdnbr ($loop, $attr, $Xaction) {

	if ($loop === null) return;		// tag de fin
	global $Xvars;
	
	$idtnposition = $Xvars['idtnposition'];
	$nbrcol = $Xvars['nbrcol'];
	$Xvars['idtrecol'] = $idtrecol = $loop+1;
	
	$SQLresult3 = requete ("SELECT idcollection FROM Collections WHERE idtnposition = $idtnposition
									and idtrecol = $idtrecol");
	$Xvars['nbr'] = mysqli_num_rows($SQLresult3);
	// URL de recherche
	$Xvars['quest_r'] = 
					Phd_encode("Collections.idtnposition=$idtnposition AND Collections.idtrecol = $idtrecol", session_id ());

	if ($idtnposition !=1) $Xvars['tot'][$idtrecol]	+= $Xvars['nbr'];

	// Lire et compter l'un des critères dominants pour cette position
		return ($idtrecol < $nbrcol) ? 'ACT,LOOP' : 'ACT' ;	
 }
 
############################################################ XML_threcol ###
function XML_threcol ($loop, $attr, $Xaction) {

	if ($loop === null) return;		// tag de fin
	global $Xvars;
	static $SQLresult_threcol;

	// Si tag de début, appeler la liste des noms des critères dominants
	if ($loop === 0) {
		$SQLresult_threcol = requete ("SELECT * FROM Trecols ORDER BY trecol"); 		
 		
		// Noter le nombre de colonnes pour l'affichage td_recol
		$Xvars['nbrcol'] = mysqli_num_rows ($SQLresult_threcol);
	}
 
 	//  Appel du libellé courant, et initialisation de la table des totaux
	while ($ligne = mysqli_fetch_assoc ($SQLresult_threcol)) { 
		$Xvars['trecol'] = $ligne['trecol'];
		$Xvars['tot'][$ligne['idtrecol']] = 0;

		return ($ligne) ? 'ACT,LOOP' : 'EXIT' ;
	}	
 }
 
########################################################################################################################
########################################################################################################################

$custom_css = "list_npositions.css";
require_once ('init.inc.php');

# Initialisations ##############################

Debut ();

# AFFICHAGE de l'écran principal 
###############################################

// Passage des paramètres principaux
global $Xvars;
$Xvars['tot'] = array ();	
#======================= Afficher partir du modèle XML

	$liste_xml = Xopen ('./XML_modeles/list_npositions.xml') ;
	Xpose ($liste_xml);

#################################### Fin de traitement
Fin(); 
?>