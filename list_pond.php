<?php
# v25.4			191203	PhD		Création
# v25.5			191228	PhD		Ajout du traitement des objets sélectionnés pour appel à Rechecher,
#														reprise des pondérations propres à la session
# v25.8			200411	PhD		Ajout custom_css
# v26			221209	PhD		Remplacé 'res1c' par 'tampond'
###


/* Protection des entrées -------------------------------------------------------
'action'					- POST submit - uniquement testé valeur connue
'idcollections'		- POST  - Filtré numérique
--- affichage par bloc :
'affich'			- POST  - Filtré NormIN  - uniquement testé switch
'borne1'			- POST  - Filtré numérique
'pas'					- POST  - Filtré numérique
'pas_ar'			- POST  - Filtré NormIN  - uniquement testé switch
'pas_av'			- POST  - Filtré NormIN  - uniquement testé switch
------------------------------------------------------------------------------ */

#====================================================================================== XML_itemtri ===
function XML_itemtri ($loop, $attr, $Xaction) {
	if ($loop === null) return;		//  Détection de la balise finale >>>>>>>>>>>>>>>>>>>>>>>>>>
	
	global $Xvars, $tablid, $tablvp, $tabtyp;
	
	$borne1 = $Xvars['borne1'];
	$pas = $Xvars['pas'];
	$length = $Xvars['total_objets'];
	
	//  Appel du nom courant	
	$idcollection = $tablid[$loop];
	if ($loop+1 >= $borne1) {    // Traitement à partir de la borne basse
		$Xvars['idcollection'] = $idcollection = $tablid[$loop];
		$Xvars['vp'] = $tablvp[$loop];
		$Xvars['type'] = $tabtyp[$loop];
	
		// Composer le numéro d'inventaire complet
		$Xvars['nrinventaire'] = Nrinventaire ($idcollection);

		$Xvars['db'] = $_SESSION['db'];
		
		// Désignation sans affichage du type d'objet
		$Xvars['design_titre'] = Design_titre ($idcollection, FALSE);  

	
		// Alternance des couleurs de ligne
		$Xvars['class'] =  ($loop % 2) ? 'collig1' : 'collig2';
	} elseif ($loop+1<$borne1) return 'LOOP';	// avant la borne basse, boucle sans affichage

	return (($loop+1<$length) AND ($loop+1<$borne1+$pas)) ? 'ACT,LOOP' : 'ACT';	// Affichage -> borne haute, puis sortie
} 

############################################################################
################################################################# TRAITEMENT

$custom_css = "list_pond.css";
require_once ('init.inc.php');
require_once ('aff_recol.inc.php');

Debut();  

### Traitement des entrées
####################################
$action = @$_POST['action'];
//$idcollections = NormIN ('idcollections');
if (isset ($_POST['idcollections'])) {
	$idcollections = array_values(array_unique ($_POST['idcollections']));
}

	$f_bloc = TRUE;
	$affich = NormIN ('affich');
	$borne1 = (is_numeric (@$_POST['borne1'])) ? $_POST['borne1'] : 1;
	$pas = (is_numeric (@$_POST['pas'])) ? $_POST['pas'] : $dbase['taille_bloc'];
	$pas_ar = NormIN ('pas_ar');
	$pas_av = NormIN ('pas_av');
	
### Traitement des actions
####################################
	switch ($action) {
	
		// Appel d'une sélection de fiches pour réaffichage en liste récolement/désherbage
		// Le champ Collections.tampond de chaque fiche sert de tampon pour noter les idrapporteurs 
		// des rapporteurs qui ont sélectionné cette fiche, sous la forme '#nn,'
		case 'recol' : 
			// Y a-t-il déjà des fiches sélectionnées pour ce rapporteur
			$rap = '#'.$_SESSION['idrapporteur'].',';
			$res = requete ("SELECT idcollection, tampond FROM Collections WHERE tampond LIKE '%$rap%'");
													
			// oui : effacer les marques
			while ($ligne = mysqli_fetch_assoc ($res)) {
				$idcol = $ligne['idcollection'];
				$str = str_replace ($rap, '', $ligne['tampond']);
				requete ("UPDATE Collections SET tampond='$str' WHERE idcollection=$idcol");
			}
			
			// puis effectuer un marquage des nouvelles fiches
			foreach ($idcollections as $idcollection) {
				$result = requete ("SELECT tampond FROM Collections WHERE idcollection=$idcollection");
				$ligne = mysqli_fetch_assoc ($result);
				$str = $ligne['tampond'].$rap;
					requete ("UPDATE Collections SET tampond='$str' WHERE idcollection=$idcollection");
			}	
			
			// et appeler Rechercher avec la bonne requête
			$SQL_include = "tampond LIKE '%$rap%'";
			$f_include = TRUE;
			include ('rechercher.php');							// >>>>>>>>>>>>>>>>>> Branchement vers "Rechercher"
																							// >>>>>>>> EXIT >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
			break;	// pour la syntaxe
			
#------------------------------------------------
		// Si affichage par bloc : calcul des bornes selon formulaire ###
		case 'pas_ar' :				// Pas arrière
			$borne1 -= $pas;
			if ($borne1 <1) $borne1 =1;
			break;
		case 'pas_av' :				// Pas avant
			$borne1 += $pas;
			break;
		case 'reafficher' :			// Réafficher
			break;			// rien à faire
	}

### Puis afficher la liste
####################################

# Sélectionner les éléments nécessaires pour le tri
$SQLresult = requete ( "SELECT idcollection, type, idtrecol, m1, m2, m3, m4, m5, m6, m7, m8,m9,m10 FROM Collections ");
$Xvars['SQLresult'] = $SQLresult;

$Xvars['total_objets'] = mysqli_num_rows ($SQLresult);

# Établir le tableau des objets pondérés
$tablid = array ();
$n = 0;
while ($ligne = mysqli_fetch_assoc ($SQLresult)) { 
		
	// prendre les pondérations
	$pond = $_SESSION['km'];
	$sum = $_SESSION['kr'][$ligne['idtrecol']-1];
	for ($i = 1; $i <= 10; $i++) {
		$sum += $ligne['m'.$i] * $pond[$i-1];
	}
	$tablid[$n] = $ligne['idcollection'];
	$tablvp[$n] = $sum;
	$tabtyp[$n] = $ligne['type'];
	$n++;
}
# et le trier
array_multisort($tablvp, SORT_NUMERIC, SORT_DESC,
								$tablid, SORT_NUMERIC, SORT_ASC,
								$tabtyp);
$Xvars['tablid'] = $tablid;
$Xvars['tablvp'] = $tablid;
$Xvars['tabtyp'] = $tabtyp;

#----------------Préparer les éléments pour affichage par bloc
$Xvars['server'] = $_SERVER['PHP_SELF'].'?'.$_SERVER['QUERY_STRING'];	// adresse retour formulaire

$Xvars['f_bloc'] = $f_bloc;
$Xvars['borne1'] = $borne1;
$Xvars['pas'] = $pas;

$Xvars['action'] = $action;

#======================= Afficher partir du modèle XML
$liste_xml = Xopen ('./XML_modeles/list_pond.xml') ;
Xpose ($liste_xml);

#################################### Fin de traitement
Fin ();
?>