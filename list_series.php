<?php
# v24					190223	PhD		Création à partir de list_etabs
# v25					190615	PhD		Supprimé inst debug 8
# v25.2				190906	PhD		Le terme SQL de recherche est maintenant codé
# v25.3.01		191107	PhD		Correction requete select idcollection, correction quest
# v25.8				200411	PhD		Ajout custom_css
###

/* Protection des entrées -------------------------------------------------------
'action'				- POST - uniquement testé switch
------------------------------------------------------------------------------ */
############################################################ XML_list_serie ###
function XML_list_serie ($loop, $attr, $Xaction) {

	if ($loop === null) return;		// tag de fin
	global $Xvars;
	static $SQLresult_bat;

	// Si tag de début, appeler la liste desséries
	if ($loop === 0) {
		
		$SQLresult_bat = requete (
			"SELECT *	FROM Series 
			LEFT JOIN Etablissements ON Series.idetablissement = Etablissements.idetablissement
			ORDER BY etablissement, codeserie");
 	}
			
	//  Appel de l'élément courant
	while ($ligne = mysqli_fetch_assoc ($SQLresult_bat)) { 
		$Xvars['ligne'] = $ligne;

		// Chercher le nombre de fiches concernées	
		$codeserie = $ligne['codeserie'];
		$idetablissement = $ligne['idetablissement'];

		$SQLresult2 = requete ("SELECT idcollection FROM Collections 
									WHERE idetablissement=$idetablissement AND nrinv LIKE '".$codeserie."%'");
		$Xvars['nbr_fiches'] = mysqli_num_rows($SQLresult2);
	
		// Préparer les paramètres pour l'URL de recherche
		$Xvars['quest'] = Phd_encode
						("Collections.idetablissement=$idetablissement AND nrinv LIKE '".$codeserie."%'", session_id ());
	
		// Alternance des couleurs de ligne
		$Xvars['class'] =  ($loop % 2) ? 'collig1' : 'collig2';		
	
		return ($ligne) ? 'ACT,LOOP' : 'EXIT' ;
	}
} 

########################################################################################################################
########################################################################################################################

$custom_css = "list_series.css";
require_once ('init.inc.php');

## Traitement des entrées :
###########################
	
# Initialisations ##############################

Debut ();

# AFFICHAGE de l'écran principal 
###############################################

// Passage des paramètres principaux
global $Xvars;

#======================= Afficher partir du modèle XML

	$liste_xml = Xopen ('./XML_modeles/list_series.xml') ;
	Xpose ($liste_xml);

#################################### Fin de traitement
Fin(); 
?>