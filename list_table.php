<?php
# v25.7				200319	PhD		Création
# v25.7.01		200402	PhD		Ajout tablemere.nomid
# v25.8				200411	PhD		Ajout custom_css
###

/* Protection des entrées -------------------------------------------------------
'table'				- POST - Comparé à liste de valeurs
------------------------------------------------------------------------------ */
############################################################ XML_list ###
function XML_list_table ($loop, $attr, $Xaction) {

	if ($loop === null) return;		// tag de fin
	global $Xvars;
	static $SQLresult;

	// Si tag de début, appeler la liste des éléments
	if ($loop === 0) {
		$table = $Xvars['table'];		
		$SQLresult = requete ("SELECT *	FROM $table");
	}
			
	//  Appel de l'état courant
	while ($ligne = mysqli_fetch_assoc ($SQLresult)) { 
		$Xvars['ligne'] = $ligne;

		// Chercher le nombre de fiches concernées	
		$nomid = $Xvars['nomid'];
		$id = $ligne[$nomid]; 

		$tablemere = $Xvars['tablemere'];
		$SQLresult2 = requete ("SELECT $nomid FROM $tablemere WHERE $nomid = $id");
		$Xvars['nbr_fiches'] = mysqli_num_rows($SQLresult2);
	
		// Préparer les paramètres pour l'URL de recherche
		$Xvars['quest'] = Phd_encode("$tablemere."."$nomid = $id", session_id ());
	
		// Alternance des couleurs de ligne
		$Xvars['class'] =  ($loop % 2) ? 'collig1' : 'collig2';		
	
		return ($ligne) ? 'ACT,LOOP' : 'EXIT' ;
	}
} 

########################################################################################################################
########################################################################################################################

$custom_css = "list_table.css";
require_once ('init.inc.php');
Debut ();

// Table des arguments pour chaque cas
$Ttitre=0; $Tlibid=1; $Tlibchamp=2; $Ttablemere=3;

$TT = array (
	'Acquisitions' => array ('modes d\'acquistion', '', 'Acquisition', 'Collections'),
	'Codeprogs' => array ('natures de programme', '', 'Code programme', 'Logiciels'),
	'Designations' => array ('désignations', '', 'Désignation', 'Machines'),
	'Domaines' => array ('domaines/sous-domaines', '', 'Domaine', 'Collections'),
	'Etatfiches' => array ('états de validation', 'Code d\'état', 'État de la fiche', 'Collections'),
	'Etats' => array ('états des objets', '', 'État', 'Collections'),
	'Langprogs' => array ('langages de programmation', '', 'Langage', 'Logiciels'),
	'Langues' => array ('langues de rédaction', '', 'Langue', 'Documents'),
	'Periodes' => array ('périodes de production', '', 'Période', 'Collections'),
	'Productions' => array ('modes de production', '', 'Production', 'Collections'),
	'Services' => array ('états opérationnel', '', 'Service', 'Machines'),
	'Supports' => array ('types de supports des logiciels', '', 'Support', 'Logiciels'),
	'Systemes' => array ('systèmes d\'exploitation des logiciels', '', 'Système', 'Logiciels'),
	'Typedocs' => array ('type de documents', '', 'Type de document', 'Documents'),
	'Typeillustrs' => array ('types d\'illustrations', '', 'Type d\'illustration', 'Documents')
);

$TT_EN = array (
	'Acquisitions' => array ('methods of acquisition', '', 'Acquisition', 'Collections'),
	'Codeprogs' => array ('program codes', '', 'Program code', 'Logiciels'),
	'Designations' => array ('désignations', '', 'Designation', 'Machines'),
	'Domaines' => array ('Domain/subdomain', '', 'Domain', 'Collections'),
	'Etatfiches' => array ('validation status', 'Status code', 'Record status', 'Collections'),
	'Etats' => array ('condition of the artifact', '', 'Condition', 'Collections'),
	'Langprogs' => array ('programming languages', '', 'Programming language', 'Logiciels'),
	'Langues' => array ('editing languages', '', 'Language', 'Documents'),
	'Periodes' => array ('production periodes', '', 'Periode', 'Collections'),
	'Productions' => array ('methods of production', '', 'Production', 'Collections'),
	'Services' => array ('operational status', '', 'Status', 'Machines'),
	'Supports' => array ('software medias', '', 'Media', 'Logiciels'),
	'Systemes' => array ('operating systems', '', 'System', 'Logiciels'),
	'Typedocs' => array ('types of document', '', 'Type', 'Documents'),
	'Typeillustrs' => array ('types of illustrations', '', 'Illustrations', 'Documents')
);

### Traitement des entrées ###########################
$table = NormIN ('table', 'R');
if (!array_search ($table, $T_ordre))  DIE ("Nom de table non admis !");
	
# Initialisations ##############################


# AFFICHAGE de l'écran principal 
###############################################

// Passage des paramètres
global $Xvars;
$Xvars['table'] = $table;
$Xvars['nomid'] = $nomid = Nomid ($table);

$Xvars['nomchamp'] = substr ($nomid,2);   // supprimer les 2 cars 'id'
if ($_SESSION['langue'] != 'FR') {				// Si anglais, vérifier que la colonne existe
	$nom_en = substr ($nomid,2).'_en';
	if (in_array ($nom_en, colonnes ($table))) $Xvars['nomchamp'] = $nom_en;
}

// Prendre la ligne correspondant à la table demandée
if ($_SESSION['langue'] == 'FR') $T = $TT[$table];
else $T= $TT_EN[$table];

$Xvars['titre'] = $T[$Ttitre];
$Xvars['libid'] = $T[$Tlibid];
$Xvars['libchamp'] = $T[$Tlibchamp];
$Xvars['tablemere'] = $T[$Ttablemere];

#======================= Afficher partir du modèle XML

	$liste_xml = Xopen ('./XML_modeles/list_table.xml') ;
	Xpose ($liste_xml);

#################################### Fin de traitement
Fin(); 
?>