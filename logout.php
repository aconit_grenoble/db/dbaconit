<?php
# v19			170903	PhD		Création
# v21			171122	PhD		Utilisation de la fonction JS retour_DBAconit
#
#####################################################################     
# Appel pour se désinscrire de la session en cours
# Il faut passer par les initialisations pour pouvoir accéder à la base et libérer le verrou modifications
###

/* Aucune entrée -------------------------------------------------------
------------------------------------------------------------------------------ */

require_once ('init.inc.php');

Deverrouiller ();
session_unset();
session_destroy();
session_write_close();
//setcookie(session_name(),'',0,'/');

# Calculer l'URL du serveur DBAconit et rappeler le programme
echo <<<EOD
	<script language='javascript'>	
		retour_DBAconit(2);
	</script>
EOD;
?>