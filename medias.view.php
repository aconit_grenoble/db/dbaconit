<!DOCTYPE html >
<html lang="fr">

<!-- 
#	Affichage des différents médias dans une fenetre séparée
###
# v12.0			140214	MSA		Création pour nouveau traitement des Médias
# v14.2			151214	PhD		Suppression echo dba_html
# v15		 		160224	PhD		Reprise des affichages à partir du travail dans dbgalerie, décoder légende
# v25.7.01	200403	PhD		Supprimé onClick="window.close ()" sur MP4 et PDF	
# v25.9			200724	PhD		Ajouté traitement des images disques IMG
# v26.2.01	230430	PhD		Corrigé traitement 'fname'
###
#
# Ce module assure l'affichage de tous les médias sous forme d'une page HTML dans une fenêtre volante.
# Il est appelé :
# - par la fonction 'Prepar_photo.php' (globalfcts) à partir de tous les écrans affichant les vignettes des medias
# - par la fonction javascript 'traite_view' dans le module 'mod_medias.php' pour les affichages des medias selectionnés#
#
# Dans le cas particulier des fichiers IMG, la enêtre propose un télécjhrgement
###
-->

<head>
	<meta charset="utf-8" />
	<title> MEDIA - ACONIT </title>
	<script>
		var size = 0;
		var diff=0;
		var diff2=0;

		function myTimer() {
			window.resizeTo(document.getElementById('div_id2').offsetWidth+25,
			document.getElementById('div_id').offsetHeight+50);
		}
		
		var myVar=setTimeout(function(){myTimer()},300);		// temps nécessaire pour l'affichage des vidéos...
	</script>
</head>

<?php 
####################################	Controle des données entrées
	//print_r ($_REQUEST);
	$in = $_REQUEST['media'];
		$media = htmlspecialchars (trim ($in), ENT_QUOTES, 'utf-8');
		
	$in = @$_REQUEST['cotemedia'];
		$fname = htmlspecialchars (trim ($in), ENT_QUOTES, 'utf-8');
		
	$in = @$_REQUEST['legende'];		
		$legende = base64_decode ($in);
		
	$in = @$_REQUEST['format'];
		$format = htmlspecialchars (trim ($in), ENT_QUOTES, 'utf-8');
		if($format == 'mov') $format = "quicktime";

	
########################################################################## Format jpg ou png ###
if($format=='jpg' OR $format=='png'){
	echo <<<EOD
<body style="padding:0; margin:0; background-color:grey; " onClick="window.close ()">
	<div id="div_id" style="margin:10px;">
		<img id='div_id2' alt="Photo" src='$media'>
		<p style='font-family:Arial,sans-serif; font-size:14px; font-weight:bold; text-align:center;'> $legende </p>
		<p style='font-family:Arial,sans-serif; font-size:10px; font-style:italic; color:white; text-align:right;'> 
				Cliquez sur l'image pour fermer</p>
	</div>	
</body>
</html>
EOD;
}

############################################  Format mp3, Format mp4 ou Format mov/quicktime ###
if($format=='mp3' OR $format=='mp4' OR $format=='quicktime'){
	echo <<<EOD
<body style="background-color:grey;" >
	<div id="div_id" style="margin:10px;">
		<video id='div_id2' src='$media' autoplay controls > </video>
		<p style='font-family:Arial,sans-serif; font-size:14px; font-weight:bold; text-align:center;'> $legende </p>
	</div>	
</body>
</html>
EOD;
}

################################################################################# Format pdf ###
if($format=='pdf'){
	echo <<<EOD
<body style="padding:0; margin:0; background-color:grey; " >
	<div id="div_id" style="margin:10px;">
		<embed id='div_id2' src='$media' width='900' height='900' alt='pdf'
					pluginspage='http://www.adobe.com/products/acrobat/readstep2.html'>
		<p style='font-family:Arial,sans-serif; font-size:14px; font-weight:bold; text-align:center;'> $legende </p>
	</div>	
</body>
</html>
EOD;
}

################################################################################# Format pdf ###
if($format=='img'){
	echo <<<EOD
<body style="padding:0; margin:0; background-color:#FFFFCC; " >
	<div id="div_id" style="width:300px; eight:150px; margin:10px;">
	<div id="div_id2" style="margin:10px; font-family:Arial,sans-serif; font-size:14px;">		
			<p style='font-family:Arial,sans-serif; font-size:14px;'> TÉLÉCHARGER / DOWNLOAD </p>
			<a href="$media" download="$fname"> $legende </a>
	</div>	
	</div>
</body>
</html>
EOD;
}

####################################
?>