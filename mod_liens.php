<?php
# V25			190711	PhD		Ajouté préselection dans XML_prefinv2
###

/* Protection des entrées -------------------------------------------------------
'action'				- POST  - Filtré NormIN  - uniquement testé switch
'commentlien'		- POST  - Filtré NormIN
'idcol1'				- POST  - Filtré  numérique
'idcol2'				- POST  - Filtré  numérique
'idcollection'	* REQ  transmis par URL (suivant/précédent) - filtré numérique
'idetablissement2' - POST  - Filtré NormIN
'idfamille'			- POST  - Filtré  numérique
'menu'					* REQ  transmis par URL (suivant/précédent) - uniquement testé switch
'nrinv2'				- POST  - Filtré NormIN
'idlien'				- POST  - Filtré  numérique
	// utilisé par modifier_table :
(champs)				* POST - tous les champs sont filtrés NormIN
------------------------------------------------------------------------------ */

require_once ('init.inc.php');
require_once ("mod_liens.inc.php");

### Traitement des entrées :
###########################
	$action = NormIN ('action');
	$commentlien = NormIN ('commentlien');

	$idcol1 = @$_POST['idcol1'];
	if ($idcol1 && !is_numeric($idcol1)) DIE ("*** Paramètre 'idcol1' faux ! ***"); 
	$idcol2 = @$_POST['idcol2'];
	if ($idcol2 && !is_numeric($idcol2)) DIE ("*** Paramètre 'idcol2' faux ! ***"); 
   
  $idcollection = @$_REQUEST['idcollection'];
  if (!is_numeric($idcollection)) 	DIE ("*** Paramètre 'idcollection' faux ! ***"); 

	$idfamille = @$_POST['idfamille'];
	if ($idfamille && !is_numeric($idfamille)) DIE ("*** Paramètre 'idfamille' faux ! ***"); 
	
	$idetablissement2 = NormIN ('idetablissement');
	$nrinv2 = NormIN ('nrinv2');   

	$idlien = @$_POST['idlien'];
	if ($idlien && !is_numeric($idlien)) DIE ("*** Paramètre 'idlien' faux ! ***"); 

	
# Initialisations ##############################
// Debut () et MenuConsulter () sont déjà traités dans "consulter.php"

$mode = "liste";		// Valeur par défaut pour affichage simple de la liste
$Xvars = array();
$Xvars['nbrfamille'] = Nbrlignes ('Familles');					// pour dimensionner les Select


#######################################################################################
### Aiguillage suivant action demandée
#######################################################################################
switch ($action) {

	######################### Aiguillage selon le bouton choisi à l'intérieur du bloc "Modifier" ###
	#================================================================================== Ajouter ===
	case 'ajouter' :
		$Xvars['id_select'] = valpardefaut ('Liens', 'idfamille');
		
		// Repérer l'établissement courant
		$result = requete ("SELECT idetablissement FROM Collections WHERE idcollection='$idcollection'");
		$ligne = mysqli_fetch_assoc ($result);
		$Xvars['idetab1'] = $ligne ['idetablissement'];
		
		$mode = 'ajout';						// Changer le mode d'affichage et ré-afficher
		break;
		
	#================================================================================== Supprimer ===
	case 'supprimer' :
		// Vérification 
		if (!$idlien) { 
				erreurMsg ("Vous n'avez sélectionné aucun lien"); 
				break;
		}

		$result = requete ("DELETE FROM Liens WHERE idlien='$idlien'");
		if ($result) Message ("- Le lien a été supprimé - ");

		miseaJour ($idcollection);
		break;
		
	#================================================================================== Inverser ===
	case 'inverser' :
		// Vérification 
		if (!$idlien) { 
				erreurMsg ("Vous n'avez sélectionné aucun lien"); 
				break;
		}

		$result = requete ("SELECT idcol1, idcol2 FROM Liens WHERE idlien='$idlien'");
		if ($result) {
			$ligne = mysqli_fetch_assoc ($result);
			$idcol1 = $ligne['idcol1'];
			$idcol2 = $ligne['idcol2'];		
			$result = requete ("UPDATE Liens SET idcol1=$idcol2, idcol2=$idcol1 WHERE idlien='$idlien'");
			if ($result) Message ("- Le lien a été inversé - ");
		}
		break;
		
	#================================================================================== Modifier ===
	case 'modifier' :			
	// Vérification 
		if (!$idlien) { 
	  	erreurMsg ("Vous n'avez sélectionné aucun lien"); 
	  	break;
		}
		$result = requete ("SELECT * FROM Liens WHERE idlien='$idlien'");
			$ligne = mysqli_fetch_assoc ($result);
			
			$Xvars['idl'] = $idlien;
			$Xvars['idcol1'] = $ligne['idcol1'];
			$Xvars['idcol2'] = $ligne['idcol2'];	
			$Xvars['clien'] = $ligne['commentlien'];			// "#$commentlien" donne une chaine vide !!!
			$Xvars['id_select'] = $ligne['idfamille'];		// pointer la ligne à sélectionner

		$mode = 'modif';		// Changer le mode d'affichage et ré-afficher
		break;
	

	#================================================================================== Annuler ===
	case 'annuler' :
		break;	// Le cas "Annuler" ne demande aucun traitement spécifique.
		

	########################## Aiguillage selon le bouton choisi à l'intérieur du bloc "Ajouter" ###
	#================================================================================== Enregistrer ===
  case 'enregistrer' :
	
		// Vérification des erreurs
		if (!$idcol2 AND !$nrinv2) {
			erreurMsg ("Vous n'avez proposé aucun lien"); 	  
			break;
		}
	
		// Contrôle du numéro fourni
		if (!$idcol2) $idcol2 = nrinv_to_idcol ($nrinv2, $idetablissement2);
		$table_elem = Section ($idcol2);
		if (!$table_elem) {
			erreurMsg ("Ce numéro n'existe pas...");
			break;
		}
	 
		// Vérification de l'identité
		if (!in_array ("mod_objet", $droits)) {
			erreurMsg ("Vous ne vous êtes pas identifié..."); 
			include ('identification.php');
			exit;
		}
	
		// Vérifier que le lien n'existe pas déjà, puis écrire
	  $query = "
		 SELECT idcol1, idcol2 FROM Liens WHERE (idcol1 = '$idcollection' AND idcol2 = '$idcol2') ";
	  $result = requete ($query);
	  $compteur = mysqli_num_rows ($result);
	  
	  if ($compteur == 0 && ($idcol2 != $idcollection)) {
		 requete ("INSERT INTO Liens VALUES (NULL,'$idcollection','$idcol2','$commentlien','$idfamille')");
		 Message ("- Le lien %0 / %1 a été ajouté - ", $idcollection, $idcol2);
	  } else {
		 Message ("- Le lien %0 / %1 existe déjà - ", $idcollection, $idcol2);
	  }
	
		miseaJour ($idcollection);
		break;

	########################## Aiguillage selon le bouton choisi à l'intérieur du bloc "Modifier" ###
	#================================================================================== Exécuter ===
	case 'executer' :			// Retour du tableau de modif

		// Vérification des erreurs
		if ($idcol1 != $idcollection AND $idcol2 != $idcollection) {
			erreurMsg ("Un des indices [id] doit obligatoirement être celui de l'objet courant");
			break;
		}
		
		if ($idcol1 != $idcollection) {			 	// Si le premier indice n'est pas l'objet courant, 
																					// vérifier son existence
			if (!Section ($idcol1)) {
				erreurMsg ("[id] origine n'existe pas...");
				break;
			}
		} elseif ($idcol2 != $idcollection) {		// Si le second indice n'est pas l'objet courant, 
																						// vérifier son existence
			if (!Section ($idcol2)) {
				erreurMsg ("[id] cible n'existe pas...");
				break;
			}
		} 
		
		// Pas d'autres controles => écrire
		$commentsql = NormSQL ($commentlien);
		$requete = "UPDATE Liens SET idcol1=$idcol1, idcol2=$idcol2, idfamille=$idfamille, commentlien='$commentsql' 
								WHERE idlien=$idlien";
		if (Requete ($requete)) MiseaJour ($idcollection);
		else erreurMsg ("Modification refusée...");
		break;			
						 
}		// end_switch action

####################################################################################################
# Affichage écran principal "Modifier un lien" et sur demande "Établir un lien", "modifier un lien" 
####################################################################################################

 	$Xvars['idcollection'] = $idcollection;
 	$Xvars['menu'] = $menu;
 	$Xvars['mode'] = $mode;
	$Xvars['num_commune'] = isset ($dbase['num_commune']);		// Cas particulier numération commune à tous établissements

	// Afficher à partir du modèle XML	
	$liste_xml = Xopen ('./XML_modeles/mod_liens.xml') ;
	Xpose ($liste_xml);

  Fin ();
?>