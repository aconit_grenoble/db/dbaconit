<?php
# v24.1		190318	PhD		Supprimé initialisation nrinv et designation, non utilisés
# v25			190615	PhD		Supprimé inst debug 8
###

/* Protection des entrées -------------------------------------------------------

'action'			- POST non accessible - uniquement testé switch
'ajouts[]'			- POST non accessible
'idcollection'		* REQ  transmis par URL (suivant/précédent) - filtré numérique
'menu'				* REQ  transmis par URL (suivant/précédent) 
'supprs[]'			- POST non accessible
------------------------------------------------------------------------------ */

############################################################ XML_liste_attach ###
function XML_liste_attach ($loop, $attr, $Xaction) {
	global $Xvars;
	
	if ($loop === null) return;		// tag de fin
	
	if ($loop == 0) 							// Premier appel, lire la table
 		$Xvars['SQLresult_attach'] = requete ($Xvars['req_aff_attach']);

	//  Appel du mot courant
	$ligne = mysqli_fetch_assoc ($Xvars['SQLresult_attach']); 
	$Xvars['mot'] = Snv ($ligne [$Xvars['nom_mot']], $ligne [$Xvars['nom_mot'].'_en']);
	$Xvars['idmot'] = $ligne ['id'.$Xvars['nom_mot']];
	
	return ($ligne) ? 'ACT,LOOP' : 'EXIT' ;
} 

############################################################# XML_liste_choix ###
function XML_liste_choix ($loop, $attr, $Xaction) {
	global $Xvars;
	
	if ($loop === null) return;		// tag de fin
	
	if ($loop == 0) 							// Premier appel, lire la table
 		$Xvars['SQLresult_choix'] = requete ($Xvars['req_aff_tout']);

	//  Appel du mot courant
	$ligne = mysqli_fetch_assoc ($Xvars['SQLresult_choix']);
	$Xvars['mot'] = Snv ($ligne [$Xvars['nom_mot']], $ligne [$Xvars['nom_mot'].'_en']);
	$Xvars['idmot'] = $ligne ['id'.$Xvars['nom_mot']];
		
	return ($ligne) ? 'ACT,LOOP' : 'EXIT' ;
} 

############################################################################
############################################################ CORPS PROGRAMME
require_once ('init.inc.php');
require_once ('consulter.inc.php');


## Traitement des entrées :
###########################
	$action = @$_POST['action'];
	$ajouts = @$_POST['ajouts'];  	
   	$idcollection = @$_REQUEST['idcollection'];
   if (!is_numeric($idcollection)) 	DIE ("*** Paramètre 'idcollection' faux ! ***"); 
   
	$menu = @$_REQUEST['menu'];
	$supprs = @$_POST['supprs'];
	
# Initialisations ##############################

// Debut () et MenuConsulter () sont déjà traités dans "consulter.php"

// Correspondance entre le menu appelé et les messages/requêtes spécifiques
switch ($menu) {
case 'M_mots clés' :
	$t_cor = array (
	'nom_mot' => 'motcle',
	't_detach' => Tr ("Dans la liste des mots clés attachés", "In the attached keywordslist").
		",<br/>".Tr("sélectionnez les mots clés à supprimer :", "select the words to suppress:"),
	't_attach' => Tr ("Sélectionnez les mots clés à ajouter :", "Select the keywords to add:"),
	'req_suppr' => "DELETE FROM Col_Mot WHERE idcollection='$idcollection' 
		AND idmotcle=",   // '$idmot'
	'req_ajout' => "INSERT INTO Col_Mot VALUES ('$idcollection',"	// '$idmot')
	);
	$Xvars['req_aff_attach'] = "SELECT * FROM Col_Mot, Motscles 
		WHERE Col_Mot.idcollection='$idcollection' AND Col_Mot.idmotcle=Motscles.idmotcle ORDER BY motcle";
	$Xvars['req_aff_tout'] = "SELECT * FROM Motscles ORDER BY motcle";

	break;
	
case 'M_matériaux' :
	$t_cor = array (
	'nom_mot' => 'materiau',
	't_detach' => Tr ("Dans la liste des matériaux attachés", "In the attached materialslist").
		",<br/>".Tr("sélectionnez les matériaux à supprimer :", "Select the materials to suppress:"),
	't_attach' => Tr ("Sélectionnez les matériaux à ajouter :", "select the materials to add:"),
	'req_aff_attach' => "SELECT * FROM Col_Mate, Materiaux 
		WHERE Col_Mate.idcollection='$idcollection' AND Col_Mate.idmateriau=Materiaux.idmateriau ORDER BY materiau",
	'req_aff_tout' => "SELECT * FROM Materiaux ORDER BY materiau",
	'req_suppr' => "DELETE FROM Col_Mate WHERE idcollection='$idcollection' 
		AND idmateriau=",	// '$idmot'
	'req_ajout' => "INSERT INTO Col_Mate VALUES ('$idcollection'," // '$idmot')
	); 
	$Xvars['req_aff_attach'] = "SELECT * FROM Col_Mate, Materiaux 
		WHERE Col_Mate.idcollection='$idcollection' AND Col_Mate.idmateriau=Materiaux.idmateriau ORDER BY materiau";
	$Xvars['req_aff_tout'] = "SELECT * FROM Materiaux ORDER BY materiau";
}


# EXECUTION pour modification
#############################


if ($action) {
// Modifications de la table   

  // Vérification de l'identité
  if (!in_array ("mod_objet", $droits)) {
     erreurMsg ("Vous ne vous êtes pas identifié...");
     include ('identification.php');
     exit;
  }

    // Traitement de l'action demandée
    switch ($action) {
		case 'supprimer' : 
			if (isset ($supprs)) {
				foreach ($supprs as $idmot) {
					$result = requete ($t_cor['req_suppr']."'$idmot'");
					if ($result) debugMsg ("- Le lien entre le mot $idmot et l'élément $idcollection a été supprimé - ");
					miseaJour ($idcollection);
				}
			} else {
				array_push ($erreurs,"Vous n'avez sélectionné aucun mot."); $erreur++;
			}
			break;
		
		case 'ajouter' : 
			if (isset ($ajouts)) {
				foreach ($ajouts as $idmot) {
					requete ($t_cor['req_ajout']."'$idmot')");
					miseaJour ($idcollection);
				}
			} else {
				array_push ($erreurs,"Vous n'avez sélectionné aucun mot."); $erreur++;
			}
	}
         
}

# AFFICHAGE del'écran de modification principal 
###############################################

global $Xvars;
 
// Passage des paramètres principaux
$Xvars['idcollection'] = $idcollection;
$Xvars['nom_mot'] = $t_cor['nom_mot'];
$Xvars ['menu'] = $menu;

// Textes (ceci contourne le pb des accents UTF8)
$Xvars['t_detach'] = $t_cor['t_detach'];
$Xvars['t_attach'] = $t_cor['t_attach'];
$Xvars['t_subm1'] = Tr ('Détacher', 'Unlink');
$Xvars['t_subm2'] = Tr ('Attacher', 'Link');

#======================= Afficher partir du modèle XML
	$liste_xml = Xopen ('./XML_modeles/mod_mots_mats.xml') ;
	Xpose ($liste_xml);

#################################### Fin de traitement
Fin(); 
?>