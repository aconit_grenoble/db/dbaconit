<?php
# v18			170803	PhD		Création, ajouté test dans 'Ecrire_col_mouv'
# v25			190615	PhD		Supprimé inst debug 8
# v25.10	210211	PhD		Ajouté test duplication et code retour fonction Ecrire_col_mouv
###

############################################################## Ecrire_col_mouv ###
function Ecrire_col_mouv ($idcollection, $idmouvement, $commouv, $test='notest') {

	if ($test=='test') {
		// vérifier l'existence de l'enregistrement mouvements
		$res = requete ("SELECT idmouvement FROM Mouvements WHERE idmouvement=$idmouvement");
		if (0 == mysqli_num_rows ($res)) {
			erreurMsg (Tr ("Cet enregistrement mouvement n'existe pas", "This move record doesn't exist"));
			return;						// >>>>>>>>>>>>>>>>>>>>>>>> EXIT
		}
	}

### Assurer l'écriture dans Col_Mouv et la date de mise à jour de Collections
	// Tester d'abord si le lien n'existe pas
	$res = requete ("SELECT idcollection FROM Col_Mouv 
											WHERE idcollection='$idcollection' AND idmouvement='$idmouvement'");

	if (!mysqli_num_rows ($res)){					// Si le lien n'existe pas déjà 
		$res = requete ("INSERT INTO Col_Mouv VALUES ('$idcollection','$idmouvement', '$commouv')");

		if ($res) {
			miseaJour ($idcollection);
			Message ("Mouvement %0 attaché", $idmouvement);		
			return TRUE;
		}
	} else {
		erreurMsg (Tr ("Cet enregistrement existe déjà", "This record already exists"));	
		return FALSE;
	}
}


############################################################ XML_liste_attach ###
function XML_liste_attach ($loop, $attr, $Xaction) {

	if ($loop === null) return;		// tag de fin
	global $Xvars;

	// Si tag de début, appeler la liste des mouvements attachés
	if ($loop === 0) {
		$idcollection = $Xvars['idcollection'];

		$Xvars['SQLresult_attach'] = requete (
	 		"SELECT * FROM Col_Mouv 
	 		LEFT JOIN Mouvements ON Mouvements.idmouvement=Col_Mouv.idmouvement
	 		LEFT JOIN Typemouvts On Typemouvts.idtypemouvt=Mouvements.idtypemouvt
			WHERE idcollection=$idcollection
	 		ORDER BY datemouv");
 	}
	
	//  Appel du mouvement courant
	$ligne = mysqli_fetch_assoc ($Xvars['SQLresult_attach']); 

	$Xvars['mouvt'] = $ligne ['idmouvement'].
							' : '.$ligne ['datemouv'].' - '.$ligne ['typemouvt'].' - '.$ligne ['orgmouv'].'=>'.$ligne ['destmouv'] ;
	$Xvars['idmouvt'] = $ligne ['idmouvement'];
	
	return ($ligne) ? 'ACT,LOOP' : 'EXIT' ;
} 

############################################################ XML_liste_types ###
function XML_liste_types ($loop, $attr, $Xaction) {

	if ($loop === null) return;		// tag de fin
	
	global $Xvars;
	static $SQLresult_liste;
	
	// tag de début, lire la table
	if ($loop === 0) {
		$idcollection = $Xvars['idcollection'];
  	$SQLresult_liste = requete ("SELECT * FROM Typemouvts	ORDER BY Typemouvts.idtypemouvt");
  }
  
	//  Appel des valeurs courantes
	$ligne = mysqli_fetch_assoc ($SQLresult_liste);

	// Préparer la pré-sélection
	if (isset ($Xvars['idtypeselect'])) $Xvars['idselect'] = $Xvars['idtypeselect'] == $ligne['idtypemouvt'];
	else $Xvars['idselect'] = FALSE;

	// Préparer les variables
  $Xvars['idtypemouvt'] = $ligne['idtypemouvt'];
  $Xvars['typemouvt'] = $ligne['typemouvt'];
	
	return ($ligne) ? 'ACT,LOOP' : 'EXIT' ;
} 
############################################################

?>