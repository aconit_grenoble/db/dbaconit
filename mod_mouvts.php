<?php
# v18				170807	PhD		Création à partir de mod_médias, ajouté test dans 'Ecrire_col_mouv'
###

/* Protection des entrées -------------------------------------------------------
'action'				- POST - uniquement testé switch
'idcollection'	* REQ  - également transmis par URL (suivant/précédent) - filtré numérique
'idmouvement'		- POST - filtré numérique
'idtypemouvt'		- POST - filtré numérique
'datemouv'			- POST - filtré NormIn
'orgmouv'			- POST - filtré NormIn	
'destmouv'			- POST - filtré NormIn	
'comgenmouv'		- POST - filtré NormIn
'commouv'				- POST - filtré NormIn 
'menu'					* REQ	 - transmis par URL (suivant/précédent) 
'selectm[]'			- POST
------------------------------------------------------------------------------ */

require_once ('init.inc.php');
require_once ('mod_mouvts.inc.php');

## Traitement des entrées :
###########################
	$action = @$_POST['action'];

	$idcollection = @$_REQUEST['idcollection'];	
	if (!is_numeric($idcollection)) 	DIE ("*** Paramètre 'idcollection' faux ! ***"); 

	$idmouvement = @$_POST['idmouvement'];				
	if ($idmouvement AND !is_numeric($idmouvement)) 	DIE ("*** Paramètre 'idmouvement' faux ! ***"); 
		
	$idtypemouvt = @$_POST['idtypemouvt'];				
	if ($idtypemouvt AND !is_numeric($idtypemouvt)) 	DIE ("*** Paramètre 'idtypemouvt' faux ! ***"); 

	$datemouv = NormIN ('datemouv');	
	$orgmouv = NormIN ('orgmouv');	
	$destmouv = NormIN ('destmouv');	
	$comgenmouv = NormIN ('comgenmouv');	
	$commouv = NormIN ('commouv');	
		
	$selectm = @$_POST['selectm'];
			
	
# Initialisations ##############################

	// Vérification de l'identité (des fois que...)
	if (!in_array ("mod_objet", $droits)) {
		 erreurMsg ("Vous ne vous êtes pas identifié...");
		 include ('identification.php');
		 exit;
	}
	
	$mode = "principal";
	
// Debut () et MenuConsulter () sont déjà traités dans "consulter.php"

# EXECUTION pour modification
#############################

if ($action) {

### Traitement de l'action demandée
	switch ($action) {

	### Actions à partir des 4 boutons associés à la liste des médias ##############################
	#=================================================================================== Ajouter ===
		case 'ajouter1' :
			$mode = 'ajouter';						// Changer le mode d'affichage et afficher

			$Xvars['datemouv'] = date ('Y-m-d'); 
			break;
		
		
	#================================================================================== Attacher ===	
		case 'attacher1' :
			$mode = 'attacher';						// Changer le mode d'affichage et ré-afficher
			break;

	#================================================================================== Modifier ===
		case 'modifier1' :	
			// Vérification 
			if (isset ($selectm)) {
				$idmouvement = $selectm[0];			// Prendre le premier élément si sélection multiple
			} else {
				erreurMsg ("Vous n'avez sélectionné aucun mouvement"); 
				break;
			}

			// Appel des champs mouvement
			$result = requete (
				"SELECT * FROM Mouvements 
				LEFT JOIN Col_Mouv ON Mouvements.idmouvement=Col_Mouv.idmouvement
													AND Col_Mouv.idcollection=$idcollection
				LEFT JOIN Typemouvts On Typemouvts.idtypemouvt=Mouvements.idtypemouvt
				WHERE Mouvements.idmouvement='$idmouvement' ");
			$ligne = mysqli_fetch_assoc ($result);
		
			$Xvars['idmouvement'] = $idmouvement;
			$Xvars['datemouv'] = $ligne['datemouv'];
			$Xvars['idtypeselect'] = $ligne['idtypemouvt'];
			$Xvars['orgmouv'] = $ligne['orgmouv'];
			$Xvars['destmouv'] = $ligne['destmouv'];
			$Xvars['comgenmouv'] = $ligne['comgenmouv'];
			$Xvars['commouv'] = $ligne['commouv'];
				
			$mode = 'modifier';						// Changer le mode d'affichage et ré-afficher
			break;
		
	#================================================================================= Supprimer-exécution ===
		case 'supprimer' :
	
		if (isset ($selectm)) {
			foreach ($selectm as $idmouvement) {
				$result = requete ("DELETE FROM Col_Mouv WHERE idcollection='$idcollection' AND idmouvement='$idmouvement'");
				if ($result) Message ("- Le lien entre le mouvement %0 et l'élément %1 a été supprimé - ",
										$idmouvement, $idcollection);
			}
			miseaJour ($idcollection);
		} else erreurMsg ("Vous n'avez sélectionné aucun mouvement");
		break;
	 
	### Actions à partir des boutons associés aux formulaires annexes ##############################
	#=============================================================================== Ajouter-exécution ===
		case 'ajouter2' :

		//  Vérifier que la date n'est pas nulle...
		if ( 0 == substr ($datemouv, 0, 1)) {
			erreurMsg ( "Le champ date ne peut pas rester nul");
			break;						// >>>>>>
		}
		
		// Créer l'enregistrement mouvement
		requete ("INSERT INTO Mouvements
			VALUES (NULL, '$datemouv', '$idtypemouvt', '$orgmouv', '$destmouv', '$comgenmouv')");

		if ($result) {
			$idmouvement = mysqli_insert_id ($dblink);
			Ecrire_col_mouv ($idcollection, $idmouvement, $commouv);
		}

		break;	

	#================================================================================== Attacher-exécution ===	
		case 'attacher2' :

		if ($idmouvement) Ecrire_col_mouv ($idcollection, $idmouvement, $commouv, 'test');
		else erreurMsg ("Vous n'avez pas spécifié un numéro de mouvement");
		break;


	#================================================================================== Modifier-exécution ===
	case 'modifier2' :	
	
		//  Vérifier que la date n'est pas nulle...
		if ( 0 == substr ($datemouv, 0, 1)) {
			erreurMsg ( "Le champ date ne peut pas rester nul");
			break;						// >>>>>>
		}
		
		// Écrire tous les champs Mouvements susceptibles d'avoir été modifiés
		$requete = "UPDATE Mouvements SET datemouv='$datemouv', idtypemouvt='$idtypemouvt',orgmouv='$orgmouv', 
								destmouv='$destmouv', comgenmouv='$comgenmouv'
							WHERE idmouvement = $idmouvement";
								
		if (Requete ($requete)) {
			// Si écriture réussie, écrire le commentaire spécial à cet objet
				$result = requete ("UPDATE Col_Mouv SET commouv='$commouv' 
													WHERE idcollection=$idcollection AND idmouvement=$idmouvement");

		} else erreurMsg ("Erreur d'écriture");
		
		break;
		
	}
			 
}

# AFFICHAGE de l'écran principal 
###############################################

// Passage des paramètres principaux
global $Xvars;
$Xvars['idcollection'] = $idcollection;
$Xvars['mode'] = $mode;

$Xvars['menu'] = $menu;

#======================= Afficher partir du modèle XML

	$liste_xml = Xopen ('./XML_modeles/mod_mouvts.xml') ;
	Xpose ($liste_xml);

#################################### Fin de traitement
Fin(); 
?>