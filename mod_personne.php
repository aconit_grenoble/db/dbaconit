<?php
# v22				180521	PhD		Simplifié le code 'enregistrer' car 'pcivil' n'est plus 'enum'
# v23.1			180901	PhD		Ajouté champ pnotes
# v24				190225	PhD		Reprise du traitement des organismes et des personnes existantes à attacher
# v24.1			190318	PhD		Suppression initialisation nrinv, non utilisé
# v26.2			230417	PhD		Réécrit le cas 'rapporteur' qui doublonnait des noms en table Personnes
###

/* Protection des entrées -------------------------------------------------------
'action'				- POST non accessible - uniquement testé switch
'pcivil'				- POST  - Filtré NormIN
'pprenom'				- POST  - Filtré NormIN
'pnom'					- POST  - Filtré NormIN
'pnotes'				- POST  - Filtré NormIN
'presel'				- POST  - Filtré NormIN	
'idpersonne'		- POST  - filtré numérique
'idcollection'	* REQ  transmis par URL (suivant/précédent) - filtré numérique
'lettres'				- POST  - Filtré NormIN
'menu'					* REQ  transmis par URL (suivant/précédent) - uniquement testé switch
'relations[]'		- POST
'supprs[]'			- POST 
------------------------------------------------------------------------------ */

require_once ('init.inc.php');
require_once ('consulter.inc.php');
require_once ('mod_org_per.inc.php');

## Traitement des entrées :
###########################
$action = @$_POST['action'];
$pcivil = NormIN ('pcivil');
$pnom = NormIN ('pnom');
$pnotes = NormIN ('pnotes');
$pprenom = NormIN ('pprenom');
$presel = NormIN ('presel');

$idcollection = @$_REQUEST['idcollection'];
if (!is_numeric($idcollection)) 	DIE ("*** Paramètre 'idcollection' faux ! ***"); 
$idpersonne = @$_REQUEST['idpersonne'];

$lettres = NormIN ('lettres');
$menu = @$_REQUEST['menu'];
$relations = @$_POST['relations'];
$supprs = @$_POST['supprs'];

	
# Initialisations ##############################

// Debut () et MenuConsulter () sont déjà traités dans "consulter.php"

$mode = "liste";			// Valeur par défaut pour affichage simple de la liste
$Xvars = array();
$Xvars['cle'] = 'clepersonne';														// Traitement organismes											
$Xvars['nbrrelations'] = Nbrlignes ('Relations');					// pour dimensionner le Select

// Noms préselectionnés
$Xvars['psel_civil'] = '';
$Xvars['psel_prenom'] = '';
$Xvars['psel_nom'] = '';


# EXECUTION pour modification
#############################

// Vérification de l'identité
  if (!in_array ("mod_objet", $droits)) {
     erreurMsg ("Vous ne vous êtes pas identifié..."); 
     include ('identification.php');
     exit;
   }
   
# Aiguillage suivant action demandée ############################################
   switch ($action) {

	### Modifications de la table des liens ########################################################
	######################### Aiguillage selon le bouton choisi à l'intérieur du bloc "Modifier" ###

	#================================================================================== Ajouter ===
	# Afficher le formulaire d'ajout
	case 'ajouter' :
		$mode = 'ajout';	
		break;
		
	#================================================================================== Supprimer ===
	case 'supprimer' :
	   case 'supprimer' :
    // Vérification
		if (!$supprs)  {
			erreurMsg ("Vous n'avez sélectionné aucun nom");
			break;
		}
		
    $sc = 0;	 
		foreach ($supprs as $idents) {
	 		$t_ident = explode ('|', $idents);
	    if (requete ("DELETE FROM Col_Per WHERE idcollection=$idcollection AND idrelation=".$t_ident[0]." 
	    							AND idpersonne=".$t_ident[1])) {
        Message ("- Le lien entre le nom %0 et l'élément %1 a été supprimé - ", $t_ident[2], $idcollection);
	      $sc++;
	    }
		}  
		
	    if ($sc) miseaJour ($idcollection);
      break;

#================================================================================== Enregistrer ===
   case 'enregistrer' :
   		
    // Vérification des erreurs
		if (!isset ($pnom) || !$pnom) {
			erreurMsg ("Vous n'avez pas indiqué de nouveau nom");
			break;
		}
		
		if (!isset ($relations)  || $relations == 0) {
			erreurMsg ("Vous n'avez pas indiqué la nouvelle relation");
			break;
		}
		
	  // Si l'auteur est déjà dans la table, noter idpersonne sinon l'ajouter
		$SQLpcivil =  NormSQL ($pcivil);
		$SQLpprenom = NormSQL ($pprenom);
		$SQLpnom = NormSQL ($pnom);
		$SQLpnotes = NormSQL ($pnotes);		
		
		$requete = "SELECT idpersonne FROM Personnes 
								WHERE (pcivil='$SQLpcivil') AND (pprenom ='$SQLpprenom') AND (pnom ='$SQLpnom')";	
		$SQLresult = requete ($requete);

    if (0 == mysqli_num_rows ($SQLresult)) { 
			$requete = "INSERT INTO Personnes VALUES (NULL,'$SQLpcivil', '$SQLpprenom', '$SQLpnom', '$SQLpnotes')";
			if (requete ($requete)) {
				Message ("Le nom « %0 %1 %2 » a été ajouté", $pcivil, $pprenom, $pnom);
				$idpersonne = mysqli_insert_id ($dblink);
			} else {
				erreurMsg ("Impossible d'enregistrer le nouveau nom");
				break;
			}
			
    } else {
       $ligne = mysqli_fetch_assoc ($result);
   	 	 $idpersonne = $ligne['idpersonne'];
 				Message ("Le nom « %0 %1 %2 » existe déjà, il est attaché sans modifications", $pcivil, $pprenom, $pnom);
    }
    
    // et on peut enchainer sur le lien entre colection et personne
    Attacher_personne ($idcollection, $idpersonne, $relations);
		// Balayer toutes les relations demandées	  
	 break;


#================================================================================== Utiliser ===
# Afficher le formulaire demandant les premiers caractères recherchés
	case 'utiliser' :
		$mode = "rechercher";
		break;
		
#================================================================================== Rapporteur ===
# Préselectionner le rapporteur
	case 'rapporteur' :
		$idrapporteur = $_SESSION['idrapporteur'];

		// Identifier le rapporteur à partirde la table Rapporteurs		
		$result_rap = requete ("SELECT * FROM Rapporteurs WHERE idrapporteur = $idrapporteur");
		$ligne = mysqli_fetch_assoc ($result_rap);
		$Xvars['psel_civil'] = '';
		$Xvars['psel_prenom'] = $rprenom = $ligne['rprenom'];
		$Xvars['psel_nom'] = $rnom = $ligne['rnom'];
		
		// Mais il faut encore trouver son index dans la table Personnes, s'il existe.
		$result_nom = requete ("SELECT idpersonne FROM Personnes WHERE pprenom='$rprenom' AND pnom='$rnom'");
    if (0 == mysqli_num_rows ($result_nom)) {
    	$mode = 'ajout';			// L'ajouter s'il n'existe pas
    } else {
			$ligne = mysqli_fetch_assoc ($result_nom);
			$Xvars['psel_idpersonne'] = $ligne['idpersonne'];
			$mode = "attacher";
		}
		break;
		
#================================================================================== Rechercher ===
# Lancer la recherche à partir des caractères demandés, 
# Puis afficher le formulaire de sélection d'un nom dans la liste
	case 'rechercher' :
		// L'appel à la base est fait ici pour récupérer le nombre de lignes du select
		$result_nom = requete ("SELECT idpersonne, pcivil, pnom, pprenom FROM Personnes 
														WHERE pnom LIKE '$lettres%'  ORDER BY pnom, pprenom");
		$Xvars['result_nom'] = $result_nom;
		$Xvars['nb_noms'] = mysqli_num_rows ($result_nom);		// L'appel à la base est fait ici pour récupérer le nombre
		$mode = "selecter";
		break;
		
#================================================================================== Preselect ===
# Un nom a été pré-sélectionné dans le bloc, afficher le formulaire d'ajout
	case 'preselect' :
		$mode = "attacher";

		// Traiter les préselections
		$t_presel = explode ('|', $presel);
		$Xvars['psel_civil'] = $t_presel[0];
		$Xvars['psel_prenom'] = $t_presel[1];
		$Xvars['psel_nom'] = $t_presel[2];
		$Xvars['psel_idpersonne'] = $t_presel[3];
		break;
		
#================================================================================== Attacher ===
# Attacher lla personne existante, avec la relation demandée
	case 'attacher' :
		if (!isset ($relations)  || $relations == 0) {
			erreurMsg ("Vous n'avez pas indiqué la nouvelle relation");
		} else {
			Attacher_personne ($idcollection, $idpersonne, $relations);
		}	
 		$mode = "liste";
		break;
		
#================================================================================== Annuler ===
# Le cas "Annuler"  ne demande aucun traitement spécifique.
 	case 'annuler' :
 		$mode = "liste";
 		break;
 		
 	}	// end_switch

	   
########################################################################################################################
# Affichage écran principal et sur demande "Ajouter nom", "Rechercher nom existant" 
########################################################################################################################

 	$Xvars['idcollection'] = $idcollection;
 	$Xvars['menu'] = $menu;
 	$Xvars['mode'] = $mode;

	// Afficher à partir du modèle XML	
	$liste_xml = Xopen ('./XML_modeles/mod_personne.xml') ;
	Xpose ($liste_xml);

  Fin ();
?>  