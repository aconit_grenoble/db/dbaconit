<?php
# v17.1		170509	PhD		Création
# v18			170725	PhD		Correction messages
# v19			170903	PhD		Message alerte en anglais
# v25			190709	PhD		Adaptation au 'type' objet & iconographie, retouche Design_titre
# v25.8		200410	PhD		Ajout paramètre etat_vaide
###

/* Protection des entrées -------------------------------------------------------
'ntype'				- POST  - Filtré Normin - valeurs controlées
------------------------------------------------------------------------------ */

require_once ('init.inc.php');

### Traitement des entrées :
###########################
# Ce module est appelé en 'include' par 'consulter.php', il dispose des variables 'idcollection' et 'table'
# mais pas du 'type'...

// Avant retour de l'écran de choix, 'ntable' est NULL
$ntype = NormIN ('ntype', 'P');
if  (!in_array ($ntype, array ('machine', 'document', 'logiciel', 'objet', 'iconographie', NULL)))
	DIE ("*** Paramètre 'ntype' faux ! ***"); 
	
// Lire le paramètre indiquant les états validés
$etat_valide = @$dbase['etat_valide'];
$etat_valide = explode(',', $etat_valide);

### Contrôle de validité de la modification ###########################################
// Lire l'état fiche
$r = requete (" SELECT idetatfiche FROM Collections WHERE idcollection = $idcollection"); 
$ligne = mysqli_fetch_assoc($r);
$idetatfiche = $ligne['idetatfiche'];
	
// La modification n'est pas autorisée si la fiche est déjà validée
if (in_array ($idetatfiche, $etat_valide)) {
	$text = Tr ("Le changement de type n\'est pas autorisé pour les fiches déjà validées !", 
							'Type modification is not allowed on validated records!');
	echo"<script language='javascript'>";
	echo"alert('$text')";
	echo"</script>";

	//	Réafficher la fiche et sortir
	Affobjet ('Complet', $table, $idcollection);
	Fin ();
	exit;					// >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> EXIT >>>
}

# Initialisations #####################################################################
// Debut () et MenuConsulter () sont déjà traités dans "consulter.php"

$Xvars = array();

$r = Requete ("SELECT type FROM Collections WHERE idcollection=$idcollection");
$ligne = mysqli_fetch_assoc ($r);
$type = $ligne['type'];

// Paramètres de l'enregistrement à supprimer
$t_old = array (	'machine' => array ('o_table'=>'Machines', 'o_id'=>'idmachine', 'o_nom'=>'nom'),
									'document' => array ('o_table'=>'Documents', 'o_id'=>'iddocument', 'o_nom'=>'titredoc'), 
									'logiciel' => array ('o_table'=>'Logiciels', 'o_id'=>'idlogiciel', 'o_nom'=>'titrelog'), 
									'objet' => array ('o_table'=>'Machines', 'o_id'=>'idmachine', 'o_nom'=>'nom'), 
									'iconographie' => array ('o_table'=>'Documents', 'o_id'=>'iddocument', 'o_nom'=>'titredoc') ); 
// Paramètres de la table à créer
$t_new = array (	'machine' => array ('n_table'=>'Machines', 'n_id'=>'idmachine', 'n_nom'=>'nom'),
									'document' => array ('n_table'=>'Documents', 'n_id'=>'iddocument', 'n_nom'=>'titredoc'), 
									'logiciel' => array ('n_table'=>'Logiciels', 'n_id'=>'idlogiciel', 'n_nom'=>'titrelog'),
									'objet' => array ('n_table'=>'Machines', 'n_id'=>'idmachine', 'n_nom'=>'nom'), 
									'iconographie' => array ('n_table'=>'Documents', 'n_id'=>'iddocument', 'n_nom'=>'titredoc') ); 

#######################################################################################
### Traitement de la réponse
#######################################################################################
if  (in_array ($ntype, array ('machine', 'document', 'logiciel', 'objet', 'iconographie'))) {
	extract ($t_old[$type]);			// Paramètres de l'enregistrement à supprimer
	extract ($t_new[$ntype]);			// Paramètres de l'enregistrement à créer
	
### Même table : simple enregistrement du nouveau type	#################################
	if ($o_table == $n_table) {
		$r = requete ("UPDATE Collections SET type='$ntype' WHERE idcollection = $idcollection");
		if (mysqli_affected_rows ($dblink)) Message ("Fiche corrigée", "Data base updated");

	} else {
### Tables différentes effacement et création d'un nouvel item ##########################
	### Lecture du nom/titre de l'objet
		$r = requete (" SELECT $o_nom, $table.$o_id FROM Collections, $o_table 
										WHERE Collections.$o_id = $table.$o_id AND idcollection = $idcollection"); 
		$ligne = mysqli_fetch_assoc($r);
		$nom_obj = $ligne[$o_nom];

	### Effacement de l'enregistement en table
		$r = requete ("DELETE FROM $o_table WHERE $o_id = ".$ligne[$o_id] );
		if (mysqli_affected_rows ($dblink)) Message ("Enregistrement %0 supprimé", $o_table);

				
	### On crée un enregistrement en table Machines ou Documents ou Logiciels
	### et on place le nom de l'objet
		if (!($r = requete ("INSERT INTO $n_table SET $n_nom = \"$nom_obj\" "))) 
				erreurMsg ("Pas d'inscription dans %0", $n_table); 	
		else {	
			if (mysqli_affected_rows ($dblink)) Message ("Enregistrement %0 créé", $n_table);
			// Récupérer l'index de cette table
			$idtable = mysqli_insert_id ($dblink);


	### Et on corrige la fiche Collections : supprimer l'ancien lien et écrire le nouveau
			$r = requete ("UPDATE Collections SET type='$ntype', $o_id = NULL, $n_id = $idtable 
												WHERE idcollection = $idcollection");
		if (mysqli_affected_rows ($dblink)) Message ("Fiche corrigée", "Data base updated");
		}

	}
### On enchaîne sur l'affichage des résultats, 
### puis ré-affichage du formulaire... À l'utilisateur de choisir la suite
	AfficheMessages ();			// afficher les messages en tête
	$type = $ntype;
	
}

####################################################################################################
# Affichage de l'écran formulaire
####################################################################################################

	$Xvars['dbase'] = $dbase;
 	$Xvars['idcollection'] = $idcollection;
 	$Xvars['designation'] = Design_titre ($idcollection, FALSE);
 	$Xvars['menu'] = $menu;
 	$Xvars['type'] = $type;

	// Afficher à partir du modèle XML	
	$liste_xml = Xopen ('./XML_modeles/mod_type.xml') ;
	Xpose ($liste_xml);
 
	Fin ();

?>