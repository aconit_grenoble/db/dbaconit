<?PHP
# v23.3			181108	PhD		Dans item : ppel à Design_titre pour préparer la désignation affichée
#	v24				1900219	PhD		localisation=>etablissement, préparé pour affichage vignettes, ajout XML_prefinv
# v25				190709	PhD		Ajouté comptage pbjet et icono, retouche Design_titre
# v25.0.01	190715	PhD		Corrigé commentaire appel de Design_titre
# v25.4			191205  PhD		Ajout 'Aiguil_rech_resultat', envoi systématique de $loop
# v25.5			200102	PhD		Suppression pondérations provisoire session, traitement fichier note_recol
# v25.7			200317	PhD		Ajouté Prepare_photo pour permettre affichage photo élargie
# v25.8			200412	PhD		Le fichier note-recol est déplacé dans $db_plugin
# v25.14		220714	PhD		Pas de traitement média si nb_media==0 (PHP 8)
# v26.2.01	230427	PhD		Modifié 'XML_item' pour lire cotemedia
###

/* Protection des entrées -------------------------------------------------------
'ntype'				- POST  - Filtré Normin - valeurs controlées
------------------------------------------------------------------------------ */

require_once ('init.inc.php');

### Traitement des entrées :
###########################
# Ce module est appelé en 'include' par 'consulter.php', il dispose des variables 'idcollection' et 'table'
# mais pas du 'type'...

// Avant retour de l'écran de choix, 'ntable' est NULL
$ntype = NormIN ('ntype', 'P');
if  (!in_array ($ntype, array ('machine', 'document', 'logiciel', 'objet', 'iconographie', NULL)))
	DIE ("*** Paramètre 'ntype' faux ! ***"); 

### Contrôle de validité de la modification ###########################################
//
###

######################################################################################## Aiguil_rech_resultat ###
function Aiguil_rech_resultat () 
# Affichage de la table des résultats, dans le bon mode (standard, récolement/placement, désherbage, sélection)
# en fonction des indicateurs P_RECOL et P_SELECT
{		
	global $dir_plugin, $note_recol, $Xvars;
			
	// Mode standard
	if (!Pb (P_RECOL) AND !Pb (P_SELECT)) {
		$liste_xml = Xopen ('./XML_modeles/rech_resultat.xml') ;
		Xpose ($liste_xml);
	
	// Modes récolement/désherbage
	} elseif (Pb (P_RECOL) AND !Pb (P_SELECT)) {		
		$tstruct = Tstructure (array ('Collections'));	 // Création d'une table de structure simplifiée
		$Xvars['tstruct'] = $tstruct;	// ajouter la table de structure
		if (file_exists ($dir_plugin.$note_recol)) {		// oter le suffixe .xml qui est ajouté par le call
				$Xvars['note_recol'] = '../'.$dir_plugin.substr($note_recol, 0, -4);
		} else $Xvars['note_recol'] = FALSE;
		
		$liste_xml = Xopen ('./XML_modeles/rech_resultat_recol.xml') ;
		Xpose ($liste_xml);
		
	}	elseif (!Pb (P_RECOL) AND Pb (P_SELECT)) {
		if (isset ($_SESSION['ids'])) $Xvars['ids'] = $_SESSION['ids'];
  	else $Xvars['ids'] = array ();
  	
		$liste_xml = Xopen ('./XML_modeles/rech_resultat_desh.xml') ;
		Xpose ($liste_xml);
			
	} elseif  (Pb (P_RECOL) AND Pb (P_SELECT)) {
		if (isset ($_SESSION['ids'])) $Xvars['ids'] = $_SESSION['ids'];
  	else $Xvars['ids'] = array ();
  	
		$liste_xml = Xopen ('./XML_modeles/rech_resultat_sel.xml') ;
		Xpose ($liste_xml);
	}
}

 
######################################################################################## MajRequetes ###
function MajRequetes ($SQL, $NomRequete) {
# Ajout, modification ou suppression d'une requête prédéfinie
# $SQL : code SQL de la requête (effacer la requête si vide)
# $ NomRequete : Nome de la requête à créer ou à modifier
###
	global $droits, $dblink;
	
#--- Si nom vide ou si pas autorisation de modif : sortie immédiate
	if (empty ($NomRequete) OR !in_array ("mod_req", $droits)) return;		// >>>>>>>>>>>>>> RETURN
	

#--- Identifier les requêtes de ce nom et décider des actions
	$act = '';			// par défaut
	$result = requete ('SELECT * FROM SQLrequetes WHERE nom = "'.$NomRequete.'" ORDER BY idrapporteur');
	
	// Si aucune requête de ce nom : ajouter
	if (0==mysqli_num_rows ($result)) $act = 'insérée';
	
	// sinon balayer les différentes réponses
	else {
		while ($req = mysqli_fetch_array($result)) {
			// Si requête commune, pas d'action possible
			if ($req['idrapporteur'] <= 0) {
				$act = 'rien'; 
				break; 													// >>> sortie du while
			}		
			
			// Si c'est une requête privée de ce rapporteur...
			if ($req['idrapporteur'] === $_SESSION['idrapporteur']) {
				// si le contenu SQL est vide, effacer la requête
				if (empty ($SQL)) $act = 'effacée';
				// sinon, si SQL est différent : mettre à jour
				else if ($SQL != $req['requete']) $act = 'mise à jour';
				// si même contenu : rien à faire
				else $act = 'rien';
			}			
		}
		
		// Balayage terminé. Si rien n'est décidé : ce n'est pas un nom de requête commune, ce n'est pas "ma" requête privée
		// ==> on peut ajouter celle-ci	
		if ($act == '') $act =  'insérée';
	}
	
	switch ($act) {
	case 'rien' : break;
	
	case 'effacée' :
		requete ('DELETE FROM SQLrequetes WHERE idrapporteur='.
		$_SESSION['idrapporteur'] . ' AND nom="' . $NomRequete.'"');
		break;
		
	case 'mise à jour' :
		requete ('UPDATE SQLrequetes SET requete = "' . NormSQL($SQL) .
		'" WHERE idrapporteur = '. $_SESSION['idrapporteur'] . 
		' and nom = "' . $NomRequete . '"');
		break;
		
	case 'insérée' :
		requete ('INSERT INTO SQLrequetes VALUES (0,'.$_SESSION['idrapporteur'].
		', "'.$NomRequete.'", "'.NormSQL($SQL).'")');
	}
	
#--- Si une action a été réalisée, l'afficher	
	if ( $act != 'rien' && mysqli_affected_rows ($dblink))  Message ("Requête '%0' %1 ", $NomRequete, $act);
	return;
}

### Fonctions utilisées dans l'affichage 'rech_form.xml'
#################################################################################

################################################################# XML_prefinv ###  

function XML_prefinv ($loop, $attr, $Xaction) {
	if ($loop === null) return;		// tag de fin
	
	global $Xvars;
	
	// tag de début, lire la table
	if ($loop === 0) {
  	$Xvars['SQL_prefinv'] = requete ("SELECT idetablissement, prefinv FROM Etablissements  ORDER BY prefinv");
  }
  
	//  Appel des valeurs courantes
	$ligne = mysqli_fetch_assoc ($Xvars['SQL_prefinv']);
	
	// Préparer les variables
  $Xvars['idetablissement'] = $ligne['idetablissement'];
  $Xvars['prefinv'] = $ligne['prefinv'];
	
	return ($ligne) ? 'ACT,LOOP' : 'EXIT' ;
} 

#====================================================================================== XML_requetes ===
function XML_requetes ($loop, $attr, $Xaction) {
	if ($loop === null) return;		//  Détection de la balise finale >>>>>>>>>>>>>>>>>>>>>>>>>>
	
	global $Xvars, $droits;

	// tag de début, 
	if ($loop === 0) {
		$where = "WHERE idrapporteur=-1 ";
		if (in_array ("aff_req_com", $droits)) $where .= "OR idrapporteur=0 ";
		if (in_array ("aff_req_priv", $droits)) $where .= "OR idrapporteur=". $_SESSION['idrapporteur'];

		$Xvars['result'] = requete ('SELECT * FROM SQLrequetes ' .$where. ' ORDER BY idrapporteur, nom ');
	}
	
	if ($r = mysqli_fetch_assoc ($Xvars['result'])) {
		$Xvars['r_nom'] = $r['nom'];
		$Xvars['r_req'] = $r['requete'];
		
		$Xvars['onchange'] = "document.getElementById('Nom').value='".str_replace("&#039;", "\\u0027", $r['nom'])."'; 				
							document.getElementById('SQL').value='".str_replace("&#039;", "\\u0027", $r['requete'])."';" ;
							
		$Xvars['r_class'] = ($r['idrapporteur'] > 0) ? 'req_usr' : (($r['idrapporteur'] == 0) ? 'req_sys' : 'req_com');
		$Xvars['loop'] = $loop;
		return 'ACT,LOOP';
	} else
		return  'EXIT';

}

### Fonctions utilisées dans l'affichage 'rech_resultat.xml' et 'rech_resultat_recol.xml'
#########################################################################################

#====================================================================================== XML_item ===
function XML_item ($loop, $attr, $Xaction) {
	if ($loop === null) return;		//  Détection de la balise finale >>>>>>>>>>>>>>>>>>>>>>>>>>
	
	global $db, $Xvars;
	
	if ($ligne = mysqli_fetch_assoc ($Xvars['SQLresult'])) {
		$Xvars = array_merge ($Xvars, $ligne);
	
		// Composer le numéro d'inventaire complet
		$Xvars['nrinventaire'] = Nrinventaire ($ligne['idcollection']);
		
		// Désignation sans affichage du type d'objet
		$idcollection = $ligne['idcollection'];			
		$Xvars['design_titre'] = Design_titre ($idcollection, FALSE);  


		// Compter machines/docs/logs/objets/iconos
		if ($Xvars['type'] == 'machine') $Xvars['compt_c'] +=1;
		elseif ($Xvars['type'] == 'document') $Xvars['compt_d'] +=1;
		elseif ($Xvars['type'] == 'logiciel') $Xvars['compt_l'] +=1;
		elseif ($Xvars['type'] == 'objet') $Xvars['compt_o'] +=1;
		elseif ($Xvars['type'] == 'iconographie') $Xvars['compt_i'] +=1;
		
		// Trouver le nombre de médias associés et le premier idmedia
		$SQLresult_medias = requete (			
			" SELECT Col_Med.idmedia, cotemedia, descrimedia, ordre_media, formatmedia
					FROM Medias, Col_Med 
					WHERE Col_Med.idmedia = Medias.idmedia AND Col_Med.idcollection = $idcollection
					ORDER BY ordre_media");

		$Xvars['nb_medias'] = mysqli_num_rows ($SQLresult_medias);	
		if ($Xvars['nb_medias']){
			$l_photo = mysqli_fetch_assoc ($SQLresult_medias);
			$Xvars['idmedia']	= $l_photo['idmedia'];
			$Xvars['ext'] = $l_photo['formatmedia'];

			Prepar_photo ($l_photo);		// Préparer l'instruction d'appel javascript des photos élargies
		} else $Xvars['mpview'] = ''; // (éviter erreur si Prepar_photo pas appelé...)
		
		// Alternance des couleurs de ligne
		$Xvars['class'] =  ($loop % 2) ? 'collig1' : 'collig2';
		// Transmettre le compteur pour numéroter les lignes en cas d'affichage récolement ou désherbage
		$Xvars['loop'] = $loop;
		
		return 'ACT,LOOP';
		
	} else {
		$ligne = array ();	// Pour éviter warning avec fonction extract
		return  'EXIT';
	}
} 

?>