<?PHP
#	Ensemble des fonctions utilisées pour le traitement AJAX de l'écran de modification Médias
############
# v12.8			140115	MSA	Création pour nouveau traitement des Médias
# v13.0			150707	PhD Introduction Mysqli
# v25.1			190717	PhD		Corrigé MYSQLI_BOTH

require_once ('init.inc.php');

$id 		= ($_POST["param1"]) ;

	//recupere description du media dans la table medias + format media
	$result = requete ("select descrimedia,formatmedia from Medias where idmedia=$id");
	if (!$result) die ('Erreur: ' . mysqli_error ($dblink));	
	$result = mysqli_fetch_array ($result, MYSQLI_BOTH);
	$descri_media = $result['descrimedia'];
	$format_media = $result['formatmedia'];
	$chemin_photo_media = AdMedia ($id, $db, $format_media);
	$chemin_vignette_media = AdMedia ($id, $db, $format_media, $t='v');
	
	$rapport = "";
	if (file_exists($chemin_vignette_media) == true) {
		if (unlink($chemin_vignette_media)) 
			$rapport = "Fichier vignette supprim&eacute;<br/>";
		else 
			$rapport = "ERREUR fichier vignette<br/>";
	} else $rapport = "Pas de fichier vignette<br/>";
	
	if( file_exists($chemin_photo_media) == true) {
		if (unlink($chemin_photo_media))
			$rapport .= "Fichier media supprim&eacute;<br/>";
		else 
			$rapport .= "ERREUR fichier media<br/>";
	} else $rapport .= "Pas de fichier media<br/>";
	
//Effacement de la ligne dans la table Médias
	if (requete ("DELETE FROM Medias WHERE idmedia='$id'")) 
		$rapport .= "Enregistrement supprim&eacute;";
	else
		$rapport .= "ERREUR table Medias";

		echo $rapport;

?>
