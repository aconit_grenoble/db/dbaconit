<?php
# v18				170704	PhD		Création
# v25.4			101211 	PhD		Corrigé la vérification du transfert
# v25.8			200411	PhD		Ajout custom_css
# v25.9			200718	PhD		Ajout numéro inventaire
# v26.5			231223	PhD		La comparaison porte maintenant sur les fiches état 2 plus les fiches état 4
###

$custom_css = "utilit_diffpatstec.css";
require_once ('init.inc.php');					// Initialisations, identification..

#################################################################################

/* Protection des entrées -------------------------------------------------------
'action'				- POST - Uniquement testé isset
------------------------------------------------------------------------------ */

## Traitement des entrées :
###########################

$action = @$_POST['action'];

Debut ();

// Si le nom de fichier est défini...
if ($action) {

#--- Vérification du transfert du fichier
	if ($_FILES['fichier']['error'] ==0) {
	
		$xml_file = $_FILES['fichier']['tmp_name'];

#################################################### Comparaison des listes

#--- Ouverture du fichier XML extrait de la base PATSTEC
		libxml_use_internal_errors(true);
		if (false === ($simplexml = @simplexml_load_file($xml_file))) {
			echo "<h2 style='text-align:center; color:red;'>Erreurs dans le fichier &lt;$xml_file&gt; </h2>";
			foreach(libxml_get_errors() as $error) {
				echo "<p> $error->message </p>";
			}
			exit;						//>>>>>>>>>>>>>>>>>>>>>>>>
		}

echo '<h1>'.Tr ("Différences entre les fiches DBAconit et les fiches PATSTEC :",  
							'Differences between DBAconit files and PATSTEC files:').'</h1>';
echo '<h4>'. Tr ("Les 2 bases utilisent l'index [id] (idcollection) pour identifier les fiches",
								"The two databases use [id] (idcollection) as file index"). '</h4>';
#--- Créer une table des fiches PATSTEC
		$tab_patstec = array ();

		// Exploration du fichier XML, de fiche en fiche
		foreach ($simplexml->Zone_répétée as $fiche) {
			$num = (string)$fiche->ATT_numero_fiche;
			$titre = (string)$fiche->ATT_titre;
			$numinv = (string)$fiche->ATT_numero_inventaire;
			$tab_patstec[$num] = $numinv." - ".$titre;
		}

echo '<p>'.Tr ('Nombre de fiches PATSTEC : ', 'Number of PATSTEC files: ').count($tab_patstec).'</p>';

#--- Créer une table des idcollections de la base locale
		$tab_aconit = array ();
	
		$result = requete ("SELECT idcollection, nom, titredoc, titrelog FROM Collections 
							left join Machines on Machines.idmachine=Collections.idmachine    
							left join Documents on Documents.iddocument=Collections.iddocument    
							left join Logiciels on Logiciels.idlogiciel=Collections.idlogiciel     
							WHERE idetatfiche=2 OR  idetatfiche=4 ORDER BY idcollection");
		while ($ligne = mysqli_fetch_array ($result)) {
			$titre = $ligne['nom'].$ligne['titredoc'].$ligne['titrelog'];	// un seul est non vide
			$tab_aconit[$ligne['idcollection']] = $titre;
		}

echo '<p>'.Tr ('Nombre de fiches DBAconit : ', 'Number of DBAconit files: ').count($tab_aconit).'</p>';

#--- Comparer les tables
		$exces_patstec = array_diff_key ($tab_patstec, $tab_aconit);
		echo "<pre>";
		echo '<h2>'. Tr ("Fiches PATSTEC en excès :", 'Excess PATSTEC files:').'</h2>';
		print_r ($exces_patstec);

		$exces_aconit = array_diff_key ($tab_aconit, $tab_patstec);
		echo '<h2>'. Tr ("Fiches ACONIT en excès :", 'Excess ACONIT files:').'</h2>';
		print_r ($exces_aconit);
		echo "</pre>";


	} else {
		 erreurMsg ("Erreur de transfert du fichier (# %0)",$_FILES['fichier']['error']);
		 AfficheMessages ();
	}
}

#################################################### AFFICHAGE(S)
$Xvars['action'] = $action;

$liste_xml = Xopen ('./XML_modeles/utilit_diffpatstec.xml') ;
Xpose ($liste_xml);

Fin ();
?>

