<?php
# v24				190219	PhD		localisation=>etablissement, utilisé Nrinventaire
# v25				190615	PhD		Supprimé inst debug 8
# v25.8			200411	PhD		Ajout custom_css
# v25.14		220716	PhD		argument de strlen non nul (PHP 8.1)
###


/* Protection des entrées -------------------------------------------------------
'marks[]'			- POST 	- comparé par valeur 
'raps[]'			- POST	- comparé par valeur	
're_afficher'	- POST	- testé isset
------------------------------------------------------------------------------ */

############################################################# Affich_etat ###
function Affich_etat ($idetatfiche) {
	return ($idetatfiche>5 AND $idetatfiche<11);
}

########################################################### XML_etatfiche ###
function XML_etat ($loop, $attr, $Xaction) {

	if ($loop === null) return;		// tag final : </item>
	
	global $Xvars;
	static $SQLresult_etat;
	
	// tag de début, lire la table
	if ($loop === 0) {
		$SQLresult_etat = requete (	
			"SELECT idetatfiche, etatfiche FROM Etatfiches
			WHERE idetatfiche>=5 AND idetatfiche<=10
			ORDER BY idetatfiche" );
	}

	//  Appel du nom courant
	$l_etat = mysqli_fetch_assoc ($SQLresult_etat);
	$Xvars['l_etat'] = $l_etat;
	$Xvars['idetatfiche'] = $l_etat['idetatfiche'];

	return ($l_etat) ? 'ACT,LOOP' : 'EXIT' ;
} 

################################################################ XML_item ###
function XML_item ($loop, $attr, $Xaction) {

	if ($loop === null) return;		// tag final : </item>
	
	global $Xvars;
	global  $tableau,  $nb;
		
	// Alternance des couleurs de ligne
	$Xvars['class'] =  ($loop % 2) ? 'collig1' : 'collig2';

	if ($loop < $nb) {
		$Xvars['ligne'] = $tableau[$loop];		
		return  'ACT,LOOP';
	} else return  'EXIT' ;
} 

############################################################################
################################################################# TRAITEMENT

$custom_css = "utilit_encours.css";
require_once ('init.inc.php');

### Traitement des entrées
####################################

$nomrap = $_SESSION['rapporteur'];	// Filtrage pour le rapporteur enregistré

$f_re_afficher = (isset ($_POST['re_afficher'])) ? TRUE : FALSE;

if (isset ($_POST['marks'])) {		// Tout ceci manque d'élégance, mais au moins c'est clair...
	$l_marks = $_POST['marks'];
	Pb_set (P_PERSO_5, in_array (5, $l_marks));
	Pb_set (P_PERSO_6, in_array (6, $l_marks));
	Pb_set (P_PERSO_7, in_array (7, $l_marks));
	Pb_set (P_PERSO_8, in_array (8, $l_marks));
	Pb_set (P_PERSO_9, in_array (9, $l_marks));
	Pb_set (P_PERSO_10, in_array (10, $l_marks));
} else {
	$l_marks = array (							// il est nécessaire de conserver l_marks pour faciliter le traitement XML
		(Pb (P_PERSO_5)?5:''), (Pb (P_PERSO_6)?6:''),	(Pb (P_PERSO_7)?7:''),
		(Pb (P_PERSO_8)?8:''),	(Pb (P_PERSO_9)?9:''),	(Pb (P_PERSO_10)?10:''));
}

// Sélection du rapporteur : elle se fait uniquement en cas de réaffichage de la liste
// (sinon post['raps'] est toujours null)
if ($f_re_afficher) {
	if (isset ($_POST['raps'])) {
		$l_raps = $_POST['raps'] ;
		Pb_set (P_PERSO_INV, in_array ('respinv', $l_raps));
		Pb_set (P_PERSO_COLL, in_array ('corcol', $l_raps));
	} else {
		Pb_set (P_PERSO_INV, 0);
		Pb_set (P_PERSO_COLL, 0);
	}
}
$f_perso = Pb (P_PERSO_INV) || Pb (P_PERSO_COLL); 	// Mode perso si au moins un choix personnel

###Création des tables décrivant les fiches
##############################################

$t_cor_col = array ();
$t_etatfiche = array ();
$t_manque = array ();
$t_nom = array ();
$t_idcollection = array ();
$t_nb_medias = array ();
$t_note = array ();
$t_nrinventaire = array ();
$t_resp_inv = array ();
$t_type = array ();

### Sélectionner les fiches concernées
#######################################

// Sélectionner tous les états demandés
$where = '';
foreach ($l_marks as $item) {
   if ($item != '') $where .= 'idetatfiche='.$item.' OR ';
}
$where = substr($where, 0, -3);

$SQLresult = requete ( "SELECT *
	FROM Collections
	left join Machines on Machines.idmachine=Collections.idmachine 
	left join Documents on Documents.iddocument=Collections.iddocument     
	left join Logiciels on Logiciels.idlogiciel=Collections.idlogiciel  
	left join Acquisitions on Acquisitions.idacquisition=Collections.idacquisition
	left join Etablissements on Etablissements.idetablissement=Collections.idetablissement
	WHERE ".$where); 

### NOTE :
# Il est moins élégant mais nettement plus simple de traiter les fiches "perso" lors de l'analyse ligne à ligne :
# 	le traitement par SQL oblige à passer de la table Rapporteurs à la table Personnes.
# ATTENTION : la sélection se fait ici par le nom seul, sans le prénom...

### Analyser ligne à ligne, vérifier la présence de ...
#######################################################

$i=-1; 	// le premier indice sera 0 (comme le compteur "loop")

while ($ligne = mysqli_fetch_assoc ($SQLresult)) {
	$idcollection = $ligne ['idcollection'];

	// Composer les tables de noms et d'organismes
	$tl_orga = Compose_tl_orga ($idcollection);
	$tl_perso = Compose_tl_perso ($idcollection);
	
	// Si affichage d'un tableau tableau perso, prendre le nom du rédacteur 
	// et le chercher dans les responsables inventaire et/ou correspondant collection, 
	// Si oui, sauter l'itération en cours
	if ($f_perso) {
		if ( !(!(stripos (AffNoms ('p', 'Responsable inventaire'), $nomrap)===FALSE) && Pb (P_PERSO_INV))
		 AND !(!(stripos (AffNoms ('p', 'Correspondant collection'), $nomrap)===FALSE) && Pb (P_PERSO_COLL)) ) CONTINUE;
	};
	
	// Incrémenter le compteur
	$i += 1; 
	
	$t_idcollection [$i] = $idcollection;	

	$t_nrinventaire [$i] = Nrinventaire ($idcollection, $ligne);

	$t_etatfiche [$i] =  $ligne ['idetatfiche'];
	
	$t_note[$i] = 0;		// valeur initiale 
	$t_manque[$i] = '';		// valeur initiale
	
	if (!empty($ligne['idmachine'])) {
//		$t_type [$i] = 'machine';
		$t_nom [$i] = $ligne['nom'];
	} elseif (!empty($ligne['iddocument'])) {
//		$t_type [$i] = 'document';
		$t_nom [$i] = $ligne['titredoc'];
	} elseif (!empty($ligne['idlogiciel'])) {
//		$t_type [$i] = 'logiciel';
		$t_nom [$i] = $ligne['titrelog'];
	};	
	$t_type [$i] = $ligne ['type'];
	
	$t_cor_col[$i] = AffNoms ('p','Correspondant collection');
	$t_resp_inv[$i] = AffNoms ('p', 'Responsable inventaire');
	
	// Table des manques est idéalement vide...
	$t_manque[$i] = '';
	$t_note[$i] = 0;
	
#---Collections
	// date ou période		1
	if (($ligne['date1'] != 0) OR ($ligne['idperiode']>1)) $t_note[$i] += 2;
	else $t_manque[$i] .= "date ou période, ";
	
	// acquisition			1
	if ($ligne['acquisition'] != '(inconnu)') $t_note[$i] += 1;
	else $t_manque[$i] .= "acquisition, ";
	
	// description			10
	if ($ligne['description'] !== NULL AND strlen ($ligne['description']) > 10) $t_note[$i] += 10;
	else $t_manque[$i] .= "<strong>description</strong>, ";
	
	// utilisation			10
	if ($ligne['descutilisation'] !== NULL AND strlen ($ligne['descutilisation']) > 10) $t_note[$i] += 10;
	else $t_manque[$i] .= "<strong>utilisation</strong>, ";
	
	// état					1
	if ($ligne['idetat'] != 1) $t_note[$i] += 2;
	else $t_manque[$i] .= "etat, ";
	
	//---donateur			2	
	if (AffNoms ('op', 'Donateur')) $t_note[$i] += 2;
	else $t_manque[$i] .= "donateur, ";
		
	//---utilisateur		1
	if (AffNoms ('op', 'Utilisateur')) $t_note[$i] += 1;
	else $t_manque[$i] .= "utilisateur, ";
	
	//---correspondant collection	5
	if (AffNoms ('p', 'Correspondant collection')) $t_note[$i] += 5;
	else $t_manque[$i] .= "Correspondant collection, ";
	
	//---matériaux			5
	$SQLmate = requete ( "SELECT idmateriau FROM Col_Mate WHERE idcollection=$idcollection"); 	
	if (mysqli_num_rows ($SQLmate) !=0) $t_note[$i] += 5;
	else $t_manque[$i] .= "matériaux, ";
	
	//---mots clés			3
	$SQLmot = requete ( "SELECT idmotcle FROM Col_Mot WHERE idcollection=$idcollection"); 	
	if (mysqli_num_rows ($SQLmot) !=0) $t_note[$i] += 5;
	else $t_manque[$i] .= "mots clés, ";

#---Cas machine
	if (!empty($ligne['idmachine'])) {
		// nom					10
		if ($ligne['nom'] != '') $t_note[$i] += 5;
		else $t_manque[$i] .= "<strong>nom</strong>, ";
		
		// (modèle)
		if ($ligne['modele'] != '') $t_note[$i] += 0;
		else $t_manque[$i] .= "modele, ";
		
		// (nr de série)		1
		if ($ligne['nrserie'] != '') $t_note[$i] += 1;
		else $t_manque[$i] .= "No de série, ";
		
		// production			1
		if ($ligne['idproduction'] != 1) $t_note[$i] += 2;
		else $t_manque[$i] .= "production, ";
	
		// dimensions paral ou cylindre	5
		if (($ligne['dimlong'] != 0) AND (($ligne['dimlarg'] != 0) OR($ligne['dimdiam'] != 0))) $t_note[$i] += 5;
		else $t_manque[$i] .= "dimensions, ";
		
		// couleur						2
		if ($ligne['couleur'] != '') $t_note[$i] += 2;
		else $t_manque[$i] .= "couleur, ";
		
		// poids						2
		if ($ligne['dimpoids'] != 0) $t_note[$i] += 2;
		else $t_manque[$i] .= "poids, ";

	
	// ---fabricant			10
#---Cas document
	} elseif (!empty($ligne['iddocument'])) {
		// titre				20
		if ($ligne['titredoc'] != 1) $t_note[$i] += 10;
		else $t_manque[$i] .= "<strong>titre document</strong>, ";
		
		//---éditeur			10
		if (AffNoms ('o', 'Editeur')) $t_note[$i] += 7;
		else $t_manque[$i] .= "Editeur, ";
	
	
#---Cas logiciel
	} elseif (!empty($ligne['idlogiciel'])) {
		// titre				20
		if ($ligne['titrelog'] != 1) $t_note[$i] += 10;
		else $t_manque[$i] .= "<strong>titre logiciel</strong>, ";
		
		//---éditeur			10
		if (AffNoms ('o', 'Editeur')) $t_note[$i] += 7;
		else $t_manque[$i] .= "Editeur, ";
	}
	
#---Médias					20+20
	$SQLmedia = requete ( "SELECT idmedia FROM Col_Med WHERE idcollection=$idcollection"); 	
	$t_nb_medias[$i] =  mysqli_num_rows ($SQLmedia);
	if (mysqli_num_rows ($SQLmedia) >1) $t_note[$i] += 40;
	else if (mysqli_num_rows ($SQLmedia) ==1) {
		$t_note[$i] += 20;
		$t_manque[$i] .= "un seul média, ";
	} else $t_manque[$i] .= "<strong>médias</strong>, ";
		
}			

$nb = $i+1; 
	Message ('Nombre de fiches : %0', $nb);

### Afficher partir du modèle XML
####################################### 
Debut();  


// Trier les tableaux suivant les notes
array_multisort ($t_note, SORT_NUMERIC, SORT_DESC, $t_cor_col, $t_etatfiche, $t_manque, $t_nb_medias,
	$t_nom, $t_idcollection, $t_nrinventaire, $t_resp_inv, $t_type); 
// et créer un tableau par lignes
$tableau = array ();
For ($i=0; $i<$nb; $i++) {
	$tableau[$i] = array (
		'cor_col' => $t_cor_col[$i], 
		'etatfiche' => $t_etatfiche[$i], 
		'manque' => $t_manque[$i], 
		'nom' => $t_nom[$i],
		'idcollection' => $t_idcollection[$i], 
		'nb_medias' => $t_nb_medias[$i],
		'note' => $t_note[$i], 
		'nrinventaire' => $t_nrinventaire[$i], 
		'resp_inv' => $t_resp_inv[$i], 
		'type' => $t_type[$i]
		);
}

$Xvars['tableau'] = $tableau;
$Xvars['nb'] = $nb;
$Xvars['l_marks'] = $l_marks;
$Xvars['nomrap'] = $nomrap;
$Xvars['droits'] = $droits;

$liste_xml = Xopen ('./XML_modeles/utilit_encours.xml') ;
Xpose ($liste_xml);

#################################### Fin de traitement
Fin ();
?>