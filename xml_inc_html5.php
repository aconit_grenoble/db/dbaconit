<?php
# v14.2		151112	PhD		Création à partir de dbgalerie
# v15			160219	PhD		Compléments pour HTML5 dbaconit
# v23.4		181128	PhD		Généralisation du retour Xaction
#
######################################################################
# Ces fonctions sont nécessaires pour la génération de code HTML5 correct
# Inversement elles ne doivent pas être intégrées pour produire du XHTML...
#
#
################################################################ XML_br ###
function XML_br ($loop, $attr, $Xaction) {
	if ($loop !== null) return $Xaction;				// $Xaction inchangé
	else return '';								// pas de recopie de la balise fermante
}

################################################################ XML_hr ###
function XML_hr ($loop, $attr, $Xaction) {
	if ($loop !== null) return $Xaction;				// $Xaction inchangé
	else return '';								// pas de recopie de la balise fermante
}

################################################################ XML_img ###
function XML_img ($loop, $attr, $Xaction) {
	if ($loop !== null) return $Xaction;				// $Xaction inchangé
	else return '';								// pas de recopie de la balise fermante
}

################################################################ XML_input ###
function XML_input ($loop, $attr, $Xaction) {
	if ($loop !== null) return $Xaction;				// $Xaction inchangé
	else return '';								// pas de recopie de la balise fermante
}

################################################################ XML_meta ###
function XML_meta ($loop, $attr, $Xaction) {
	if ($loop !== null) return $Xaction;				// $Xaction inchangé
	else return '';								// pas de recopie de la balise fermante
}

######################################################################
?>